﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class MainMenuEvents : MonoBehaviour
{
    // Define the background music of the hanger
    public AudioClip backgroundMusic;
    // Define the background music of the hanger
    public AudioClip[] sfxClips;
    // Textures
    public Texture2D mainTexture;
    // Sprite sheet block size
    public int tileSize;
    // Basic block object for all objects
    public GameObject blockObject;
    // Block Shader
    public Shader blockShader;

    // Used to store the main music object instance
    private GameObject MainMusic;

    Ship ship;
    // Use this for initialization
    public void Start ()
    {
        // Get the music game object instance
        MainMusic = GameObject.Find("MainMusic");
        // Define the instances of sfx clips
        DataStorage.SFXClips = sfxClips;
        // Switch the audio source
        MainMusic.GetComponent<AudioHandler>().SwitchClip(backgroundMusic);
        // Setup class instances of objects to various static functions
        DataStorage.BlockPrefab = blockObject;
        DataStorage.BlockShader = blockShader;
        DataStorage.SpriteSheetSpriteSize = tileSize;
        DataStorage.SpriteSheetSpriteWidth = mainTexture.width / tileSize;
        DataStorage.SpriteSheetSpriteHeight = mainTexture.height / tileSize;
        DataStorage.TexturesInstance = mainTexture;

        // Verify that all the ship blocks are loaded into ram
        Utilities.PrepairShipBlocks();
        
        // Get all the paths to the users saved ships
        List<string> shipPaths = Utilities.GetSavedShipNames(DataStorage.ShipFolderPath);
        // If the user has ships
        if (shipPaths.Count > 0)
        {
            // Generate a random index in the range of the shipPath length
            int shipIndex = new System.Random().Next() % shipPaths.Count;
            // Initialize a new ship and load it from the random file
            ship = new Ship(Utilities.LoadShipData(DataStorage.ShipFolderPath + shipPaths[shipIndex]),1);
            // Calculate the ships scale if it was to fill the whole screen
            float scale = Camera.main.orthographicSize / 2 * (Screen.width / Screen.height);
            // Reduce the size of the scale to 10%
            scale *= 0.1f;
            // Set the position of the ship
            ship.GetShipObject().transform.position -= new Vector3(Camera.main.orthographicSize / 2, 0.0f, 0.0f);
            // Set the scale of the ship to the scale
            ship.GetShipObject().transform.localScale = new Vector3(scale, scale, 1.0f);
        }
        // Add a click listener for when the hanger button is pressed
        GameObject.Find("HangerButton").GetComponent<Button>().onClick.AddListener(delegate {
            SceneManager.LoadScene("Hanger");
        });
        // Add a click listener for when the settings button is pressed
        GameObject.Find("SettingsButton").GetComponent<Button>().onClick.AddListener(delegate {
            // Check to see if the ui is open
            if (Utilities.IsUIOpen(GameObject.Find("SettingsModel").GetComponent<CanvasGroup>()))
            {
                // Close the ui
                Utilities.HideUI(GameObject.Find("SettingsModel").GetComponent<CanvasGroup>());
            }
            else
            {
                // Show the Settings ui
                Utilities.ShowUI(GameObject.Find("SettingsModel").GetComponent<CanvasGroup>());
            }
        });
        // Add a click listener for when the exit button is pressed
        GameObject.Find("ExitButton").GetComponent<Button>().onClick.AddListener(delegate {
            Application.Quit();
        });

    }

    public void Update()
    {
        // If a ship was loaded, update it's ticks
        if (ship != null) ship.UpdateTick(0);
    }

}
