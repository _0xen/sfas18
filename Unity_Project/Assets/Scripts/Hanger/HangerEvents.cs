﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System;

public class HangerEvents : MonoBehaviour
{
    // Define the background music of the hanger
    public AudioClip backgroundMusic;
    // Define the background music of the hanger
    public AudioClip[] sfxClips;
    // Textures
    public Texture2D mainTexture;
    // Sprite sheet block size
    public int tileSize;
    // Basic block object for all objects
    public GameObject blockObject;
    // Block Shader
    public Shader blockShader;


    // Boundary parent
    public GameObject bounderyParent;
    // Ship parent
    public GameObject shipParent;
    // Dimensions for boundary
    public Vector2Int bounderyDim;
    // Boundary texture coord
    public Vector2Int bounderyTextureCoord;
    // Mirror texture coord
    public Vector2Int mirrorTextureCoord;


    // Speed the camera will zoom in and out when dragging the mouse scroll wheel
    public float zoomDragSpeed;
    // Define the speed that the camera will zoom in and out when scrolling the scroll wheel
    public float zoomScrollSpeed;
    // How far can the user zoom into the screen
    public float zoomLowerBounds;
    // How far out can the user zoom the camera
    public float zoomUperBounds;
    // Used to pass the confirmation model
    public CanvasGroup confirmationModel;
    // Confirmation model text
    public Text confirmationModelText;



    // Define a list to store all the grids of a ship, Grids not at index 0 will be classed as grids that are not apart of the ship
    private List<ShipGrid> shipGrids;
    // Define a array of blocks, each index will represent a keyboard number 1-5
    private System.Type[] blockHotKeys;
    // Used to store the current selected block by the user
    private ShipPart currentSellection1;
    private ShipPart currentSellection2;
    // Stores the current frames mouse position
    private Vector3 currentMousePosition;
    // Stores the last frames mouse position for comparison
    private Vector3 lastMousePosition;
    // Get the current frame mouse position rounded down
    private Vector2Int currentMousePositionRounded;
    // Stores the camera current zoom level
    private Vector3 cameraZoomDrag;
    // Define if a block can be placed, saves calculating it every frame, only when the mouse moves or a block is added/removed
    private bool canBlockBePlaced1 = false;
    private bool canBlockBePlaced2 = false;
    // Is mirroring the current selection block
    private bool mirroring = false;
    // Define if the cursor has changed since the last frame
    private bool cursorChanged = false;
    // Used to store the mirror line instance
    private GameObject mirrorLine;
    // offset so the cursor is always on top
    private Vector3 cursorOffset = new Vector3(0.0f, 0.0f, -0.01f);
    // Define the current block index that is selected
    private int blockGroupIndex = 0;
    // Current block group we are using
    private System.Type currentBlockGroup;
    // Keeps track if the title bar popup is open, if it is then re initialize the close request
    private bool isTitleBarPopupOpen = false;
    private int titleBarPopupCloseRequestCount = 0;
    // Store a local instance of the ships data, this holds things such as its cost, etc
    private DataStorage.ShipStatData shipStatsData;

    private enum ConfirmationModel { NA, ClearGrids };
    private ConfirmationModel currentConfirmation = ConfirmationModel.NA;

    // Used to store the main music object instance
    private GameObject MainMusic;
    // Used to store the sfx object instance
    private SFXController SFXInstance;


    // Use this for initialization
    void Start ()
    {
        // Get the music game object instance
        MainMusic = GameObject.Find("MainMusic");
        // Switch the audio source
        MainMusic.GetComponent<AudioHandler>().SwitchClip(backgroundMusic);
        // Get the sfxInstance
        SFXInstance = GameObject.Find("SFXController").GetComponent<SFXController>();
        // Define the instances of sfx clips
        DataStorage.SFXClips = sfxClips;
        InitScript();
        SetupHangerForShip();
    }

    private void InitScript()
    {
        // Click Events
        SetupUI();
        // Setup class instances of objects to various static functions
        DataStorage.BlockPrefab = blockObject;
        DataStorage.BlockShader = blockShader;
        DataStorage.SpriteSheetSpriteSize = tileSize;
        DataStorage.SpriteSheetSpriteWidth = mainTexture.width / tileSize;
        DataStorage.SpriteSheetSpriteHeight = mainTexture.height / tileSize;
        DataStorage.TexturesInstance = mainTexture;

        // Define the blocks bound to each keyboard number key. This will be dynamic in the future
        blockHotKeys = new System.Type[4] {
            Block.BLOCK_INSTANCE.GetType(),//1
            PoweredBlock.POWERED_BLOCK_INSTANCE.GetType(),//2
            EngineBlock.ENGINE_BLOCK_INSTANCE.GetType(),//3
            WeaponBlock.WEPON_BLOCK_INSTANCE.GetType(),//4
        };
        // Initialize various vector positions
        currentMousePosition = new Vector3();
        lastMousePosition = new Vector3();
        currentMousePositionRounded = new Vector2Int();
        cameraZoomDrag = new Vector3();
        // Setup boundary of the editable area
        CreateBoundery();
        // Create the mirror line
        mirrorLine = Utilities.CreateObject(blockObject, new Vector3(bounderyDim.x / 2, bounderyDim.y / 2, 0.0f) + new Vector3(0.5f, 0.5f, 0.0f), bounderyParent, Utilities.GetSpriteSheetTexture(mirrorTextureCoord, new Vector2Int(1, 1)));
        mirrorLine.transform.localScale = new Vector3(0.1f, bounderyDim.y, 1.0f);
        HideMirrorLine();
    }

    private void SetupHangerForShip()
    {
        SetShipName(DataStorage.ShipName);
        LoadShip();
        // If some ship grids were successfully loaded into the game, then center the camera in the middle of grid[0]
        // Otherwise center the camera into the center of the map
        if (shipGrids.Count > 0)
        {
            // Get the center of the main grid and orientate the camera to be in the center
            Vector3 ship_offset = Utilities.GetGridCenter(shipGrids[0]);
            Camera.main.transform.position = new Vector3(-ship_offset.x, -ship_offset.y, zoomScrollSpeed);
        }
        else
        {
            // Position camera into center of the screen
            Camera.main.transform.position = new Vector3(bounderyDim.x / 2 + 0.5f, bounderyDim.y / 2 + 0.5f, zoomScrollSpeed);
        }

        // Recalculate the ships cost, etc
        shipStatsData = Utilities.RecalculateShipsStatsData(shipGrids);

        // Define what the user currently has selected as there primary block
        SwitchBlockGroup(Block.BLOCK_INSTANCE.GetType());

        // Recalculate information about the ships grids
        UpdateBlocks();
        UpdateShipUI();
    }

    // Called once to initialize interface click listeners
    void SetupUI()
    {
        // Block group selection buttons
        GameObject.Find("HullButton").GetComponent<Button>().onClick.AddListener(delegate {
            SwitchBlockGroup(Block.BLOCK_INSTANCE.GetType());
        });
        GameObject.Find("EnergyButton").GetComponent<Button>().onClick.AddListener(delegate {
            SwitchBlockGroup(PoweredBlock.POWERED_BLOCK_INSTANCE.GetType());
        });
        GameObject.Find("ThrusterButton").GetComponent<Button>().onClick.AddListener(delegate {
            SwitchBlockGroup(EngineBlock.ENGINE_BLOCK_INSTANCE.GetType());
        });
        GameObject.Find("WeponsButton").GetComponent<Button>().onClick.AddListener(delegate {
            SwitchBlockGroup(WeaponBlock.WEPON_BLOCK_INSTANCE.GetType());
        });

        // Pagination navigation buttons
        GameObject.Find("CurrentItemPaginationLast").GetComponent<Button>().onClick.AddListener(delegate {
            LastBlock();
        });
        GameObject.Find("CurrentItemPaginationNext").GetComponent<Button>().onClick.AddListener(delegate {
            NextBlock();
        });

        // On confirmation
        GameObject.Find("ConfirmationModelConfirmButton").GetComponent<Button>().onClick.AddListener(delegate {
            OnConfirmationModelConfirmButton();
        });
        GameObject.Find("ConfirmationModelCancelButton").GetComponent<Button>().onClick.AddListener(delegate {
            OnConfirmationModelCancelButton();
        });

        // On join button press
        GameObject.Find("CreateGameButton").GetComponent<Button>().onClick.AddListener(delegate {
            OnCreateGameButton();
        });
        GameObject.Find("JoinButton").GetComponent<Button>().onClick.AddListener(delegate {
            OnJoinButtonPress();
        });


        // On clear button press
        GameObject.Find("ClearButton").GetComponent<Button>().onClick.AddListener(delegate {
            RequestClearGrids();
        });


        // Save / Load buttons
        GameObject.Find("SaveButton").GetComponent<Button>().onClick.AddListener(delegate {
            SaveShip();
            UpdateSavedShips();
        });

        GameObject.Find("LoadShipDropdown").GetComponent<Dropdown>().onValueChanged.AddListener(delegate {
            LoadShipDropdown();
        });

        UpdateSavedShips();
    }

    // Load the selected ship from the drop down based on the users choice
    void LoadShipDropdown()
    {
        // If index 0 was return then return as this is the default selection
        if (GameObject.Find("LoadShipDropdown").GetComponent<Dropdown>().value == 0) return;
        DataStorage.ShipName = GameObject.Find("LoadShipDropdown").GetComponent<Dropdown>().options[GameObject.Find("LoadShipDropdown").GetComponent<Dropdown>().value].text;
        // Update the drop down list as new ships might have been found
        UpdateSavedShips();
        // Set the drop down value back to 0
        GameObject.Find("LoadShipDropdown").GetComponent<Dropdown>().value = 0;
        // Remove all ships pats currently in the workspace
        ClearShipGrids();
        // Load the ship and re-setup all ship parameters
        SetupHangerForShip();
    }

    // Clear the Load ship drop down and populate it with all the ships currently stored in the drop down
    void UpdateSavedShips()
    {
        // Remove all values
        GameObject.Find("LoadShipDropdown").GetComponent<Dropdown>().ClearOptions();
        // Create a Select ship input value
        GameObject.Find("LoadShipDropdown").GetComponent<Dropdown>().options.Add(new Dropdown.OptionData() { text = "Select Ship" });
        // Add all other ships that exist in the game
        GameObject.Find("LoadShipDropdown").GetComponent<Dropdown>().AddOptions(Utilities.GetSavedShipNames(DataStorage.ShipFolderPath));
    }

    // When the join button is pressed
    void OnJoinButtonPress()
    {
        // Show the title bar popup with some text
        if (ValidShip())
        {
            SaveShip();
            DataStorage.ShipName = GetShipName();
            DataStorage.Connection = DataStorage.ConnectionType.Client;
            SceneManager.LoadScene("Lobby");
        }
    }

    void OnCreateGameButton()
    {
        // Show the title bar popup with some text
        if (ValidShip())
        {
            SaveShip();
            DataStorage.ShipName = GetShipName();
            DataStorage.Connection = DataStorage.ConnectionType.ServerClient;
            SceneManager.LoadScene("Lobby");
        }
    }

    // Used when the clear grids button is pressed and will open up a confirm dialog for the user to confirm
    void RequestClearGrids()
    {
        // Set the confirmation type to clear grids
        currentConfirmation = ConfirmationModel.ClearGrids;
        // Open up the popup with the defined string
        Utilities.OpenConfirmationModel("Are you sure you want to clear your ship and restart?");
    }

    // Clear all ship grid blocks and initialize a new shipGrid variable
    void ClearShipGrids()
    {
        // Loop through all the ship grids
        foreach (ShipGrid grid in shipGrids)
        {
            // Loop through all the parts
            foreach(ShipPart part in grid.GetParts())
            {
                // Destroy all the parts
                part.Destroy();
            }
        }
        // Initialize a new array for the grids
        shipGrids = new List<ShipGrid>();
    }


    // When the confirmation button is pressed
    void OnConfirmationModelConfirmButton()
    {
        switch (currentConfirmation)
        {
            // If the current confirmation type is the clear grid module then
            case ConfirmationModel.ClearGrids:
                // Clear all ship grids
                ClearShipGrids();
                // Save the ship
                SaveShip();
                // Update the UI
                UpdateSavedShips();
                // Re-initialize the hanger
                SetupHangerForShip();
                break;
        }
        // Call the cancel function as it is the same as closing the window
        OnConfirmationModelCancelButton();
    }

    // Saves the ship data
    void SaveShip()
    {
        Utilities.SaveShip(shipGrids, DataStorage.ShipFolderPath + Utilities.FilterIllegalFilenameChars(GetShipName()));
        GameObject.Find("ShipStatsName").GetComponent<Text>().text = GetShipName();
    }

    // When the cancel button is pressed
    void OnConfirmationModelCancelButton()
    {
        currentConfirmation = ConfirmationModel.NA;
        Utilities.CloseConfirmationModel();
    }

    // Called when the user navigates the last block in the que
    void LastBlock()
    {
        blockGroupIndex--;
        SwitchBlock(blockGroupIndex);
    }

    // Called when the user navigates the next block in the que
    void NextBlock()
    {
        blockGroupIndex++;
        SwitchBlock(blockGroupIndex);
    }

    // Switch what block group that is currently selected
    void SwitchBlockGroup(System.Type group)
    {
        // If we are already in the group, move to the next index in the group
        if(group == currentBlockGroup)
        {
            NextBlock();
        }
        else // Otherwise set the group and update the index
        {
            currentBlockGroup = group;
            blockGroupIndex = 0;
        }
        // Update what block is selected
        SwitchBlock(blockGroupIndex);
    }

    // Update is called once per frame
    void Update ()
    {
        MouseUpdate();
        KeyboardUpdate();
    }

    // When the application closes, save the current ship
    void OnApplicationQuit()
    {
        SaveShip();
        Debug.Log("Application ending after " + Time.time + " seconds");
    }

    // Update all UI elements in the hanger
    private void UpdateShipUI()
    {
        // Set the current blocks name
        GameObject.Find("CurrentItemName").GetComponent<Text>().text = currentSellection1.block.Name;
        // Set the current blocks image
        GameObject.Find("CurrentItemImage").GetComponent<RawImage>().texture = currentSellection1.GetCurrentTexture();
        GameObject.Find("CurrentItemImage").GetComponent<RawImage>().transform.localScale = Utilities.GetItemPortrateScale(currentSellection1);
        string currentItemInfo = "";
        // Set the current blocks cost
        currentItemInfo += "Cost: " + currentSellection1.block.Cost + "\n";
        // Set the current blocks health
        currentItemInfo += "Health: " + currentSellection1.block.Health + "\n";
        // Set the current blocks weight
        currentItemInfo += "Weight: " + currentSellection1.block.Weight + "\n";
        // If the block is powered, set the current blocks powered info
        if (currentSellection1.block is PoweredBlock)
        {
            PoweredBlock poweredBlock = (PoweredBlock)currentSellection1.block;
            if (poweredBlock.IsType(PoweredBlock.PoweredBlockType.Input)) currentItemInfo += "Input: " + poweredBlock.MaxInputPower + "MW\n";
            if (poweredBlock.IsType(PoweredBlock.PoweredBlockType.Storage)) currentItemInfo += "Storage: " + poweredBlock.MaxStoredPower + "MW\n";
            if (poweredBlock.IsType(PoweredBlock.PoweredBlockType.Output)) currentItemInfo += "Output: " + poweredBlock.MaxOutputPower + "MW\n";
        }
        // If the block is a engine, set the current blocks engine info
        if (currentSellection1.block is EngineBlock)
        {
            EngineBlock engineBlock = (EngineBlock)currentSellection1.block;
            currentItemInfo += "Thrust: " + (engineBlock.MaxThrust * 100) + "\n";
        }
        // If the block is a weapon, set the current blocks engine info
        if (currentSellection1.block is WeaponBlock)
        {
            WeaponBlock weponBlock = (WeaponBlock)currentSellection1.block;
            currentItemInfo += "Discharge: " + weponBlock.ShotPower + "MW\n";
            currentItemInfo += "Damage: " + weponBlock.BaseDamage + "\n";
        }
        // Set all the current blocks information onto the ui
        GameObject.Find("CurrentItemInfo").GetComponent<Text>().text = currentItemInfo;

        // Update the name field of the ships stats box
        GameObject.Find("ShipStatsName").GetComponent<Text>().text = GetShipName();
        string shipStatsText = "";

        // Add ships total cost to the UI
        shipStatsText += "Cost: " + shipStatsData.cost + "/" + Utilities.MaxShipCost + "\n";

        // Add ships health to the UI
        shipStatsText += "Health: " + shipStatsData.health + "\n";

        // Add ships weight to the UI
        shipStatsText += "Weight: " + shipStatsData.weight + "\n";

        // Add input power to the ui
        shipStatsText += "Input: " + shipStatsData.inputPower + "MW\n";
        // Add output power to the ui
        shipStatsText += "Output: " + shipStatsData.generatedPower + "MW\n";
        // Add stored power to the ui
        shipStatsText += "Stored: " + shipStatsData.storedPower + "MW\n";


        GameObject.Find("ShipStatsData").GetComponent<Text>().text = shipStatsText;
    }
    
    // check to see if the user can afford to place the current part
    private bool HasFundsForPart(int amount)
    {
        return shipStatsData.cost + amount < Utilities.MaxShipCost;
    }

    // Update all mouse functionality
    private void MouseUpdate()
    {
        // If the mouse is not over a UI element update its position
        if (!Utilities.IsMouseOverUI())
        {
            currentMousePosition = Input.mousePosition;
        }
        // If the user scrolls with the scroll wheel then zoom the camera in and out
        Camera.main.transform.Translate(Vector3.forward * (Input.GetAxis("Mouse ScrollWheel") * zoomScrollSpeed));
        // Verify the camera is inside the screen bounds
        CheckCameraBounds();
        // Get the mouse position in the world space at z axis 0
        Vector3 targetPosition = Camera.main.ScreenToWorldPoint(new Vector3(currentMousePosition.x, currentMousePosition.y, -Camera.main.transform.position.z - Vector2.kEpsilon));
        // If the mouse has moved into a new tile, then update the mouse position
        if (new Vector2Int(Mathf.FloorToInt(targetPosition.x), Mathf.FloorToInt(targetPosition.y)) != currentMousePositionRounded || cursorChanged)
        {
            cursorChanged = false;
            currentMousePositionRounded = new Vector2Int(Mathf.FloorToInt(targetPosition.x), Mathf.FloorToInt(targetPosition.y));
            // Move the mouse based on 'currentMousePositionRounded' and 'cursorOffset' to make sure the block is centered
            MoveCurrentSelection(new Vector3(currentMousePositionRounded.x, currentMousePositionRounded.y, 0.0f) + cursorOffset);
        }
        // If the mouse is not over a ui element
        if (!Utilities.IsMouseOverUI())
        {
            // Check to see if the alt key is down
            if (Input.GetKey(KeyCode.LeftAlt))
            {
                // If the user is holding down the left button key, then move the screen around with the mouse cursor
                if (Input.GetMouseButton(0))
                {
                    Camera.main.transform.Translate(
                        Camera.main.ScreenToWorldPoint(new Vector3(lastMousePosition.x, lastMousePosition.y, -Camera.main.transform.position.z - Vector2.kEpsilon)) -
                        Camera.main.ScreenToWorldPoint(new Vector3(currentMousePosition.x, currentMousePosition.y, -Camera.main.transform.position.z - Vector2.kEpsilon)));
                    CheckCameraBounds();
                }
            }
            else
            {
                // If the mouse left button is down
                if (Input.GetMouseButton(0))
                {
                    // Define a variable to hold if a block was placed in the mouse plane
                    bool planeOnePlace = false;
                    // If a block can be placed and the user can afford it, then place the block
                    if (canBlockBePlaced1)
                    {
                        if (HasFundsForPart(currentSellection1.block.Cost))
                        {
                            // Play the block place effect
                            SFXInstance.PlaySFX(DataStorage.SFXClips[1], SFXController.SFXSource.Channel1);
                            // Define that the plane had a block placed
                            planeOnePlace = true;
                            Utilities.AddPartToShip(shipGrids, currentSellection1.Clone(true));
                            // Update the blocks in the ship
                            currentSellection1.SetShaderColor(Utilities.BlockColorRed);
                            UpdateBlocks();
                            canBlockBePlaced1 = false;
                            // Update the ship data object to include the current blocks data
                            shipStatsData.cost += currentSellection1.block.Cost;
                            shipStatsData.health += currentSellection1.block.Health;
                            shipStatsData.weight += currentSellection1.block.Weight;
                            if (currentSellection1.block is PoweredBlock)
                            {
                                shipStatsData.generatedPower += ((PoweredBlock)currentSellection1.block).MaxOutputPower;
                                shipStatsData.inputPower += ((PoweredBlock)currentSellection1.block).MaxInputPower;
                                shipStatsData.storedPower += ((PoweredBlock)currentSellection1.block).MaxStoredPower;
                            }
                            UpdateShipUI();
                        }
                        else
                        {
                            // Display the popup that they cant afford the part
                            ShowTitleBarPopup("Not Enough Funds", 4.0f);
                        }
                    }
                    // If a block can be placed and the user can afford it, then place the block
                    if (mirroring && canBlockBePlaced2)
                    {
                        if (HasFundsForPart(currentSellection2.block.Cost))
                        {
                            // If the sfx was played for a block being placed on the non mirrored plane, don't play it now
                            if(!planeOnePlace) SFXInstance.PlaySFX(DataStorage.SFXClips[1], SFXController.SFXSource.Channel1);
                            Utilities.AddPartToShip(shipGrids, currentSellection2.Clone(true));
                            // Update the blocks in the ship
                            currentSellection2.SetShaderColor(Utilities.BlockColorRed);
                            UpdateBlocks();
                            canBlockBePlaced2 = false;
                            // Update the ship data object to include the current blocks data
                            shipStatsData.cost += currentSellection1.block.Cost;
                            shipStatsData.health += currentSellection1.block.Health;
                            shipStatsData.weight += currentSellection1.block.Weight;
                            if (currentSellection1.block is PoweredBlock)
                            {
                                shipStatsData.generatedPower += ((PoweredBlock)currentSellection1.block).MaxOutputPower;
                                shipStatsData.inputPower += ((PoweredBlock)currentSellection1.block).MaxInputPower;
                                shipStatsData.storedPower += ((PoweredBlock)currentSellection1.block).MaxStoredPower;
                            }
                            UpdateShipUI();
                        }
                        else
                        {
                            // Display the popup that they cant afford the part
                            ShowTitleBarPopup("Not Enough Funds", 4.0f);
                        }
                    }
                    // If no block was placed, play the failed sound effect
                    if (!canBlockBePlaced1 && !mirroring ||
                        !canBlockBePlaced1 && !canBlockBePlaced2 && mirroring)
                    {
                        SFXInstance.PlaySFX(DataStorage.SFXClips[3], SFXController.SFXSource.Channel3, 1);
                    }
                }
                // If the mouse right button is down
                if (Input.GetMouseButton(1))
                {
                    // If the part was removed, update the cursor change variable
                    bool partRemoved = RemovePart(currentMousePositionRounded);
                    if (partRemoved)
                    {
                        // Play the block destroy effect
                        SFXInstance.PlaySFX(DataStorage.SFXClips[2], SFXController.SFXSource.Channel1);
                        cursorChanged = true;
                        UpdateShipUI();
                    }
                    if (mirroring && RemovePart(GetMirroredPosition(currentMousePositionRounded,1)))
                    {
                        // If a part was not placed in the non mirrored plane, then play the sound effect
                        // This is to stop duplicate sound effects playing
                        if(!partRemoved) SFXInstance.PlaySFX(DataStorage.SFXClips[2], SFXController.SFXSource.Channel1);
                        cursorChanged = true;
                        UpdateShipUI();
                    }

                }
                // If the mouse scroll button is down update the zoom the camera
                if (Input.GetMouseButtonDown(2))
                {
                    cameraZoomDrag = currentMousePosition;
                }
                // If the mouse scroll button is down update the zoom the camera
                if (Input.GetMouseButton(2))
                {
                    Camera.main.transform.Translate(Vector3.forward * ((cameraZoomDrag.y - currentMousePosition.y)* zoomDragSpeed) * zoomScrollSpeed);
                    CheckCameraBounds();
                    cameraZoomDrag = currentMousePosition;
                }
                // If the mouse scroll button is up then whatever block, the cursor is over, change the selection
                if (Input.GetMouseButtonUp(2))
                {
                    GetToggledBlock();
                }
            }
        }
        lastMousePosition = currentMousePosition;
    }

    // Check to see if the camera is in the defined boundary, if not, move it into bounds
    private void CheckCameraBounds()
    {
        if (Camera.main.transform.position.z < zoomUperBounds) Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, zoomUperBounds);
        if (Camera.main.transform.position.z > zoomLowerBounds) Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, zoomLowerBounds);
        if (Camera.main.transform.position.x > bounderyDim.x * 2) Camera.main.transform.position = new Vector3(bounderyDim.x * 2, Camera.main.transform.position.y, Camera.main.transform.position.z);
        if (Camera.main.transform.position.x < -bounderyDim.x) Camera.main.transform.position = new Vector3(-bounderyDim.x, Camera.main.transform.position.y, Camera.main.transform.position.z);
        if (Camera.main.transform.position.y > bounderyDim.y * 2) Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, bounderyDim.y * 2, Camera.main.transform.position.z);
        if (Camera.main.transform.position.y < -bounderyDim.y) Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, -bounderyDim.y, Camera.main.transform.position.z);
    }

    // When called, we get the block that the mouse is over and change the current selection
    private void GetToggledBlock()
    {
        // Create a ray from the mouse's current pos
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // Collect information on all objects hit by the ray
        RaycastHit[] hits;
        hits = Physics.RaycastAll(ray, 100.0F);
        // If something was hit, continue
        if (hits.Length > 0)
        {
            // Defines if we should break out of the for loop
            bool blockFound = false;
            // Loop through all the hits and stop if there are no more or the block was found
            for (int i = 0; i < hits.Length && !blockFound; i++)
            {
                // Loop through all the ships grids and stop if there are no more or the block was found
                for (int j = 0; j < shipGrids.Count && !blockFound; j++)
                {
                    // Loop through all the ship grid's parts and stop if there are no more or the block was found
                    for (int k = 0; k < shipGrids[j].PartCount() && !blockFound; k++)
                    {
                        // If the current ship part is the one that was hit by the ray
                        if (shipGrids[j].GetParts()[k].blockObject == hits[i].transform.gameObject)
                        {
                            // Set that the block was found
                            blockFound = true;
                            // Switch group for the current selection based on the hit block type
                            SwitchBlockGroup(shipGrids[j].GetParts()[k].block.GetType());
                            // Define a index that will be the index of the block in the object group
                            int newGroupIndex = -1;
                            // Index for the loop
                            int l = 0;
                            // Loop through all the blocks defined in the Block class's group array and fine the match
                            /* Chose to use a for each here as it is easier then writing 
                             * "Block.GetTypeBlocks()[shipGrids[j].GetParts()[k].block.GetType()][l]" Several times */
                            foreach (Block indexCheck in Block.TypeBlocks[shipGrids[j].GetParts()[k].block.GetType()])
                            {
                                // If there is a ID match for the block then set the newGroupIndex
                                if(indexCheck.ID == shipGrids[j].GetParts()[k].block.ID)
                                {
                                    newGroupIndex = l;
                                }
                                l++;
                            }
                            // If the block was found, then switch what block is selected
                            if(newGroupIndex>=0)
                                SwitchBlock(newGroupIndex, shipGrids[j].GetParts()[k].rotation);
                            else // Otherwise as a fail safe, switch it manually
                                ChangeSelectedBlock(shipGrids[j].GetParts()[k].block, shipGrids[j].GetParts()[k].rotation);
                        }
                    }
                }
            }
        }
    }

    // Used to remove a part from the ship based on the position
    private bool RemovePart(Vector2Int position)
    {
        // Used to store the grid that had a part removed from
        int deletedGridIndex = -1;
        // Loop through the ship grids
        for (int i = 0; i < shipGrids.Count && deletedGridIndex == -1; i++)
        {
            // Loop through the parts in the grid
            for (int j = 0; j < shipGrids[i].PartCount() && deletedGridIndex == -1; j++)
            {
                // If the part is colliding with the position passed to the function. remove it
                if (Utilities.PointColliding(position, shipGrids[i].GetParts()[j]))
                {
                    deletedGridIndex = i;
                    // Update the ship data object to exclude the current blocks data
                    shipStatsData.cost -= shipGrids[i].GetParts()[j].block.Cost;
                    shipStatsData.health -= shipGrids[i].GetParts()[j].block.Health;
                    shipStatsData.weight -= shipGrids[i].GetParts()[j].block.Weight;
                    if (shipGrids[i].GetParts()[j].block is PoweredBlock)
                    {
                        shipStatsData.generatedPower -= ((PoweredBlock)shipGrids[i].GetParts()[j].block).MaxOutputPower;
                        shipStatsData.inputPower -= ((PoweredBlock)shipGrids[i].GetParts()[j].block).MaxInputPower;
                        shipStatsData.storedPower -= ((PoweredBlock)shipGrids[i].GetParts()[j].block).MaxStoredPower;
                    }
                    shipGrids[i].GetParts()[j].Destroy();
                    shipGrids[i].RemovePart(shipGrids[i].GetParts()[j]);
                }
            }
        }
        // If nothing was deleted, return false
        if (deletedGridIndex == -1) return false;

        List<ShipGrid> gridsToMerge = new List<ShipGrid>();
        // Loop through the ship parts and add them to a new sudo ship
        // This is to get them split into the different grids
        for (int i = 0; i < shipGrids[deletedGridIndex].PartCount(); i++)
        {
            Utilities.AddPartToShip(gridsToMerge, shipGrids[deletedGridIndex].GetParts()[i]);
        }
        // If the grid count is larger then 0, move the first index of gridsToMerge to the deletedGridIndex
        if (gridsToMerge.Count > 0)
        {
            shipGrids[deletedGridIndex] = gridsToMerge[0];
        }
        // Loop through all other grids and add them to the main ship
        for (int i = 1; i < gridsToMerge.Count; i++)
        {
            shipGrids.Add(gridsToMerge[i]);
        }
        // Update all the blocks
        UpdateBlocks();
        return true;
    }

    // Update all blocks in the ship
    private void UpdateBlocks()
    {
        // Remove any grid that is now empty
        Utilities.RemoveEmptyGrids(shipGrids);
        // Update the colors of the grid, this is the red highlighting if the part is not attached to the main grid
        Utilities.UpdateShipGridColors(shipGrids);
        // Update the Ships UI
        UpdateShipUI();
        // Update the power systems and animations
        foreach(ShipGrid grid in shipGrids)
        {
            grid.UpdatePowerSystems(true);
            grid.UpdateAnimation(0, 1);
        }
    }

    // Update the selected block by manually specifying the block type
    private void ChangeSelectedBlock(Block block, int rotation = 0)
    {
        // If there is already a block in the selection part, destroy it
        if (currentSellection1 != null) currentSellection1.Destroy();
        // Create a new block instance
        currentSellection1 = new ShipPart(new Vector3(), block, shipParent, rotation, true);
        // If there is already a block in the selection part, destroy it
        if (currentSellection2 != null) currentSellection2.Destroy();
        // Create a new block instance
        currentSellection2 = new ShipPart(new Vector3(), block, shipParent, rotation, true);
        // Move the block and the potential mirrored block into position
        MoveCurrentSelection(currentMousePosition + cursorOffset);
        cursorChanged = true;
        // If mirroring, offset the mirrored part by the current ones rotation
        if (mirroring)
        {
            if (currentSellection2.block.MirrorRotation)
                currentSellection2.Rotate(4);
            else
            {
                if (currentMousePositionRounded.x < bounderyDim.x/2)
                    currentSellection2.Rotate(1);
                else
                    currentSellection2.Rotate(-1);
            }
        }
        UpdateShipUI();
    }
    
    // Get the position of where the mirrored part selection should be placed
    private Vector2Int GetMirroredPosition(Vector2Int startingPos, int offset)
    {
        return new Vector2Int((bounderyDim.x - startingPos.x) - offset, startingPos.y);
    }

    // Get the position of where the mirrored part selection should be placed
    private Vector3 GetMirroredPosition(Vector3 startingPos, int offset)
    {
        Vector3 newPosition = new Vector3(Mathf.FloorToInt(bounderyDim.x - startingPos.x) - offset, Mathf.FloorToInt(startingPos.y), startingPos.z);
        return newPosition;
    }

    // Move the current selection and potentially the mirrored one as-well
    private void MoveCurrentSelection(Vector3 position)
    {
        // Move the first selection block into position
        currentSellection1.Move(position, true);
        // If we are mirroring, move the mirrored block into its custom position
        if (mirroring)
        {
            Vector3 offset = new Vector3(currentSellection2.rotation % 2 == 0 ? 0.0f : currentSellection2.block.PartDim.x - currentSellection2.block.PartDim.y, 0.0f, 0.0f);
            currentSellection2.Move(GetMirroredPosition(position, currentSellection2.block.PartDim.x) + offset, false);
        }
        else // If we are not mirroring the hide the selection behind the first one
        {
            currentSellection2.Move(position, true);
        }

        // Check to see if the cursor is in the boundary
        if (Utilities.InBoundery(currentMousePositionRounded, bounderyDim, currentSellection1) &&
            !Utilities.CollidingWithShipPart(shipGrids, currentSellection1))
        {
            // Highlight the cursor green as we can place the block
            currentSellection1.SetShaderColor(Utilities.BlockColorGreen);
            canBlockBePlaced1 = true;
        }
        else
        {
            // Highlight the cursor red as we can't place the block
            currentSellection1.SetShaderColor(Utilities.BlockColorRed);
            canBlockBePlaced1 = false;
        }
        // Check to see if the cursor is in the boundary and that it is mirrored
        if (mirroring && Utilities.InBoundery(currentMousePositionRounded, bounderyDim, currentSellection2) &&
            !Utilities.CollidingWithShipPart(shipGrids, currentSellection2) &&
            !Utilities.PartsColiding(currentSellection1, currentSellection2))
        {
            // Highlight the cursor green as we can place the block
            currentSellection2.SetShaderColor(Utilities.BlockColorGreen);
            canBlockBePlaced2 = true;
        }
        else
        {
            // Highlight the cursor red as we can't place the block
            currentSellection2.SetShaderColor(Utilities.BlockColorRed);
            canBlockBePlaced2 = false;
        }
    }

    private void KeyboardUpdate()
    {
        // If the input field is focused, then don't respond to any key events
        if (GameObject.Find("ShipNameInput").GetComponent<InputField>().isFocused) return;
        // If key range 1-5 is pressed, change block relatively
        for(int i = (int)KeyCode.Alpha1; i <= (int)KeyCode.Alpha4; i++)
        {
            if (Input.GetKeyUp((KeyCode)i))
            {
                SwitchBlockGroup(blockHotKeys[i - (int)KeyCode.Alpha1]);
                SFXInstance.PlaySFX(DataStorage.SFXClips[0], SFXController.SFXSource.Channel1);
            }
        }
        // If the mirror button is pressed the enable or disables it
        if (Input.GetKeyDown(KeyCode.M))
        {
            mirroring = !mirroring;
            if(mirroring)
                ShowMirrorLine();
            else
                HideMirrorLine();
            ChangeSelectedBlock(currentSellection1.block);
        }
        // If the user presses esc, the show the main menu
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            // Play the interface click
            SFXInstance.PlaySFX(DataStorage.SFXClips[0], SFXController.SFXSource.Channel1);
            // If the settings is open, close it
            if (Utilities.IsUIOpen(GameObject.Find("SettingsModel").GetComponent<CanvasGroup>()))
            {
                Utilities.HideUI(GameObject.Find("SettingsModel").GetComponent<CanvasGroup>());
            }
            else
            // If the hangar menu is open, close it
            if (Utilities.IsUIOpen(GameObject.Find("HangerMenuModel").GetComponent<CanvasGroup>()))
            {
                Utilities.HideUI(GameObject.Find("HangerMenuModel").GetComponent<CanvasGroup>());
            }
            else
            // If the confirm model is open, close it
            if (Utilities.IsUIOpen(GameObject.Find("ConfirmationModel").GetComponent<CanvasGroup>()))
            {
                Utilities.HideUI(GameObject.Find("ConfirmationModel").GetComponent<CanvasGroup>());
            }
            else
            {
                Utilities.ShowUI(GameObject.Find("HangerMenuModel").GetComponent<CanvasGroup>());
            }
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow) ||
            Input.GetKeyDown(KeyCode.A))
        {
            // Play the interface click
            SFXInstance.PlaySFX(DataStorage.SFXClips[0], SFXController.SFXSource.Channel1);
            // Rotate the main selection block counter clockwise
            currentSellection1.Rotate(-1);
            // If we are mirroring, rotate the other selection block clockwise
            if (mirroring)
            {
                if(currentSellection2.block.MirrorRotation)
                    currentSellection2.Rotate(-1);
                else
                    currentSellection2.Rotate(1);
            }
            else // Else rotate it in sync with the main selection block as it will be hidden behind it
                currentSellection2.Rotate(-1);
            MoveCurrentSelection(new Vector3(currentMousePositionRounded.x, currentMousePositionRounded.y, 0.0f) + cursorOffset);
            UpdateShipUI();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow) ||
            Input.GetKeyDown(KeyCode.D))
        {
            // Play the interface click
            SFXInstance.PlaySFX(DataStorage.SFXClips[0], SFXController.SFXSource.Channel1);
            // Rotate the main selection block clockwise
            currentSellection1.Rotate(1);
            // If we are mirroring, rotate the other selection block counter clockwise
            if (mirroring)
            {
                if (currentSellection2.block.MirrorRotation)
                    currentSellection2.Rotate(1);
                else
                    currentSellection2.Rotate(-1);
            }
            else // Else rotate it in sync with the main selection block as it will be hidden behind it
                currentSellection2.Rotate(1);
            MoveCurrentSelection(new Vector3(currentMousePositionRounded.x, currentMousePositionRounded.y, 0.0f) + cursorOffset);
            UpdateShipUI();
        }
    }

    // Create boundary that will go around the play field
    private void CreateBoundery()
    {
        Vector3 offset = new Vector3(0.5f, 0.5f, 0.01f);
        for (int i = -1; i < bounderyDim.x + 1; i++)
        {
            // Create the bottom row
            Utilities.CreateObject(blockObject, new Vector3(i, -1, 0.0f) + offset, bounderyParent, Utilities.GetSpriteSheetTexture(bounderyTextureCoord, new Vector2Int(1, 1)));
            // Create the top row
            Utilities.CreateObject(blockObject, new Vector3(i, bounderyDim.y, 0.0f) + offset, bounderyParent, Utilities.GetSpriteSheetTexture(bounderyTextureCoord, new Vector2Int(1, 1)));
        }
        for (int i = 0; i < bounderyDim.y; i++)
        {
            // Create the left size
            Utilities.CreateObject(blockObject, new Vector3(-1, i, 0.0f) + offset, bounderyParent, Utilities.GetSpriteSheetTexture(bounderyTextureCoord, new Vector2Int(1, 1)));
            // Create the right size
            Utilities.CreateObject(blockObject, new Vector3(bounderyDim.x, i, 0.0f) + offset, bounderyParent, Utilities.GetSpriteSheetTexture(bounderyTextureCoord, new Vector2Int(1, 1)));
        }
    }

    private void LoadShip()
    {
        // Remove all previous ship parts
        if (shipGrids != null)
        {
            for (int i = 0; i < shipGrids.Count; i++)
            {
                for (int j = 0; j < shipGrids[i].PartCount(); j++)
                {
                    shipGrids[i].GetParts()[j].Destroy();
                    shipGrids[i].RemovePart(shipGrids[i].GetParts()[j]);
                }
            }
        }
        // Load the users ship and store it in the shipParent game object
        shipGrids = Utilities.LoadShipHanger(shipParent, DataStorage.ShipFolderPath + Utilities.FilterIllegalFilenameChars(GetShipName()));
    }

    // Enable the mirror line
    private void ShowMirrorLine()
    {
        mirrorLine.GetComponent<Renderer>().enabled = true;
    }

    // Disable the mirror line
    private void HideMirrorLine()
    {
        mirrorLine.GetComponent<Renderer>().enabled = false;
    }

    // Switch the block based on the block index
    private void SwitchBlock(int index, int rotation = 0)
    {
        blockGroupIndex = index;
        // If the index is lower then 0, wrap around
        if (blockGroupIndex < 0) blockGroupIndex = Block.TypeBlocks[currentBlockGroup].Count - 1;
        // If the index is larger then the max blocks in the group, wrap around
        if (blockGroupIndex >= Block.TypeBlocks[currentBlockGroup].Count) blockGroupIndex = 0;

        GameObject.Find("CurrentItemPaginationText").GetComponent<Text>().text = (blockGroupIndex + 1) + "/" + Block.TypeBlocks[currentBlockGroup].Count;
        ChangeSelectedBlock(Block.TypeBlocks[currentBlockGroup][blockGroupIndex], rotation);
    }

    // Gets the ship name input field
    private InputField GetShipNameInputField()
    {
        return GameObject.Find("ShipNameInput").GetComponent<InputField>();
    }

    // Sets the ships name in the input field
    private void SetShipName(string name)
    {
        GetShipNameInputField().text = name;
    }

    // Gets the name from the ship input field
    private string GetShipName()
    {
        return GetShipNameInputField().text;
    }

    // Open the title bar popup with the defined message
    private void ShowTitleBarPopup(string message,float time)
    {
        // Check to see if the popup bar is already open, if so increment the counter of how many
        // Close requests have been made
        if (isTitleBarPopupOpen)
            titleBarPopupCloseRequestCount++;
        else
            titleBarPopupCloseRequestCount = 0;
        // Set that the popup is now open
        isTitleBarPopupOpen = true;
        // Set that the ui is visible
        Utilities.ShowUI(GameObject.Find("TitleBarPopup").GetComponent<CanvasGroup>());
        // Change the text in the popup
        GameObject.Find("TitleBarPopupText").GetComponent<Text>().text = message;
        // Set a timer to close the popup
        Invoke("HideTitleBarPopup", time);
    }
    // closes the title bar popup
   private void HideTitleBarPopup()
   {
        // Decrement the close request popup
        titleBarPopupCloseRequestCount--;
        // If it was the last instance of it being called, then close the popup
        if(titleBarPopupCloseRequestCount<0)
            Utilities.HideUI(GameObject.Find("TitleBarPopup").GetComponent<CanvasGroup>());
   }

    // Check ship to see if it valid before we enter a game
    private bool ValidShip()
    {
        // A ship cant enter if it has no parts or no grids
        if (shipGrids.Count == 0 ||
            shipGrids[0].PartCount() == 0)
        {
            ShowTitleBarPopup("Build A Ship First", 4.0f);
            return false;
        }
        // A ship can also not start if it has parts not attached to the ship
        if (shipGrids.Count > 1)
        {
            ShowTitleBarPopup("Unattached Parts Found", 4.0f);
            return false;
        }
        // Check to see if the ship power
        if (!shipGrids[0].hasPowerSystem())
        {
            ShowTitleBarPopup("Ship Has No Power", 4.0f);
            return false;
        }
        return true;
    }
}
