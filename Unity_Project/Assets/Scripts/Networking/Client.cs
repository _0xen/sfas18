﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using System;

public class Client
{
    // Define a instance of the NetworkClient
    private static NetworkClient networkClient;
    
    public Client(bool localClient, string ip)
    {
        //If the client is a local instance
        if(localClient)
            networkClient = ClientScene.ConnectLocalServer(); // Start a local client
        else
            networkClient = new NetworkClient(); // Else start a normal client
        
        SetupLobyHandles();
        
        // Attach the config for networking
        networkClient.Configure(DataStorage.ConnectionConfig, 1);
        // Init the client's instance of all ship data
        DataStorage.ClientPlayersInstance = new Dictionary<int, DataStorage.ShipLobbyData>();
        // If we are not in a local client, start the client up
        if (!localClient) networkClient.Connect(ip, DataStorage.Port);
    }

    // When the client is closed, disconnect
    public void Close()
    {
        networkClient.Disconnect();
        networkClient.Shutdown();
    }

    // Send the users current ready status to the server
    public void SetReady()
    {
        networkClient.SendByChannel(DataStorage.CustomMsgType.ShipUpdate, DataStorage.ClientData, DataStorage.TCPChannel);
    }

    // Send the ships position information to the server
    public void SendPosiiton(Ship ship)
    {
        // Initialize a new player position message
        DataStorage.PlayerPosition position = new DataStorage.PlayerPosition();
        // Set the messages various variables with the ships position data
        position.id = DataStorage.ClientData.id;
        position.position = ship.GetShipObject().transform.position;
        position.rotation = ship.GetRotationObject().transform.rotation;
        position.thrust = ship.thrust;
        position.momentum = ship.momentum;
        position.flags = ship.flags;
        position.rotationDirection = ship.rotationDirection;
        // Send the message packet to the server
        networkClient.SendByChannel(DataStorage.CustomMsgType.UpdatePosition, position, DataStorage.UDPChannel);
    }

    // Sends a data packet to the server confirming the players death
    public void SendDeath(int id)
    {
        IntegerMessage playerDeath = new IntegerMessage(id);
        networkClient.SendByChannel(DataStorage.CustomMsgType.PlayerDeath, playerDeath, DataStorage.TCPChannel);
    }

    // Called when a block collides with a solar object
    public void SendBlockDamage(int shipId, Vector2Int shipBlock)
    {
        // Create a data packet to pass to the server
        DataStorage.ShipSolarCollisionMessage shipCollisionMessage = new DataStorage.ShipSolarCollisionMessage();
        // Define the parameters that the message needs
        shipCollisionMessage.shipId = shipId;
        shipCollisionMessage.blockX = shipBlock.x;
        shipCollisionMessage.blockY = shipBlock.y;
        // Send the data packet to the server
        networkClient.SendByChannel(DataStorage.CustomMsgType.ShipSolarCollision, shipCollisionMessage, DataStorage.TCPChannel);
    }

    // Called when a collision is sent to the server
    public void SendShipCollision(int ship1Id, int ship2Id, Vector2Int ship1Block, Vector2Int ship2Block, float speed)
    {
        // Create a new instance of the data packet to send to the server
        DataStorage.ShipCollisionMessage shipcollision = new DataStorage.ShipCollisionMessage();
        // Push the collision data to the message
        shipcollision.ship1Id = ship1Id;
        shipcollision.ship2Id = ship2Id;
        // We can not pass the vector2int as a message over a network due to a unity serialization issue
        shipcollision.block1X = ship1Block.x;
        shipcollision.block1Y = ship1Block.y;
        shipcollision.block2X = ship2Block.x;
        shipcollision.block2Y = ship2Block.y;
        shipcollision.speed = speed;
        // Send the data packet to the server
        networkClient.SendByChannel(DataStorage.CustomMsgType.ShipCollision, shipcollision, DataStorage.TCPChannel);
    }

    // Called when a shot is sent to the server
    public void SendShot(int shipID, int blockId, Vector3 position, Quaternion rotation)
    {
        // Create a new instance of the data packet to send to the server
        DataStorage.ShotMessage shotMessage = new DataStorage.ShotMessage();
        // Push the shot data to the message
        shotMessage.shipID = shipID;
        shotMessage.blockID = blockId;
        shotMessage.position = position;
        shotMessage.rotation = rotation;
        // Send the data packet to the server
        networkClient.SendByChannel(DataStorage.CustomMsgType.ProjectileShot, shotMessage, DataStorage.UDPChannel);
    }

    // Called when a shot collision is sent to the server
    public void SendShotCollision(int shotID, int shipID, int weaponblockID, Vector2Int shipBlockPosition)
    {
        // Create a new instance of the data packet to send to the server
        DataStorage.ShotCollisionMessage shotCollidedMessage = new DataStorage.ShotCollisionMessage();
        // Push the collision data to the message
        shotCollidedMessage.shotID = shotID;
        shotCollidedMessage.shipID = shipID;
        shotCollidedMessage.collidedBlockX = shipBlockPosition.x;
        shotCollidedMessage.collidedBlockY = shipBlockPosition.y;
        shotCollidedMessage.blockID = weaponblockID;
        // Send the data packet to the server
        networkClient.SendByChannel(DataStorage.CustomMsgType.ProjectileShotCollision, shotCollidedMessage, DataStorage.UDPChannel);
    }

    // Setup the lobby's packet handlers
    public void SetupLobyHandles()
    {
        // Define the packet handlers
        networkClient.RegisterHandler(MsgType.Connect, DataStorage.LobbyZoneHandles.OnConnect);
        networkClient.RegisterHandler(MsgType.Disconnect, DataStorage.LobbyZoneHandles.OnClientDisconnected);
        networkClient.RegisterHandler(MsgType.Error, OnError);
        // Define lobby specific handlers
        networkClient.RegisterHandler(DataStorage.CustomMsgType.AssignPlayerIdentity, OnAssignPlayerIdentity);
        networkClient.RegisterHandler(DataStorage.CustomMsgType.ShipUpdate, DataStorage.LobbyZoneHandles.OnClientShipUpdate);
        networkClient.RegisterHandler(DataStorage.CustomMsgType.StartGame, DataStorage.LobbyZoneHandles.OnGameStart);
    }

    // Setup the battle zone's packet handlers
    public void SetupBattleZoneHandles()
    {
        // In battle
        networkClient.RegisterHandler(DataStorage.CustomMsgType.UpdatePosition, DataStorage.BattleZoneHandlesInstance.OnPlayerPositionCallbackMethod);
        networkClient.RegisterHandler(DataStorage.CustomMsgType.BlockUpdate, DataStorage.BattleZoneHandlesInstance.OnBlockUpdate);
        networkClient.RegisterHandler(DataStorage.CustomMsgType.PlayerDeath, DataStorage.BattleZoneHandlesInstance.OnPlayerDeath);
        networkClient.RegisterHandler(DataStorage.CustomMsgType.PlayerRespawn, DataStorage.BattleZoneHandlesInstance.OnPlayerRespawn);
        networkClient.RegisterHandler(DataStorage.CustomMsgType.ProjectileShot, DataStorage.BattleZoneHandlesInstance.OnProjectileShot);
    }

    // When the server has assigned us a identity
    private void OnAssignPlayerIdentity(NetworkMessage netMsg)
    {
        // Read the identity information
        IntegerMessage playerIdentity = netMsg.ReadMessage<IntegerMessage>();
        // Initialize the ship lobby data variable
        DataStorage.ClientData = new DataStorage.ShipLobbyData();
        // Set the lobby's various data variables
        DataStorage.ClientData.id = playerIdentity.value;
        DataStorage.ClientData.data = Utilities.LoadShipData(DataStorage.ShipFolderPath + DataStorage.ShipName);
        DataStorage.ClientData.name = DataStorage.ShipName;
        DataStorage.ClientData.ready = false;
        DataStorage.ClientData.connected = true;
        // Send the message packet to the server
        networkClient.SendByChannel(DataStorage.CustomMsgType.ShipUpdate, DataStorage.ClientData, DataStorage.TCPChannel);
    }
    
    // When there is a error, return to the hanger
    private void OnError(NetworkMessage netMsg)
    {
        Utilities.ReturnToHangerFromMultiplayer();
    }
}
