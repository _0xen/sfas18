﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class Server
{
    // Set to true when all clients report ready
    public bool allClientsReady = false;
    // Set true when the users are currently in a game
    public bool GameStarted { get; set; }

    // Stored all recent collisions
    private List<DataStorage.CollisionInstance> collisionInstances;
    // Stored server specific data about each players ship such as who hit them last, etc
    private Dictionary<int, DataStorage.ShipServerData> shipsStatsData;
    // Define a index that will hold the current index for the next shot to be identified by
    private int shotIndex = 0;
    // Stored all recent shots
    private Dictionary<int, DataStorage.ShotMessage> shipShotStorage;

    public Server()
    {
        // Define the lobby specific packet handlers
        NetworkServer.RegisterHandler(MsgType.Connect, OnServerConnect);
        NetworkServer.RegisterHandler(MsgType.Disconnect, OnServerPlayerDisconnect);
        NetworkServer.RegisterHandler(MsgType.Error, OnError);
        NetworkServer.RegisterHandler(DataStorage.CustomMsgType.ShipUpdate, OnServerShipUpdate);

        // Define the battle specific packet handlers
        NetworkServer.RegisterHandler(DataStorage.CustomMsgType.ShipCollision, OnShipCollision);
        NetworkServer.RegisterHandler(DataStorage.CustomMsgType.UpdatePosition, OnUpdatePosition);
        NetworkServer.RegisterHandler(DataStorage.CustomMsgType.PlayerDeath, OnPlayerDeath);
        NetworkServer.RegisterHandler(DataStorage.CustomMsgType.ProjectileShot, OnProjectileShot);
        NetworkServer.RegisterHandler(DataStorage.CustomMsgType.ProjectileShotCollision, OnProjectileShotCollision);
        NetworkServer.RegisterHandler(DataStorage.CustomMsgType.ShipSolarCollision, OnShipSolarCollision);
        // Setup the configuration for the server
        NetworkServer.Configure(DataStorage.ConnectionConfig, 8);
        // Start listening on the provided port
        NetworkServer.Listen(DataStorage.Port);


        // Initialize the player ship data arrays
        DataStorage.ServerPlayersInstance = new Dictionary<int, DataStorage.ShipLobbyData>();
        DataStorage.ServerConnectedPlayers = new Dictionary<NetworkConnection, int>();

        // Initialize the array that is in charge for recording all recent collisions
        collisionInstances = new List<DataStorage.CollisionInstance>();

        shipShotStorage = new Dictionary<int, DataStorage.ShotMessage>();

        GameStarted = false;
    }

    // When the server is closed, disconnect all the clients
    public void Close()
    {
        NetworkServer.DisconnectAll();
        NetworkServer.Shutdown();
    }

    // If the servers client click start
    public void StartGame()
    {
        // Check that all clients are ready
        if (allClientsReady)
        {
            // Send the start packet to all the clients
            NetworkServer.SendByChannelToAll(DataStorage.CustomMsgType.StartGame, new EmptyMessage(), DataStorage.TCPChannel);

            // initialize the shipsStatsData dictionary
            shipsStatsData = new Dictionary<int, DataStorage.ShipServerData>();
            // Loop through all connected players and add them to the shipsStatsData dictionary
            foreach (DataStorage.ShipLobbyData data in DataStorage.ServerPlayersInstance.Values)
            {
                shipsStatsData.Add(data.id,new DataStorage.ShipServerData());
            }
            GameStarted = true;
        }
    }

    // Called once per frame by the battle ground script
    public void Update()
    {
        // Loop backwards through the collision instance array
        for(int i = collisionInstances.Count -1; i >= 0; i--)
        {
            // If a collision instance is older then recordTime + CollisionStorageLifetime < Time.time. then continue
            if (collisionInstances[i].recordTime + DataStorage.CollisionStorageLifetime < Time.time)
            {
                // Remove it from the array
                collisionInstances.RemoveAt(i);
            }
        }

        // Get all keys in the shots storage
        List<int> keys = new List<int>(shipShotStorage.Keys);
        // Loop through the keys
        foreach (int key in keys)
        {
            // When the time that we hold the shot expires, continue
            if (shipShotStorage[key].time + DataStorage.ShotStorageLifetime < Time.time)
            {
                // Remove it from the array
                shipShotStorage.Remove(key);
            }
        }

        // Loop through each players data
        foreach(int key in shipsStatsData.Keys)
        {
            DataStorage.ShipServerData data = shipsStatsData[key];
            // If the current player is dead and the time for there respawn has elapsed
            if (!data.alive && data.deathTime + DataStorage.ShipRespawnTime < Time.time)
            {
                // Set the player to being alive
                data.alive = true;
                DataStorage.RespawnMessage playerRespawn = new DataStorage.RespawnMessage();
                playerRespawn.id = key;
                // This value represents the closest ship to that respawn location
                float closestPlayerForChosenDistance = 0.0f;
                for (int i = 1; i <=4 ; i++)
                {
                    // Create a generally high distance between the ship and the respawn position
                    float closestPlayerDistance = 100.0f;
                    // Get the current respawn location
                    Vector3 respawnLocation = GameObject.Find("Spawn" + i).transform.position;
                    // Loop through all available ships
                    foreach (Ship ship in DataStorage.BattleGroundShips.Values)
                    {
                        // If the ship is currently not alive, skip
                        if (!ship.IsAlive()) continue;
                        // Get the distance between the player and the respawn location
                        float distance = Vector3.Distance(respawnLocation, ship.GetShipObject().transform.position);
                        // If the distance between the last closest player and the current one is less, continue
                        if (closestPlayerDistance > distance)
                            closestPlayerDistance = distance;// Set the closest player to the distance
                    }
                    // If the last found closest player for the last spawn point is closer then the one we just found, use the one we just found
                    if(closestPlayerForChosenDistance < closestPlayerDistance)
                    {
                        closestPlayerForChosenDistance = closestPlayerDistance;
                        playerRespawn.position = respawnLocation;
                    }
                }
                // Send the data packet to the server
                NetworkServer.SendByChannelToAll(DataStorage.CustomMsgType.PlayerRespawn, playerRespawn, DataStorage.TCPChannel);
            }
            else
            {
                // If time between the last damage dealt to the player has elapsed, continue
                if(data.damageTime + DataStorage.TimeBeforeBlockRespawn < Time.time)
                {
                    // Add time to the damage timer
                    data.damageTime += DataStorage.BlockRespawnDelay;
                    // Loop through all the ship parts
                    foreach (ShipPart part in DataStorage.BattleGroundShips[key].GetParts())
                    {
                        // If the part is damaged or destroyed continue
                        if (part.IsActive() && part.block.CurrentHealth < part.block.Health ||
                            !part.IsActive() && Utilities.GridTouchingPart(DataStorage.BattleGroundShips[key], part)) 
                        {
                            // Add max health to the part
                            UpdateBlockHealth(key, part.GetPosition(), part.block.Health);
                            break;
                        }
                    }

                }
            }
        }
    }

    // When a client connects to the server
    private void OnServerConnect(NetworkMessage netMsg)
    {
        // Loop through all 4 player slots or just return if the game has started
        for (int i = 1; i <= 4 && !GameStarted; i++)
        {
            // if the connected players dictionary dose not contain the current index
            if (!DataStorage.ServerConnectedPlayers.ContainsValue(i))
            {
                // Add the players connection data and id to the connected players dictionary
                DataStorage.ServerConnectedPlayers.Add(netMsg.conn, i);
                // Create a new player identity message
                IntegerMessage playerIdentity = new IntegerMessage(i);
                // Loop through all players and send them a update saying that they have connected
                foreach (DataStorage.ShipLobbyData shipData in DataStorage.ServerPlayersInstance.Values)
                {
                    netMsg.conn.SendByChannel(DataStorage.CustomMsgType.ShipUpdate, shipData, DataStorage.TCPChannel);
                }
                // Send the user that has just connected there identity
                netMsg.conn.SendByChannel(DataStorage.CustomMsgType.AssignPlayerIdentity, playerIdentity, DataStorage.TCPChannel);
                // Check to see if all users are ready
                ServerCheckAllUsersReady();
                return;
            }
        }
        // When there is no lobby space or the game has already begun, disconnect
        netMsg.conn.Disconnect();
    }

    // When a player disconnects
    private void OnServerPlayerDisconnect(NetworkMessage netMsg)
    {
        // If the connected players array dose not contain the disconnecting players connection, return.
        // This is just a fail safe as it should never happen
        if (!DataStorage.ServerConnectedPlayers.ContainsKey(netMsg.conn)) return;
        // Get the index of the connected player
        int index = DataStorage.ServerConnectedPlayers[netMsg.conn];
        // Remove the player from the connected players array
        DataStorage.ServerConnectedPlayers.Remove(netMsg.conn);
        // Set that the player is not connected
        DataStorage.ServerPlayersInstance[index].connected = false;
        // Send a update packet to all clients saying that the client is no longer connected
        NetworkServer.SendByChannelToAll(DataStorage.CustomMsgType.ShipUpdate, DataStorage.ServerPlayersInstance[index], DataStorage.TCPChannel);
        // Remove the players ship and connection data from the array
        DataStorage.ServerPlayersInstance.Remove(index);
        // If the game has not already started, update the user ready check
        if(!GameStarted) ServerCheckAllUsersReady();
    }

    // When a user sends a update
    private void OnServerShipUpdate(NetworkMessage netMsg)
    {
        // Read the update packet
        DataStorage.ShipLobbyData ship = netMsg.ReadMessage<DataStorage.ShipLobbyData>();
        // If the user this update mentions dose not exist, add it
        if (!DataStorage.ServerPlayersInstance.ContainsKey(ship.id))
            DataStorage.ServerPlayersInstance.Add(ship.id, ship);
        else // Else update the instance of the ship
            DataStorage.ServerPlayersInstance[ship.id] = ship;
        // Check to see if the users are all ready
        ServerCheckAllUsersReady();
        // Send the update to all other users
        NetworkServer.SendByChannelToAll(DataStorage.CustomMsgType.ShipUpdate, ship, DataStorage.TCPChannel);
    }

    // Used to check if all the users are ready
    void ServerCheckAllUsersReady()
    {
        // If there is no connected players
        if (DataStorage.ServerPlayersInstance.Values.Count == 0)
        {
            // Set that they are not ready
            allClientsReady = false;
            return;
        }
        // Set that all the clients are ready, until proven differently
        allClientsReady = true;
        // Loop through all ship data
        foreach (DataStorage.ShipLobbyData shipData in DataStorage.ServerPlayersInstance.Values)
        {
            // If a ship was found not to be ready, mark it as false
            if (!shipData.ready) allClientsReady = false;
        }
        // If all the clients are ready
        if (allClientsReady) // Set the ready button text to start
            GameObject.Find("ReadyButtonText").GetComponent<Text>().text = "Start";
        else if (DataStorage.ClientData != null && DataStorage.ClientData.ready) // Else to waiting
            GameObject.Find("ReadyButtonText").GetComponent<Text>().text = "Waiting";
    }

    // If there is a error on the server side, return to the hanger
    private void OnError(NetworkMessage netMsg)
    {
        Utilities.ReturnToHangerFromMultiplayer();
    }

    // In Game Specific Callbacks
    private void OnUpdatePosition(NetworkMessage netMsg)
    {
        NetworkServer.SendByChannelToAll(DataStorage.CustomMsgType.UpdatePosition, netMsg.ReadMessage<DataStorage.PlayerPosition>(), DataStorage.UDPChannel);
    }

    // Called when we receive a shot request
    private void OnProjectileShot(NetworkMessage netMsg)
    {
        // Read in the message passed to the server
        DataStorage.ShotMessage shotMessage = netMsg.ReadMessage<DataStorage.ShotMessage>();
        // Increment the shot index
        shotIndex++;
        // Store the shot index into the data packet
        shotMessage.shotID = shotIndex;
        // Send the shot request to all the clients
        NetworkServer.SendByChannelToAll(DataStorage.CustomMsgType.ProjectileShot, shotMessage, DataStorage.UDPChannel);
        // Set the current time in the data packet
        shotMessage.time = Time.time;
        // Store the data packet away so we have some way of validating the shots
        shipShotStorage.Add(shotIndex, shotMessage);
    }

    // Called when a projectile has hit a ship
    private void OnProjectileShotCollision(NetworkMessage netMsg)
    {
        // Read in the data packet
        DataStorage.ShotCollisionMessage shotCollisionMessage = netMsg.ReadMessage<DataStorage.ShotCollisionMessage>();
        // If the shot storage contains the shot
        if (shipShotStorage.ContainsKey(shotCollisionMessage.shotID))
        {
            // If the block referenced in the data packet is not a weapon block, return
            if (!(Block.Blocks[shotCollisionMessage.blockID] is WeaponBlock)) return;
            // Get the damage of the weapon
            int damage = ((WeaponBlock)Block.Blocks[shotCollisionMessage.blockID]).BaseDamage;
            // Send a update packet to all clients saying how much damage was delta
            UpdateBlockHealth(shotCollisionMessage.shipID,new Vector2Int(shotCollisionMessage.collidedBlockX, shotCollisionMessage.collidedBlockY),-damage);
            // Store who damaged the ship last
            shipsStatsData[shotCollisionMessage.shipID].lastDamage = shipShotStorage[shotCollisionMessage.shotID].shipID;
            // Record the time that the ship last took damage
            shipsStatsData[shotCollisionMessage.shipID].damageTime = Time.time;
            // Remove the shot instance from the shot array
            shipShotStorage.Remove(shotCollisionMessage.shotID);
        }

    }
    

    // Called when a client has reported that it has died
    private void OnPlayerDeath(NetworkMessage netMsg)
    {
        IntegerMessage playerID = netMsg.ReadMessage<IntegerMessage>();
        DataStorage.PlayerDeathMessage deathMessage = new DataStorage.PlayerDeathMessage();
        deathMessage.deadPlayerID = playerID.value;
        deathMessage.killerID = shipsStatsData[playerID.value].lastDamage;
        // Relay the death packet to all other clients
        NetworkServer.SendByChannelToAll(DataStorage.CustomMsgType.PlayerDeath, deathMessage, DataStorage.TCPChannel);
        // Set that they are dead in the shipsStatsData dictionary and add a time of death
        shipsStatsData[playerID.value].alive = false;
        shipsStatsData[playerID.value].deathTime = Time.time;
        Debug.Log("Player " + playerID.value + "Died");
    }

    // Called when the client's ship has hit a solar object
    private void OnShipSolarCollision(NetworkMessage netMsg)
    {
        // Read the message sent from the client
        DataStorage.ShipSolarCollisionMessage shipCollisionMessage = netMsg.ReadMessage<DataStorage.ShipSolarCollisionMessage>();
        // Record the last times that the ship were damaged
        shipsStatsData[shipCollisionMessage.shipId].damageTime = Time.time;
        // Call the update health function was we want to destroy the block
        UpdateBlockHealth(
            shipCollisionMessage.shipId, // Pass the ships id
            new Vector2Int(shipCollisionMessage.blockX, shipCollisionMessage.blockY), // Pass the ships coordinates
            -DataStorage.BattleGroundShips[shipCollisionMessage.shipId].GetPart(shipCollisionMessage.blockX, shipCollisionMessage.blockY).block.Health // Pass the ships health as we want to destroy it
            );
    }

    // Called when a client has reported a collision
    private void OnShipCollision(NetworkMessage netMsg)
    {
        // Create a instance of the collision structure
        DataStorage.CollisionInstance collisionInstance = new DataStorage.CollisionInstance();
        // Add the message to the structure
        collisionInstance.messageInstance = netMsg.ReadMessage<DataStorage.ShipCollisionMessage>();
        // Loop through all collision instances
        foreach(DataStorage.CollisionInstance coll in collisionInstances)
        {
            // if we find one that matches exactly, return. 
            // I have to check through each variable as I was unable to find a better way
            if(coll.messageInstance.ship1Id == collisionInstance.messageInstance.ship1Id &&
                coll.messageInstance.ship2Id == collisionInstance.messageInstance.ship2Id &&
                coll.messageInstance.block1X == collisionInstance.messageInstance.block1X &&
                coll.messageInstance.block1Y == collisionInstance.messageInstance.block1Y &&
                coll.messageInstance.block2X == collisionInstance.messageInstance.block2X &&
                coll.messageInstance.block2Y == collisionInstance.messageInstance.block2Y)
            {
                return;
            }
        }
        // Define what time it was received at
        collisionInstance.recordTime = Time.time;
        // Push the collision instance to the list
        collisionInstances.Add(collisionInstance);

        // If the collision speed is above the max speed before damage
        if(collisionInstance.messageInstance.speed > DataStorage.MaxSpeedBeforeDamage)
        {
            // Set both players to be the last ones to damage each-other
            shipsStatsData[collisionInstance.messageInstance.ship1Id].lastDamage = collisionInstance.messageInstance.ship2Id;
            shipsStatsData[collisionInstance.messageInstance.ship2Id].lastDamage = collisionInstance.messageInstance.ship1Id;
            // Record the last times that the ships were damaged
            shipsStatsData[collisionInstance.messageInstance.ship1Id].damageTime = Time.time;
            shipsStatsData[collisionInstance.messageInstance.ship2Id].damageTime = Time.time;

            // EG: 0.23f - 1.0f = 0.13f
            // 0.13f * 100 = 13.0f   =   13 points of damage
            int damage = (int)(((collisionInstance.messageInstance.speed - DataStorage.MaxSpeedBeforeDamage) * DataStorage.CollisionDamageScalar) * 100);
            // Send block damage updates to the clients
            UpdateBlockHealth(collisionInstance.messageInstance.ship1Id, new Vector2Int(collisionInstance.messageInstance.block1X, collisionInstance.messageInstance.block1Y), -damage);
            UpdateBlockHealth(collisionInstance.messageInstance.ship2Id, new Vector2Int(collisionInstance.messageInstance.block2X, collisionInstance.messageInstance.block2Y), -damage);
        }
    }

    // Called when a blocks health has been changed
    void UpdateBlockHealth(int shipId, Vector2Int blockPos, int healthToAdd)
    {
        // Get the current health of the block
        int currentHealth = DataStorage.BattleGroundShips[shipId].GetPart(blockPos.x, blockPos.y).block.CurrentHealth;
        // Get the max health of the block
        int maxHealth = DataStorage.BattleGroundShips[shipId].GetPart(blockPos.x, blockPos.y).block.Health;



        // If the health is above its max health now, reduce it
        if (currentHealth + healthToAdd > maxHealth)
        {
            currentHealth = maxHealth;
        }else
        // If the health is below 0, set it to 0
        if (currentHealth + healthToAdd < 0)
        {
            currentHealth = 0;
        }
        else
        {
            // Add the health of the block
            currentHealth += healthToAdd;
        }

        // Create a instance of the block update message
        DataStorage.BlockUpdate blockUpdate = new DataStorage.BlockUpdate();
        // If the blocks health drops below 0, it is not alive
        blockUpdate.alive = currentHealth > 0;
        // Pass the x & y pos of the block to the message
        blockUpdate.blockX = blockPos.x;
        blockUpdate.blockY = blockPos.y;
        // Store the current health of the block
        blockUpdate.health = currentHealth;
        // Work out the health changed by taking the stored current health away from the new health
        blockUpdate.healthChange = currentHealth - DataStorage.BattleGroundShips[shipId].GetPart(blockPos.x, blockPos.y).block.CurrentHealth;
        // Pass the ship id to the message
        blockUpdate.shipId = shipId;
        // Send the block update to all the clients
        NetworkServer.SendByChannelToAll(DataStorage.CustomMsgType.BlockUpdate, blockUpdate, DataStorage.TCPChannel);

        // Set the servers local copy's health to the current health right away as we may get another update before the client side updates it
        DataStorage.BattleGroundShips[shipId].GetPart(blockPos.x, blockPos.y).block.CurrentHealth = currentHealth;
    }
}
