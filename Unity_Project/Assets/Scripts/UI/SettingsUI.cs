﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsUI : MonoBehaviour
{
    // Store the screens aspect ratio
    private float aspectRatio;
    // Store the slider instance
    private Slider musicVolumeSlider;
    // Store the slider instance
    private Slider sfxVolumeSlider;
    // Store a instance of the audio source
    private AudioSource musicSource;
    // Store the close button instance
    private Button closeButton;
    // Store a instance of the ResolutionDropdown
    private Dropdown resolutionDropdown;
    // Used to store the available resolutions
    private List<Resolution> screenResolutions;
    private List<string> screenResolutionsAsText;
    // Store a instance of the full screen toggle
    private Toggle fullscreenToggle;
    // Used to store the sfx object instance
    private SFXController SFXInstance;
    // Use this for initialization
    void Start ()
    {
        // Generate the aspect ratio of the screen res
        aspectRatio = (float)Screen.currentResolution.width / (float)Screen.currentResolution.height;
        // Get the sfxInstance
        SFXInstance = GameObject.Find("SFXController").GetComponent<SFXController>();
        // Set the music source
        musicSource = GameObject.Find("MainMusic").GetComponent<AudioSource>();
        // Get the music volume slider
        musicVolumeSlider = GameObject.Find("MusicVolumeSlider").GetComponent<Slider>();
        // Get the sfx volume slider
        sfxVolumeSlider = GameObject.Find("SFXVolumeSlider").GetComponent<Slider>();
        // Get the close button instance
        closeButton = GameObject.Find("SettingsCloseButton").GetComponent<Button>();
        // Get the resolution drop down instance
        resolutionDropdown = GameObject.Find("ResolutionDropdown").GetComponent<Dropdown>();
        // Get the full screen toggle instance
        fullscreenToggle = GameObject.Find("FullscreenToggle").GetComponent<Toggle>();
        
        // Clear the drop down
        resolutionDropdown.ClearOptions();
        // Initialize the resolution arrays
        screenResolutions = new List<Resolution>();
        screenResolutionsAsText = new List<string>();
        // Define the currently used resolution
        int usedResolution = -1;
        // Loop through the screen resolutions and add them to the array
        for (int i = 0 ; i < Screen.resolutions.Length; i++)
        {
            // Get the resolution
            Resolution res = Screen.resolutions[i];
            // Create the text instance of the resolution
            string text = res.width + "X" + res.height + " " + res.refreshRate + "Hz";
            float currentResolutionAspectRatio = (float)res.width / (float)res.height;
            // If we don't contain the resolution, continue
            if (!screenResolutionsAsText.Contains(text) && res.width >= DataStorage.MimimumScreenRes.x && res.height >= DataStorage.MimimumScreenRes.y
                // Check to see if both aspect ratios are in the range of each other
                && aspectRatio + 0.0000001f > currentResolutionAspectRatio && aspectRatio < currentResolutionAspectRatio + 0.0000001f)
            {
                // Add the resolution to both arrays
                screenResolutions.Add(res);
                screenResolutionsAsText.Add(text);
                // If the resolution matches, define the used resolution
                if (res.width == Screen.width && res.height == Screen.height)
                    usedResolution = i;
            }
        }
        // Add all resolutions to the drop down
        resolutionDropdown.AddOptions(screenResolutionsAsText);
        resolutionDropdown.value = usedResolution;

        // Set the is on state of the full screen toggle to the screens current full screen state
        fullscreenToggle.isOn = Screen.fullScreen;

        // Set the musics default volume
        musicVolumeSlider.value = PlayerPrefs.GetFloat("MusicVolume", 0.5f);
        // Add listener to the slider for the value change
        musicVolumeSlider.onValueChanged.AddListener(delegate {
            VolumeSliderChange();
        });

        // Set the musics default volume
        sfxVolumeSlider.value = PlayerPrefs.GetFloat("SFXVolume", 0.5f);
        // Add listener to the slider for the value change
        sfxVolumeSlider.onValueChanged.AddListener(delegate {
            SFXSliderChange();
        });


        // Add listener to the resolution change drop down
        resolutionDropdown.onValueChanged.AddListener(delegate {
            Resolution res = screenResolutions[resolutionDropdown.value];
            Screen.SetResolution(res.width, res.height, fullscreenToggle.isOn, res.refreshRate);
            // Play the block place effect
            SFXInstance.PlaySFX(DataStorage.SFXClips[0], SFXController.SFXSource.Channel1);
        });
        // Add listener for the close button
        closeButton.onClick.AddListener(delegate {
            Utilities.HideUI(gameObject.GetComponent<CanvasGroup>());
        });
        // Add listener for the full screen toggle
        fullscreenToggle.onValueChanged.AddListener(delegate {
            // If toggled, get the current screen resolution
            Resolution res = screenResolutions[resolutionDropdown.value];
            // Set the resolution to the current full screen state
            Screen.SetResolution(res.width, res.height, fullscreenToggle.isOn, res.refreshRate);
            // Play the block place effect
            SFXInstance.PlaySFX(DataStorage.SFXClips[0], SFXController.SFXSource.Channel1);
        });
        if(Screen.width < DataStorage.MimimumScreenRes.x ||
            Screen.height < DataStorage.MimimumScreenRes.y)
        {
            Screen.SetResolution(Screen.width, Screen.height, fullscreenToggle.isOn);
        }
    }
    // Called when the value of the volume slider changes
    public void VolumeSliderChange()
    {
        // Set the value of the volume into unity's storage system
        PlayerPrefs.SetFloat("MusicVolume", musicVolumeSlider.value);
        // Update the volume
        musicSource.volume = musicVolumeSlider.value;
    }

    // Called when the value of the sfx slider changes
    public void SFXSliderChange()
    {
        // Set the value of the volume into unity's storage system
        PlayerPrefs.SetFloat("SFXVolume", sfxVolumeSlider.value);
    }
}
