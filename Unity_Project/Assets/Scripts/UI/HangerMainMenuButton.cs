﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HangerMainMenuButton : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        // Add a click listener to the menu button
        gameObject.GetComponent<Button>().onClick.AddListener(delegate {
            Utilities.HideUI(GameObject.Find("SettingsModel").GetComponent<CanvasGroup>());
            Utilities.ShowUI(GameObject.Find("HangerMenuModel").GetComponent<CanvasGroup>());
            Utilities.HideUI(GameObject.Find("ConfirmationModel").GetComponent<CanvasGroup>());
            // Play the ui click sfx
            GameObject.Find("SFXController").GetComponent<SFXController>().PlaySFX(DataStorage.SFXClips[0], SFXController.SFXSource.Channel1);
        });
    }
}
