﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HangerMenuUI : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        // Initialize the main menus buttons
        GameObject.Find("MainMenuSettings").GetComponent<Button>().onClick.AddListener(delegate {
            // Hide the main menu UI and show the settings
            Utilities.ShowUI(GameObject.Find("SettingsModel").GetComponent<CanvasGroup>());
            Utilities.HideUI(GameObject.Find("HangerMenuModel").GetComponent<CanvasGroup>());
            Utilities.HideUI(GameObject.Find("ConfirmationModel").GetComponent<CanvasGroup>());
            // Play the ui click sfx
            GameObject.Find("SFXController").GetComponent<SFXController>().PlaySFX(DataStorage.SFXClips[0], SFXController.SFXSource.Channel1);
        });
        GameObject.Find("MainMenuBackToMainMenuButton").GetComponent<Button>().onClick.AddListener(delegate {
            // Switch scenes to the main menu
            SceneManager.LoadScene("MainMenu");
            // Play the ui click sfx
            GameObject.Find("SFXController").GetComponent<SFXController>().PlaySFX(DataStorage.SFXClips[0], SFXController.SFXSource.Channel1);
        });
        GameObject.Find("MainMenuBackButton").GetComponent<Button>().onClick.AddListener(delegate {
            // Hide the main menu UI and show the settings
            Utilities.HideUI(GameObject.Find("HangerMenuModel").GetComponent<CanvasGroup>());
            // Play the ui click sfx
            GameObject.Find("SFXController").GetComponent<SFXController>().PlaySFX(DataStorage.SFXClips[0], SFXController.SFXSource.Channel1);
        });
    }
}
