﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BattleMenuUI : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        // Initialize the main menus buttons
        GameObject.Find("BattleMenuSettings").GetComponent<Button>().onClick.AddListener(delegate {
            // Hide the main menu UI and show the settings
            Utilities.ShowUI(GameObject.Find("SettingsModel").GetComponent<CanvasGroup>());
            Utilities.HideUI(GameObject.Find("BattleMenuModel").GetComponent<CanvasGroup>());
        });
        GameObject.Find("BattleMenuBackToHangerButton").GetComponent<Button>().onClick.AddListener(delegate {
            // Switch scenes to the Hanger
            Utilities.ReturnToHangerFromMultiplayer();
        });
        GameObject.Find("BattleMenuBackButton").GetComponent<Button>().onClick.AddListener(delegate {
            // Hide the main menu UI and show the settings
            Utilities.HideUI(GameObject.Find("BattleMenuModel").GetComponent<CanvasGroup>());
        });
    }
}

