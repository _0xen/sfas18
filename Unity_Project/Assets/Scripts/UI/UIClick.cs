﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIClick : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        GetComponent<Button>().onClick.AddListener(delegate {
            // Play the UI click SFX
            GameObject.Find("SFXController").GetComponent<SFXController>().PlaySFX(DataStorage.SFXClips[0], SFXController.SFXSource.Channel1);
        });
    }
}
