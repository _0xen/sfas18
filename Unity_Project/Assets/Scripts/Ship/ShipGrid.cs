﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipGrid
{
    // Define a list that will contain all the ship parts
    List<ShipPart> parts;
    // Define the total cost of the grid
    private int shipCost = 0;
    // Define the total health of the grid
    private int totalHealth = 0;
    // Define the current health of the grid
    private int currentHealth = 0;
    // Define the total generated power of the grid
    private float maxGeneratedPower = 0.0f;
    // Define the total stored power of the grid
    private float maxStoredPower = 0.0f;
    // Define the total drawn power of the grid
    private float maxDrawnPower = 0.0f;
    // Define the thruster speeds in the 4 possible directions
    private float[] thrusterSpeed = new float[4] { 0.0f, 0.0f, 0.0f, 0.0f };
    // Define the total weight of the grid
    private int totalWeight = 0;
    // Define the order that the object types should be powered
    private List<System.Type> powerHierarchy;
    // Store the thrusters that are pointing each direction
    private List<ShipPart>[] thrusterDirections;

    public ShipGrid()
    {
        // Initialize the parts list
        parts = new List<ShipPart>();
        // Initialize the list that orders the object types should be powered
        powerHierarchy = new List<System.Type>();
        // Add the order that blocks should be powered in
        powerHierarchy.Add(typeof(EngineBlock));
        powerHierarchy.Add(typeof(WeaponBlock));
        powerHierarchy.Add(typeof(PoweredBlock)); // Add the general powered block last as these might be objects not currently added yet
        // Initialize the 4 thrust directions
        thrusterDirections = new List<ShipPart>[4];
        // Loop through the 4 thruster directions and initialize the lists inside of them
        for (int i = 0; i < 4; i++)
            thrusterDirections[i] = new List<ShipPart>();
    }

    // Get all ship parts
    public List<ShipPart> GetParts()
    {
        return parts;
    }

    // get part based on quardinate
    public ShipPart GetPart(int x, int y)
    {
        // Get the parts position by vector2int
        Vector2Int requestedPartLocation = new Vector2Int(x, y);
        // Loop through all parts
        foreach (ShipPart part in parts)
        {
            // if the parts matches, return it
            if (part.GetPosition() == requestedPartLocation)
                return part;
        }
        return null;
    }

    // Add a part to the grid
    public void AddPart(ShipPart part)
    {
        // Add a part to the main parts list
        parts.Add(part);
        // Add to the overall grids stats
        AddStats(part);
    }
    public void RemovePart(ShipPart part)
    {
        // Remove the part from the main parts list
        parts.Remove(part);
        // Remove from the grids overall stats
        RemoveStats(part);
    }

    public void AddStats(ShipPart part, bool addingPart = true)
    {
        // Add to the total ship cost
        shipCost += part.block.Cost;
        if (addingPart)
        {
            // Add the blocks health to the total cost
            totalHealth += part.block.Health;
            currentHealth += part.block.CurrentHealth;
        }
        // Add to the total weight
        totalWeight += part.block.Weight;
        // Check to see if the part is also powered
        if (part.block is PoweredBlock)
        {
            // Cast the part as a powered block
            PoweredBlock block = (PoweredBlock)part.block;
            // Add to the total generated power
            maxGeneratedPower += block.MaxOutputPower;
            // Add to the total stored power
            maxStoredPower += block.MaxStoredPower;
            // Add to the total max power input
            maxDrawnPower += block.MaxInputPower;
            // Check to see if the part is also a engine block
            if (part.block is EngineBlock)
            {
                // Calculate the direction that the thruster is currently pointing
                int direction = part.rotation < 5 ? part.rotation % 4 : (4 - (part.rotation % 4) % 4);
                // Add the thruster to the direction list
                thrusterDirections[direction].Add(part);
            }
        }
    }

    public void RemoveStats(ShipPart part, bool removeingPart = true)
    {
        // Remove from the ships cost
        shipCost -= part.block.Cost;
        if (removeingPart)
        {
            // Remove from the total health
            totalHealth -= part.block.Health;
            currentHealth -= part.block.CurrentHealth;
        }
        // Remove from the ships weight
        totalWeight -= part.block.Weight;
        // Check to see if the part is also powered
        if (part.block is PoweredBlock)
        {
            // Cast the part as a powered block
            PoweredBlock block = (PoweredBlock)part.block;
            // Remove from the total generated power
            maxGeneratedPower -= block.MaxOutputPower;
            // Remove from the total stored power
            maxStoredPower -= block.MaxStoredPower;
            // Remove from the total input power
            maxDrawnPower -= block.MaxInputPower;
            // Check to see if the part is also a engine block
            if (part.block is EngineBlock)
            {
                // Calculate the direction that the thruster is currently pointing
                int direction = part.rotation < 5 ? part.rotation % 4 : (4 - (part.rotation % 4) % 4);
                // Remove the thruster to the direction list
                thrusterDirections[direction].Remove(part);
            }
        }
    }

    // Get the total part count
    public int PartCount()
    {
        return parts.Count;
    }

    // Get the current grid cost
    public int GetShipCost()
    {
        return shipCost;
    }

    // Get the total health
    public int GetHealth()
    {
        return totalHealth;
    }

    // Return the current health of the grid
    public int GetCurrentHealth()
    {
        return currentHealth;
    }

    // Add to the current health
    public void AddCurrentHealth(int health)
    {
        currentHealth += health;
    }

    // Set the current health
    public void SetCurrentHealth(int health)
    {
        currentHealth = health;
    }

    // Get the grids generated power
    public float GetGeneratedPower()
    {
        return maxGeneratedPower;
    }

    // Get the grids stored power
    public float GetStoredPower()
    {
        return maxStoredPower;
    }

    // Get the grids drawn power
    public float GetDrawnPower()
    {
        return maxDrawnPower;
    }

    // Check to see if the grid is powered
    public bool hasPowerSystem()
    {
        return GetGeneratedPower() > float.Epsilon;
    }

    // Get the thruster speed in x direction
    public float GetThrusterSpeed(int direction)
    {
        return thrusterSpeed[direction];
    }

    // Get the total weight of the ship
    public int GetWeight()
    {
        return totalWeight;
    }

    // Update the power system and manage what is powering what
    public void UpdatePowerSystems(bool inHanger)
    {
        // Get the currently generated power
        float currentPower = GetGeneratedPower();
        // Loop through each object type in the hierarchy of objects that should be powered
        foreach (System.Type objectType in powerHierarchy)
        {
            // Loop through each ship part
            foreach (ShipPart part in parts)
            {
                // If the block is not active we skip it
                if (!part.IsActive())
                    continue;
                // If the current part type matches the one in the hierarchy
                if (part.block.GetType() == objectType)
                {
                    // We cast the part and initialize a new variable
                    PoweredBlock poweredPart = (PoweredBlock)part.block;
                    // We reset the input and output variables to there default
                    poweredPart.ResetPower();
                    // If the block can take in power
                    if (poweredPart.IsType(PoweredBlock.PoweredBlockType.Input))
                    {
                        // If we still have power that we can provide
                        if (currentPower > 0.0f + float.Epsilon)
                        {
                            // Get the current input power
                            float inputPower = poweredPart.MaxInputPower;
                            // If not enough power can be provided to the part, provide as much as available
                            if (inputPower >= currentPower)
                            {
                                inputPower = currentPower;
                            }
                            // If the part can also store power
                            // When in hanger we do not want the power usage to be affected by stored power
                            if (!inHanger &&poweredPart.IsType(PoweredBlock.PoweredBlockType.Storage))
                            {
                                // Get the max stored power by the part
                                float maxStoragePower = poweredPart.MaxStoredPower;
                                // Get the current stored power
                                float currentSoragePower = poweredPart.CurrentStoredPower;
                                // For as long as the current stored power is less then the max storage power
                                if(currentSoragePower < maxStoragePower)
                                {
                                    // If inputing the current power request will be too much for the storage
                                    // Then calculate how much we need to charge it by
                                    if(currentSoragePower + inputPower > maxStoragePower)
                                    {
                                        // Store the remaining required power
                                        poweredPart.StorePower(maxStoragePower - currentSoragePower);
                                        // Deduct the power that the storage was charged by
                                        currentPower -= maxStoragePower - currentSoragePower;
                                    }
                                    else
                                    {
                                        // Store the max input available
                                        poweredPart.StorePower(inputPower);
                                        // Deduct the max input
                                        currentPower -= inputPower;
                                    }
                                }
                            }
                            else // Cant store power
                            {
                                // Deduct from the input power
                                currentPower -= inputPower;
                                // Input the available power
                                poweredPart.InputPower(currentPower);
                            }
                            // Even though the block might only be getting a small
                            // amount of power
                            poweredPart.Powered = true;
                            part.SetAnimationGroup(1);
                        }
                        else // Else, Since there is no more power, we wet it to un powered
                        {
                            poweredPart.Powered = false;
                            part.SetAnimationGroup(0);
                        }
                    }
                }
            }
        }
    }

    // Update the grids animations based on the flags provided
    public void UpdateAnimation(DataStorage.ShipFlags flags, int tick)
    {
        // Loop through all the ship parts
        foreach (ShipPart part in parts)
        {
            // If the block is powered
            if (part.block is PoweredBlock)
            {
                // Cast a new instance of the block as a powered block
                PoweredBlock poweredBlock = (PoweredBlock)part.block;
                // If the block is powered
                if (poweredBlock.isPowered())
                {
                    // Set the animation
                    part.SetAnimationGroup(1);
                    // If the block is a engine
                    if (part.block is EngineBlock)
                    {
                        // Loop through the thruster directions
                        for(int i = 0; i < 4; i++)
                        {
                            // If the thrust direction contains the current part and its thrusting in that direction
                            if (thrusterDirections[i].Contains(part) &&
                                (flags & DataStorage.thrusterFlags[i]) == DataStorage.thrusterFlags[i])
                            {
                                // Set the animation
                                part.SetAnimationGroup(2);
                            }
                        }
                    }
                    else if (part.block is WeaponBlock)
                    {
                        // Cast the part block as a weapon block
                        WeaponBlock weaponBlock = ((WeaponBlock)part.block);
                        // If the part is shooting then we can infer that the part is in animation group 2
                        if ((flags & DataStorage.ShipFlags.Shooting) == DataStorage.ShipFlags.Shooting)
                        {
                            part.SetAnimationGroup(2);
                        }
                        // If we are a storage weapon, then continue
                        if (weaponBlock.IsType(PoweredBlock.PoweredBlockType.Storage))
                        {
                            part.SetAnimationGroup(2);
                            // Calculate the frame of the weapons animation based on the amount of animation frames and the weapons charge
                            int frame = (int)(part.block.AnimationFrames.GetLength(1) * (weaponBlock.CurrentStoredPower / weaponBlock.MaxStoredPower));
                            // If the frame is larger then the amount of available frames, continue
                            // OR set it to the max
                            if (!(frame < part.block.AnimationFrames.GetLength(1)))
                                frame = part.block.AnimationFrames.GetLength(1) - 1;
                            // Set the texture based on the current tick
                            part.SetTextureFromGroup(frame % part.block.AnimationFrames.GetLength(1));
                            // Update the objects texture
                            part.UpdateObject();
                            // Continue with next loop
                            continue;
                        }
                    }
                }
                else
                {
                    // Set the animation
                    part.SetAnimationGroup(0);
                }
            }
            // Set the texture based on the current tick
            part.SetTextureFromGroup(tick % part.block.AnimationFrames.GetLength(1));
            // Update the objects texture
            part.UpdateObject();    
        }
    }

    // Called when a block update event is triggered
    public void UpdateBlock(bool enabled, Vector2Int blockPosition, int healthChange)
    {
        lock (DataStorage.Lock_)
        {
            UpdateBlock(enabled, GetPart(blockPosition.x, blockPosition.y), healthChange);
        }
    }

    // Called when a block update event is triggered
    // It will loop through blocks if a block has been disabled and find parts that are detached and remove them
    public void UpdateBlock(bool enabled, ShipPart part, int healthChange, bool recursive = false)
    {
        lock (DataStorage.Lock_)
        {
            // If the enabled state is different to the active state of the block
            if (part.IsActive() != enabled)
            {
                // Toggle the blocks state
                part.SetActive(enabled);
                // If the block is enabled
                if (enabled)
                {
                    // Add the parts stats back to the grid
                    AddStats(part, false);
                }
                else
                {
                    // If we are in a recursed version of the function, only adjust the health of the grid
                    if (!recursive)
                    {
                        // Create a holder for the grids that are spawned from the block being removed
                        List<ShipGrid> grids = new List<ShipGrid>();
                        // Loop through the ship parts and add them to a new sudo ship
                        // This is to get them split into the different grids
                        for (int i = 0; i < parts.Count; i++)
                        {
                            // If the part is already inactive, continue
                            if (!parts[i].IsActive())
                                continue;
                            // Add the part to the grids
                            Utilities.AddPartToShip(grids, parts[i]);
                        }
                        // Create a var to store the grid with the largest cost
                        ShipGrid gridToSave = null;
                        // If a grid was generated
                        if (grids.Count > 0)
                        {
                            // Loop through the grids
                            foreach (ShipGrid grid in grids)
                            {
                                // If there is not currently a grid to save, set the current grid to be it
                                if (gridToSave == null)
                                {
                                    gridToSave = grid;
                                }
                                else
                                {
                                    // If the grid to saves cost is lower then the current
                                    if (gridToSave.GetShipCost() < grid.GetShipCost())
                                    {
                                        // Disable the old grid
                                        foreach (ShipPart partToDissable in gridToSave.GetParts())
                                        {
                                            UpdateBlock(false, partToDissable, -partToDissable.block.CurrentHealth, true);
                                        }
                                        //  Set the current grid to be saved
                                        gridToSave = grid;
                                    }
                                    else
                                    {
                                        // Loop through and disable the current grid
                                        foreach (ShipPart partToDissable in grid.GetParts())
                                        {
                                            UpdateBlock(false, partToDissable, -partToDissable.block.CurrentHealth, true);
                                        }
                                    }

                                }
                            }
                        }
                    }
                    // Remove the stats of the part from the grid
                    RemoveStats(part, false);
                }
            }
            else
            {
                part.block.CurrentHealth += healthChange;
            }
        }
    }

    // Recalculate the thrust in the various directions and other data sets
    public void RecalculateValues()
    {
        // Reset the thruster speed array
        thrusterSpeed = new float[4] { 0.0f, 0.0f, 0.0f, 0.0f };
        currentHealth = 0;
        // Loop through all the ship parts
        foreach (ShipPart part in parts)
        {
            // If the part is broken, skip it
            if (!part.IsActive())
                continue;
            if(part.block.CurrentHealth< part.block.Health)
                currentHealth += part.block.CurrentHealth;
            else
                currentHealth += part.block.Health;
            // If the part is a engine
            if (part.block is EngineBlock)
            {
                // Cast a new instance of the block as a engine block
                EngineBlock engineBlock = (EngineBlock)part.block;
                // If the block is powered
                if (engineBlock.isPowered())
                {
                    // calculate the direction the thruster is pointing in
                    int direction = part.rotation < 5 ? part.rotation % 4 : (4 - (part.rotation % 4) % 4);
                    // Add to the thruster speed
                    thrusterSpeed[direction] += engineBlock.MaxThrust;
                }
            }
        }
    }
}
