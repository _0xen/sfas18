﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipPart
{
    // Define the position of the part
    public Vector3 position;
    // Define the current block that the part is based on
    public Block block;
    // Define the rotation of the block
    public int rotation = 0;
    // Define the movement object of the part
    public GameObject movmentObject;
    // Define the scale object of the part
    public GameObject scaleObject;
    // Define the actual block object of the part
    public GameObject blockObject;
    // Define the parent that the part will be attached too
    private GameObject ship_parent;
    // Used to add a small amount to the block scale to account for floating point errors causing size issues between blocks
    private Vector3 blockScaleOffset = new Vector3(0.001f, 0.001f, 0.0f);

    //  Define the current animation group
    private int animationGroup = 0;
    // Define the current index in the animation group
    private int textureIndex = 0;

    public ShipPart(Vector3 _position, Block _block,GameObject _ship_parent, int _rotation, bool snapToGrid = false)
    {
        block = _block.Clone(); // Get a new copy of the object as we need each one to be unique
        position = _position;
        ship_parent = _ship_parent;
        // Create the movement object instance. This will be the object that will be moved to manipulate all the other objects
        movmentObject = new GameObject(block.Name + " Position");
        movmentObject.transform.parent = ship_parent.transform;
        // This is the scaler object that will be moved to scale all the other objects
        scaleObject = new GameObject(block.Name + " Transformation");
        scaleObject.transform.parent = movmentObject.transform;
        // This is the model object that is the representation of the 1x1 square in out game, offset by 0.5,0.5 from its origin so it scales from
        // its bottom right corner
        blockObject = Utilities.CreateObject(
            DataStorage.BlockPrefab, // Block Prefab
            new Vector3(0.5f, 0.5f, 0.0f), // Set the position relative based on the position provided
            scaleObject, // Parent game object
            block.GetTexture(0,0)
            );
        // Once scaled we can move the object into its new position
        Move(position, snapToGrid);
        // Rotate the block
        SetRotate(_rotation);
        // If the block cant be rotated, update it manually
        if(!block.Rotatable) UpdateObject();
    }

    // Get the position of the part rounded down
    public Vector2Int GetPosition()
    {
        return new Vector2Int(Mathf.FloorToInt(position.x + 0.5f), Mathf.FloorToInt(position.y + 0.5f));
    }

    // Return if the block is active or not
    public bool IsActive()
    {
        return blockObject.activeInHierarchy;
    }

    // Set if the block is active or not
    public void SetActive(bool active)
    {
        // if the part dose not match the current active status then continue
        if (blockObject.activeInHierarchy != active)
        {
            // Set the blocks active status
            blockObject.SetActive(active);
            // If the block is active then set its health back to full or set its health to 0
            if (active)
                block.CurrentHealth = block.Health;
            else
                block.CurrentHealth = 0;
        }
    }

    // Move the part and lock it to the hanger grid system if needed
    public void Move(Vector3 _position, bool snapToGrid = false)
    {
        // If we require the moved part to be blocked to a grid
        if (snapToGrid)
            movmentObject.transform.position = new Vector3(Mathf.FloorToInt(_position.x), Mathf.FloorToInt(_position.y), _position.z);
        else
            movmentObject.transform.position = _position;
        position = movmentObject.transform.position;
    }

    // Set the shader color of the part
    public void SetShaderColor(Vector4 color)
    {
        Utilities.SetShaderVar(blockObject, "_ColorTint", color);
    }

    // Get the current animation group index
    public int GetAnimationGroupIndex()
    {
        return animationGroup;
    }

    // Set the current animation group
    public void SetAnimationGroup(int animationGroupIndex)
    {
        animationGroup = animationGroupIndex;
    }

    // Get the current texture that is applied to the block
    public Texture GetCurrentTexture()
    {
        return blockObject.GetComponent<Renderer>().material.mainTexture;
    }

    // Set the texture index from the group
    public void SetTextureFromGroup(int _textureIndex)
    {
        textureIndex = _textureIndex;
    }

    // Destroy the current objects that make up the ship part
    public void Destroy()
    {
        GameObject.Destroy(blockObject);
        GameObject.Destroy(scaleObject);
        GameObject.Destroy(movmentObject);
    }

    // Clone the current ship part
    public ShipPart Clone(bool resetToZeroDepth = false)
    {
        // Create a new instance of the ship part
        ShipPart newPart = new ShipPart(position, block, ship_parent, rotation, true);
        // If we need the parts position reset then set the z axis to 0
        if (resetToZeroDepth) newPart.Move(newPart.position + new Vector3(0.0f, 0.0f, -newPart.position.z));
        return newPart;
    }

    // Set the rotation of the part
    public void SetRotate(int _rotation)
    {
        // If the block is not rotatable, return
        if (!block.Rotatable)
        {
            rotation = 0;
            return;
        }
        // Set the rotation of the block
        rotation = _rotation;
        // If the block can be mirrored
        if (block.MirrorRotation)
            rotation = (_rotation + 8) % 8; // Make sure the rotation is in the range of 0-7
        else
            rotation = (_rotation + 4) % 4; // Make sure the rotation is in the range of 0-3
        UpdateObject();
    }

    // Update the current parts texture rotation, dimensions, etc
    public void UpdateObject()
    {
        blockObject.GetComponent<Renderer>().material.mainTexture = null;
        //GameObject.Destroy(blockObject.GetComponent<Renderer>().material.mainTexture);
        // If the block cant be rotated
        if (!block.Rotatable)
        {
            // Set the local scale of the block to the provided dimensions
            scaleObject.transform.localScale = new Vector3(block.PartDim.x, block.PartDim.y, 1.0f) + blockScaleOffset;
            // Set the texture to the current animation frame group at x index
            blockObject.GetComponent<Renderer>().material.mainTexture = block.GetTexture(block.AnimationFrames[animationGroup, textureIndex% block.AnimationFrames.Length], 0);
            return;
        }
        // Set the texture to the current animation frame group at x index, with rotation applied to it
        blockObject.GetComponent<Renderer>().material.mainTexture = block.GetTexture(block.AnimationFrames[animationGroup, textureIndex % block.AnimationFrames.Length], rotation);
        //blockObject.GetComponent<Renderer>().material.mainTexture = Utilities.RotateTexture(block.GetTexture(block.animationFrames[animationGroup, textureIndex % block.animationFrames.Length]), rotation);
        // Flip the block if it is being mirrored
        blockObject.GetComponent<Renderer>().material.SetTextureScale("_MainTex", new Vector2(rotation < 4 ? 1.0f : -1.0f, 1.0f));
        // Next we scale the object by the parts dimensions
        scaleObject.transform.localScale = new Vector3(rotation % 2 == 0 ? block.PartDim.x : block.PartDim.y, rotation % 2 == 0 ? block.PartDim.y : block.PartDim.x, 1.0f) + blockScaleOffset;
    }

    // Rotate the part 90 degrees clockwise for ever step
    public void Rotate(int rotate)
    {
        rotation += rotate;
        SetRotate(rotation);
    }
}
