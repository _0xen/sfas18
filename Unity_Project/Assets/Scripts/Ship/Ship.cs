﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : ShipGrid
{
    // Stores the game object that is used for the ships position
    private GameObject shipPosition;
    // Stores the object that is responsible for the ships rotation
    private GameObject shipRotation;
    // Stores the object that is used to offset the ship by half of its width and height
    private GameObject shipTranslation;

    /// Used to store the ships grid
    //private ShipGrid shipGrid;

    // Define a vector the ships momentum, thrust and drag
    public Vector3 momentum;
    public Vector3 thrust;
    public Vector3 solarBody;
    private Vector3 drag;

    public Vector3 rotationDirection;

    // Stores the ships current animation and firing states
    public DataStorage.ShipFlags flags = 0;

    private bool isAlive;
    private int playerID;
    private int kills;
    private int deaths;

    // Initialize the ship
    public Ship(string shipData, int id)
    {
        // Create a game object that is used for the ships position
        shipPosition = new GameObject("Ship Position");
        // Create a game object that is used for the ships rotation
        shipRotation = new GameObject("Ship Rotation");
        // Attach the rotation object to the position object
        shipRotation.transform.parent = shipPosition.transform;

        // Used to get the offset of the ship by half of its width and height
        shipTranslation = new GameObject("Ship Offset");
        // Attach the translation object to the rotation object
        shipTranslation.transform.parent = shipRotation.transform;

        // Initialize vectors that will store the ships momentum, thrust and drag
        momentum = new Vector3();
        thrust = new Vector3();
        solarBody = new Vector3();
        drag = new Vector3();
        // Initialize the vector that will store the current vector that we wish the ship to face
        rotationDirection = new Vector3();

        // Define if the current ship is alive
        isAlive = true;

        // Set the players id
        playerID = id;

        // Initialize the kills and deaths
        kills = 0;
        deaths = 0;

        // Initialize the ship grid
        //shipGrid = new ShipGrid();


        // Load the ships grid and store it in the shipGrid variable
        Utilities.LoadShipBattleGround(shipTranslation, this, shipData, id);
        // Set the grids color to white (Normal)
        Utilities.SetGridColor(this, Utilities.BlockColor);
        // Set the ship position to the origin
        shipRotation.transform.position = new Vector3(0.0f, 0.0f, 0.0f);
        // Calculate the center of the center of the grid
        UpdateShipCenter();
        // Recalculate the ships thrust across all 4 directions
        RecalculateValues();
    }

    // Get the ship position GameObject
    public GameObject GetShipObject()
    {
        return shipPosition;
    }

    // Create getter for the players kills
    public int GetKills()
    {
        return kills;
    }

    // Create increment for the players kills
    public void AddKill()
    {
        kills++;
    }

    // Create getter for the players deaths
    public int GetDeaths()
    {
        return deaths;
    }

    // Create increment for the players deaths
    public void AddDeath()
    {
        deaths++;
    }

    // Get the players id
    public int GetID()
    {
        return playerID;
    }

    // Get the ships rotational object
    public GameObject GetRotationObject()
    {
        return shipRotation;
    }

    // return if the current ship is alive
    public bool IsAlive()
    {
        return isAlive;
    }

    // Set if the current ship is alive or not
    public void SetAlive(bool alive)
    {
        // Set the isAlive flag to 'alive'
        isAlive = alive;
        // Loop through each part and set there active status
        foreach(ShipPart part in GetParts())
        {
            UpdateBlock(alive, part, part.block.Health, true);
        }
        // If the ship is alive, then set it back to full health, or set it to 0
        if (alive)
            SetCurrentHealth(GetHealth());
        else
            SetCurrentHealth(0);
    }

    // Set the flag to indicate the ship is shooting
    public void BeginShooting()
    {
        flags |= DataStorage.ShipFlags.Shooting;
        UpdateAnimation(flags, 0);
    }

    // Set the flag to indicate the ship is not shooting
    public void EndShooting()
    {
        flags &= ~DataStorage.ShipFlags.Shooting;
        UpdateAnimation(flags, 0);
    }

    // Set when the thrusters are on the ship are firing and in what direction
    public void BeingThrust(int index)
    {
        flags |= DataStorage.thrusterFlags[index];
        UpdateAnimation(flags, 0);
    }

    // Set when the thrusters are turned off and in what direction
    public void EndThrust(int index)
    {
        flags &= ~DataStorage.thrusterFlags[index];
        UpdateAnimation(flags, 0);
    }

    // Used to move the ship based on what thrusters are firing
    public void Move()
    {
        // Loop through all thruster directions
        for(int i = 0; i < 4; i++)
        {
            // If the ship is flagged to be thrusting in x direction continue
            if((flags & DataStorage.thrusterFlags[i]) == DataStorage.thrusterFlags[i])
            {
                // Get the thrust value in x direction
                float thrusterOutput = GetThrusterSpeed(i);
                // If we are thrusting down or left then reverse the thrust
                if (i >= 2) thrusterOutput = -thrusterOutput;
                // Apply the thrust to the thrust vector
                thrust += Utilities.Scalar(thrusterOutput, i % 2 == 0 ? shipRotation.transform.up : shipRotation.transform.right);
            }
        }
    }

    // Rotate towards the facing vector provided inside the ship
    public void RotateTowards()
    {
        // If the ship no longer produces power, it can not rotate so return
        if (!hasPowerSystem()) return;
        // Calculate the angle between the up vector of the ship and its rotation direction
        float angle = Vector3.Angle(shipRotation.transform.up, rotationDirection);
        // Get the rotation speed of the ship based on the base speed minus the speed reduction based on its weight
        float rotationSpeed = Mathf.Clamp(DataStorage.BaseRotationSpeed - (GetWeight() * DataStorage.RotationSpeedDescale), 1.5f, 10.0f);
        // Create a new roration Quaternion
        Quaternion newRotation;
        // If the angle to the left of the ship is less then the angle to the right of the ship, continue
        if (Vector3.Angle(shipRotation.transform.right, rotationDirection) < Vector3.Angle(-shipRotation.transform.right, rotationDirection))
            newRotation = Quaternion.Euler(0, 0, shipRotation.transform.rotation.eulerAngles.z - angle);
        else
            newRotation = Quaternion.Euler(0, 0, shipRotation.transform.rotation.eulerAngles.z + angle);
        // Apply the new angle to the object
        shipRotation.transform.rotation = Quaternion.RotateTowards(shipRotation.transform.rotation, newRotation, rotationSpeed);
    }

    // Updated the ships animations, power systems and thrust
    public void UpdateTick(int tick)
    {
        UpdatePowerSystems(false);
        RecalculateValues();
        UpdateAnimation(flags, tick);
    }

    // Update the ships data
    public void Update()
    {
        lock (DataStorage.Lock_)
        {
            // If the ship is not alive then dont move
            if (!IsAlive()) return;
            // Process the ships rotation
            RotateTowards();
            // Calculate the ships current thrust
            Move();
            // Create drag based on ships weight and the ships current momentum
            drag = Utilities.Scalar(-(GetWeight() * 0.000002f), momentum);

            // Calculate the new momentum for the ship
            momentum = solarBody + Utilities.Sum(momentum, thrust, drag);

            // Speed limits the ship by checking its momentum and if it is greater then the max speed, then reduce
            // If the momentum of the ship is larger then the max speed then limit it
            if (momentum.magnitude > DataStorage.MaxShipSpeed)
            {
                // normalize the momentum variable so its size will be 1.0f
                momentum.Normalize();
                // Scale the momentum by the speed
                momentum *= DataStorage.MaxShipSpeed;
            }
            // Move the ship based on the momentum vector
            shipPosition.transform.position += momentum;

            // Make sure the ship is in 0.0f of the x axis
            if (shipPosition.transform.position.z > 0.0f || shipPosition.transform.position.z < 0.0f)
                shipPosition.transform.position = new Vector3(shipPosition.transform.position.x, shipPosition.transform.position.y, 0.0f);

            // Make sure the position is inside the bounds
            CheckShipInBounds();

            // Reset the thrust to 0
            thrust = new Vector3(0.0f, 0.0f, 0.0f);
        }
    }

    public void CheckShipInBounds()
    {
        // If the position is outside the maps x upper bounds
        if (shipPosition.transform.position.x > DataStorage.MapBounderySize.x)
        {
            // Adjust the x pos to fit back inside the bounds
            shipPosition.transform.position = new Vector3(DataStorage.MapBounderySize.x, shipPosition.transform.position.y, shipPosition.transform.position.z);
            // Define we hit the bounds
            momentum = new Vector3(0.0f, momentum.y, 0.0f);
        }
        if (shipPosition.transform.position.x < -DataStorage.MapBounderySize.x) // If the position is outside the maps x lower bounds
        {
            // Adjust the x pos to fit back inside the bounds
            shipPosition.transform.position = new Vector3(-DataStorage.MapBounderySize.x, shipPosition.transform.position.y, shipPosition.transform.position.z);
            // Define we hit the bounds
            momentum = new Vector3(0.0f, momentum.y, 0.0f);
        }

        // If the position is outside the maps y upper bounds
        if (shipPosition.transform.position.y > DataStorage.MapBounderySize.y)
        {
            // Adjust the y pos to fit back inside the bounds
            shipPosition.transform.position = new Vector3(shipPosition.transform.position.x, DataStorage.MapBounderySize.y, shipPosition.transform.position.z);
            // Define we hit the bounds
            momentum = new Vector3(momentum.x, 0.0f, 0.0f);
        }
        if (shipPosition.transform.position.y < -DataStorage.MapBounderySize.y) // If the position is outside the maps y lower bounds
        {
            // Adjust the y pos to fit back inside the bounds
            shipPosition.transform.position = new Vector3(shipPosition.transform.position.x, -DataStorage.MapBounderySize.y, shipPosition.transform.position.z);
            // Define we hit the bounds
            momentum = new Vector3(momentum.x, 0.0f, 0.0f);
        }
    }

    // this will be called when a block changes to allow us to update the power systems and the thrust
    public void UpdateShipAfterChange()
    {
        // Recalculate the ship center as some blocks might have been removed
        UpdateShipCenter(true);
        // Recalculate the ships thrust as a engine could have been removed
        RecalculateValues();
    }

    // Update the ship translation
    private void UpdateShipCenter(bool updatePosition = false)
    {
        lock (DataStorage.Lock_)
        {
            // Find the ships offset
            Vector3 shipOffset = Utilities.GetGridCenter(this);
            // If the position should be updated, continue
            if (updatePosition)
            {
                // Update the ships position to include any ship
                shipPosition.transform.localPosition += (shipRotation.transform.rotation * (shipTranslation.transform.localPosition - shipOffset));
            }
            shipTranslation.transform.localPosition = shipOffset;
        }
    }

    // Get the speed of the ship
    public float GetSpeed()
    {
        return momentum.magnitude;
    }
}
