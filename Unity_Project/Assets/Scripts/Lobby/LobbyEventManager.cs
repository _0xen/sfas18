﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class LobbyEventManager : MonoBehaviour
{
    // Basic block object for all objects
    public GameObject blockObject;
    // Define the background music of the hanger
    public AudioClip backgroundMusic;

    // Define the various GameObjects required by the class
    private GameObject AddressPanel;
    private GameObject ScoreBoard;
    private GameObject ConnectedPanel;
    private GameObject ReadyButtonText;
    private GameObject TitleBarMessage;
    private GameObject AddressField;
    private GameObject ReadyButton;
    private GameObject ConnectButton;
    private GameObject BackToLobby;
    // Used to store the main music object instance
    private GameObject MainMusic;


    // Use this for initialization
    public void Start ()
    {
        // Get the music game object instance
        MainMusic = GameObject.Find("MainMusic");
        // Switch the audio source
        MainMusic.GetComponent<AudioHandler>().SwitchClip(backgroundMusic);
        // Initialize the various game objects with the instances within the scene
        AddressPanel = GameObject.Find("AddressPanel");
        ScoreBoard = GameObject.Find("ScoreBoard");
        ConnectedPanel = GameObject.Find("ConnectedPanel");
        ReadyButtonText = GameObject.Find("ReadyButtonText");
        TitleBarMessage = GameObject.Find("TitleBarMessage");
        AddressField = GameObject.Find("AddressField");
        ReadyButton = GameObject.Find("ReadyButton");
        ConnectButton = GameObject.Find("ConnectButton");
        BackToLobby = GameObject.Find("BackToLobby");

        // Setup class instances of objects to various static functions
        DataStorage.BlockPrefab = blockObject;
        // Make sure that the game dose not pause when the user is not on it (Debugging)
        Application.runInBackground = true;
        // Setup the config for the connection
        SetupConnectionConfig();
        DefineClientHandles();

        // Define the click lister for the ready button
        ReadyButton.GetComponent<Button>().onClick.AddListener(delegate { ReadyButtonClick(); });
        ConnectButton.GetComponent<Button>().onClick.AddListener(delegate { ConnectToServer(); });

        // Load the last connected ip onto the address field
        AddressField.GetComponent<InputField>().text = PlayerPrefs.GetString("LastIP","");

        // If the client is not null, then it is already connected and we should continue the connection
        if (DataStorage.Client != null)
        {
            AfterBattle();
            return;
        }
        
        // Depending on the type of connection
        switch (DataStorage.Connection)
        {
            // If we are the server and client
            case DataStorage.ConnectionType.ServerClient:
                StartServer();
                break;
            // If we are the client
            case DataStorage.ConnectionType.Client:
                Utilities.ShowUI(AddressPanel.GetComponent<CanvasGroup>());
                Utilities.HideUI(ScoreBoard.GetComponent<CanvasGroup>());
                Utilities.HideUI(ConnectedPanel.GetComponent<CanvasGroup>());
                break;
            case DataStorage.ConnectionType.None:
            default:
                UpdateTitleBarMessage("Invalid, returning to hangar");
                ReturnToHanger();
                return;
        }
    }

    // Setup the handles for the client packet callbacks
    void DefineClientHandles()
    {
        DataStorage.LobbyZoneHandles.OnConnect = OnClientConnect;
        DataStorage.LobbyZoneHandles.OnClientDisconnected = OnClientDisconnected;
        DataStorage.LobbyZoneHandles.OnGameStart = OnGameStart;
        DataStorage.LobbyZoneHandles.OnClientShipUpdate = OnClientShipUpdate;
    }

    // Called at startup if we are already connected to the server
    // This is used to Re setup the UI with player information, etc
    void AfterBattle()
    {
        // Re setup the title bar title
        TitleBarMessage.GetComponent<Text>().text = "Lobby";
        // Hide the client connection panel and show the connected panel
        Utilities.ShowUI(ScoreBoard.GetComponent<CanvasGroup>());
        Utilities.HideUI(ConnectedPanel.GetComponent<CanvasGroup>());
        Utilities.HideUI(AddressPanel.GetComponent<CanvasGroup>());
        // Define the click listener on the back to lobby button
        // When clicked we hide the score board and show the lobby
        BackToLobby.GetComponent<Button>().onClick.AddListener(delegate {
            Utilities.HideUI(ScoreBoard.GetComponent<CanvasGroup>());
            Utilities.ShowUI(ConnectedPanel.GetComponent<CanvasGroup>());
            Utilities.HideUI(AddressPanel.GetComponent<CanvasGroup>());
        });

        // Set some data storage vars back to there defaults
        DataStorage.ClientData.ready = false;
        // If we are the server, reset the game started field
        if(DataStorage.ConnectionType.ServerClient == DataStorage.Connection)
            DataStorage.Server.GameStarted = false;
        // Loop through the ships data
        foreach (DataStorage.ShipLobbyData shipData in DataStorage.ClientPlayersInstance.Values)
        {
            Utilities.ShowUI(GameObject.Find("Player" + shipData.id + "ScoreBoardDiv").GetComponent<CanvasGroup>());
            GameObject.Find("Player" + shipData.id + "ScoreName").GetComponent<Text>().text = shipData.name;
            GameObject.Find("Player" + shipData.id + "Score").GetComponent<Text>().text = "K:" + shipData.kills + " D:" + shipData.deaths;
            // Set the player to being unready
            shipData.ready = false;
            // Update the lobby ui
            UpdateLobyUI(shipData);
        }
    }

    // When the server sends a update about one of the players
    private void OnClientShipUpdate(NetworkMessage netMsg)
    {
        // Read the ship update packet
        DataStorage.ShipLobbyData ship = netMsg.ReadMessage<DataStorage.ShipLobbyData>();
        // If the ship is not connected anymore
        if (!ship.connected)
        {
            // Remove the client instance of the ship
            DataStorage.ClientPlayersInstance.Remove(ship.id);
        }
        else
        {
            // If the clients local instance dose not exist, create one
            if (!DataStorage.ClientPlayersInstance.ContainsKey(ship.id))
                DataStorage.ClientPlayersInstance.Add(ship.id, ship);
            else // Else set the instance that exists to the new one
                DataStorage.ClientPlayersInstance[ship.id] = ship;
        }
        UpdateLobyUI(ship);
    }

    // Called to update the lobby data with the newest data
    private void UpdateLobyUI(DataStorage.ShipLobbyData ship)
    {
        if (!ship.connected)
        {
            // Hide the UI element
            Utilities.HideUI(GameObject.Find("Player" + ship.id).GetComponent<CanvasGroup>());
        }
        else
        {
            // Show the player info UI
            Utilities.ShowUI(GameObject.Find("Player" + ship.id).GetComponent<CanvasGroup>());
            // Set the players status
            GameObject.Find("Player" + ship.id + "Status").GetComponent<Text>().text = ship.ready ? "Ready" : "Waiting";
        }
    }

    // When the client connects
    void OnClientConnect(NetworkMessage netMsg)
    {
        // Update the title bar
        TitleBarMessage.GetComponent<Text>().text = "Lobby";
    }

    // When the server sends the start message, load the battle zone
    private void OnGameStart(NetworkMessage netMsg)
    {
        SceneManager.LoadScene("BattleZone");
    }

    // When the client disconnects from the server, return to the hanger
    private void OnClientDisconnected(NetworkMessage netMsg)
    {
        Utilities.ReturnToHangerFromMultiplayer();
    }

    // When the connect button is pressed, call this function
    void ConnectToServer()
    {
        StartClient(false);
    }

    // Called when the server needs to start
    void StartServer()
    {
        if(DataStorage.Server == null)
        {
            UpdateTitleBarMessage("Setting up server");
            // Initialize the server and client
            DataStorage.Server = new Server();
            // Set the client to connect locally
            StartClient(true);
        }
        else
        {
            Utilities.ShowUI(ConnectedPanel.GetComponent<CanvasGroup>());
            Utilities.HideUI(AddressPanel.GetComponent<CanvasGroup>());
        }
    }

    // Called when we need to connect to the server
    // pass a bool to define if we need to connect to a local server
    void StartClient(bool local)
    {
        // Hide the address panel and show the connection panel
        Utilities.ShowUI(ConnectedPanel.GetComponent<CanvasGroup>());
        Utilities.HideUI(AddressPanel.GetComponent<CanvasGroup>());
        UpdateTitleBarMessage("Searching");
        // Start the client
        DataStorage.Client = new Client(local, AddressField.GetComponent<InputField>().text);
        // Set the value of the ip to the ip stored in the address field
        PlayerPrefs.SetString("LastIP", AddressField.GetComponent<InputField>().text);
    }

    // When the ready button is pressed
    void ReadyButtonClick()
    {
        // If the client is not ready
        if (!DataStorage.ClientData.ready)
        {
            // Set that the client is ready and send the update to the server
            ReadyButtonText.GetComponent<Text>().text = "Waiting";
            DataStorage.ClientData.ready = true;
            DataStorage.Client.SetReady();
        }
        // If we are the server and everyone is ready then start the game
        else if(DataStorage.Connection == DataStorage.ConnectionType.ServerClient && DataStorage.Server.allClientsReady)
        {
            DataStorage.Server.StartGame();
        }
        // Otherwise set the client back to not being ready
        else
        {
            // Set the client back to not being ready
            ReadyButtonText.GetComponent<Text>().text = "Ready";
            DataStorage.ClientData.ready = false;
            DataStorage.Client.SetReady();
        }
    }

    // Setup the connection config
    void SetupConnectionConfig()
    {
        DataStorage.ConnectionConfig.ConnectTimeout = 500;
        DataStorage.ConnectionConfig.MaxConnectionAttempt = 5;
        DataStorage.ConnectionConfig.PacketSize = 1440;
        DataStorage.ConnectionConfig.FragmentSize = 900;
        DataStorage.ConnectionConfig.ResendTimeout = 500;
        DataStorage.ConnectionConfig.DisconnectTimeout = 1500;
    }

    // Update the title bar text
    private void UpdateTitleBarMessage(string text)
    {
        TitleBarMessage.GetComponent<Text>().text = text;
    }

    // Return to the hanger
    void ReturnToHanger()
    {
        Utilities.ReturnToHangerFromMultiplayer();
    }
}
