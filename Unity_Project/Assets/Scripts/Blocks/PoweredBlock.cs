﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoweredBlock : Block
{
    // Flags to define what type of powered block we are creating
    public enum PoweredBlockType
    {
        Output = 0x01,
        Input = 0x02,
        Storage = 0x04
    }
    // Set what storage type the block is
    private PoweredBlockType storageType;
    public PoweredBlockType StorageType
    {
        get { return storageType; }
        private set { storageType = value; }
    }

    // Set the max input, Stored and output power of the block
    private float maxInputPower;
    public float MaxInputPower
    {
        get { return maxInputPower; }
        private set { maxInputPower = value; }
    }
    private float maxStoredPower;
    public float MaxStoredPower
    {
        get { return maxStoredPower; }
        private set { maxStoredPower = value; }
    }
    private float maxOutputPower;
    public float MaxOutputPower
    {
        get { return maxOutputPower; }
        private set { maxOutputPower = value; }
    }
    // Set the current input, stored and output power for the block
    private float currentInputPower;
    public float CurrentInputPower
    {
        get { return currentInputPower; }
        set { currentInputPower = value; }
    }
    private float currentStoredPower;
    public float CurrentStoredPower
    {
        get { return currentStoredPower; }
        set { currentStoredPower = value; }
    }
    private float currentOutputPower;
    public float CurrentOutputPower
    {
        get { return currentOutputPower; }
        set { currentOutputPower = value; }
    }
    // Define if the block is powered this tick
    private bool powered;
    public bool Powered
    {
        get { return powered; }
        set { powered = value; }
    }

    // Base constructor that will initialize no data
    protected PoweredBlock() :base() { }


    // Construct a instance of the object and pass the data for the 'Block' and for the 'PoweredBlock' class
    protected PoweredBlock(int id, string name, int health, int weight, int cost, Vector2Int partDim, Vector2Int textureCoord, int animationFrameCount, int[,] animationFrames, bool[,] colisionSides, bool rotatable, bool mirrorRotation,
        PoweredBlockType storageType, float maxInputPower, float maxStoredPower, float maxOutputPower) :
        base(id, name, health, weight, cost, partDim, textureCoord, animationFrameCount, animationFrames, colisionSides, rotatable, mirrorRotation)
    {
        // Initialize class member variables
        StorageType = storageType;
        MaxInputPower = maxInputPower;
        MaxOutputPower = maxOutputPower;
        MaxStoredPower = maxStoredPower;
        CurrentStoredPower = 0.0f;
        Powered = false;
    }

    // Get the blocks powered state
    public bool isPowered()
    {
        return powered;
    }

    // Reset the blocks power currents
    public void ResetPower()
    {
        currentInputPower = 0.0f;
    }

    // Set the blocks input power
    public void InputPower(float power)
    {
        currentInputPower += power;
    }

    // Add to the blocks stored power
    public void StorePower(float power)
    {
        currentStoredPower += power;
    }

    // Remove from the blocks output power
    public void OutputPower(float power)
    {
        currentOutputPower -= power;
    }

    // Get the blocks generated power, if it is not set to generate power, return 0.0f
    public float GetGeneratedPower()
    {
        if(!IsType(PoweredBlockType.Input) && IsType(PoweredBlockType.Output))
        {
            return maxOutputPower;
        }
        return 0.0f;
    }

    // Get the amount of power drawn by the block. If it dose not draw power then return 0.0f
    public float GetDrawnPower()
    {
        if (IsType(PoweredBlockType.Input))
        {
            return maxInputPower;
        }
        return 0.0f;
    }
    
    // Check to see if the block has a type flag
    public bool IsType(PoweredBlockType type)
    {
        return (storageType & type) == type;
    }
    
    // Create a blank instance of the object
    public static readonly PoweredBlock POWERED_BLOCK_INSTANCE = new PoweredBlock();

    // Define powered blocks and there data
    public static readonly PoweredBlock REACTOR = new PoweredBlock(2, "Reactor", 40, 50, 25, new Vector2Int(1, 1), new Vector2Int(3, 0), 3, new int[,] { { 0, 0 }, { 0, 0 }, { 1, 2 } }, new bool[0, 0], false, false,
        PoweredBlockType.Output, 0.0f, 0.0f, 2.0f);
    public static readonly PoweredBlock REACTOR_LARGE = new PoweredBlock(7, "Reactor Large", 150, 300, 100, new Vector2Int(3, 2), new Vector2Int(0, 2), 3, new int[,] { { 0, 0 }, { 0, 0 }, { 1, 2 } }, new bool[0, 0], true, true,
        PoweredBlockType.Output, 0.0f, 0.0f, 20.0f);
    
    // This is used as just a random variable that we need to access on project start-up to make sure the class is in memory
    public static bool powered_block_object_compiled = true;
}