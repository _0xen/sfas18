﻿using System.Collections;
using System.Linq;
using System;
using System.Collections.Generic;
using UnityEngine;

public class BlockHandler : MonoBehaviour
{
    void Start()
    {

    }
    // Called when a collision detection event is triggered in the object
    void OnCollisionEnter2D(Collision2D coll)
    {
        // If either of the two objects are not active, return
        if (!gameObject.activeInHierarchy) return;
        if (!coll.collider.gameObject.activeInHierarchy) return;
        //If both game objects contain "Ship" in there tags and there tags don't match, continue
        if (gameObject.tag.Contains("Ship"))
        {
            if(coll.collider.gameObject.tag.Contains("Ship") && gameObject.tag != coll.collider.gameObject.tag)
            {
                // Get the ship ids from both the objects tags
                int ship1 = Int32.Parse(gameObject.tag.Replace("Ship", ""));
                int ship2 = Int32.Parse(coll.collider.gameObject.tag.Replace("Ship", ""));
                // Here we check to see is ship1's id is smaller then ship2's
                if (ship1 < ship2)
                {
                    // Tell the battle zone that the two ships have collided, passing the smaller ship id first
                    BattleZoneEvents.instance.ShipPartCollided(ship1, ship2,
                    gameObject,
                    coll.collider.gameObject);
                }
                else
                {
                    // Tell the battle zone that the two ships have collided, passing the smaller ship id first
                    BattleZoneEvents.instance.ShipPartCollided(ship2, ship1,
                    coll.collider.gameObject,
                    gameObject);
                }
            }
            // If the object we collided with is a sun, continue
            else if (coll.collider.gameObject.tag == "Sun")
            {
                int ship = Int32.Parse(gameObject.tag.Replace("Ship", ""));
                BattleZoneEvents.instance.ShipPartCollidedWithSolarObeject(ship, gameObject);
            }
        }
    }
}
