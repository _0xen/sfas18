﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBlock : PoweredBlock
{
    // Store the shots power
    private float shotPower;
    public float ShotPower
    {
        get { return shotPower; }
        private set { shotPower = value; }
    }
    // Store the base damage of the shot
    private int baseDamage;
    public int BaseDamage
    {
        get { return baseDamage; }
        private set { baseDamage = value; }
    }
    // Projectile lifetime
    private float projectileLifeTime;
    public float ProjectileLifeTime
    {
        get { return projectileLifeTime; }
        private set { projectileLifeTime = value; }
    }
    // Speed the projectile should travel
    private float projectileSpeed;
    public float ProjectileSpeed
    {
        get { return projectileSpeed; }
        private set { projectileSpeed = value; }
    }
    // Projectile size
    private Vector3 projectileSize;
    public Vector3 ProjectileSize
    {
        get { return projectileSize; }
        private set { projectileSize = value; }
    }
    // Projectile color
    private Color projectileColor;
    public Color ProjectileColor
    {
        get { return projectileColor; }
        private set { projectileColor = value; }
    }
    // Is the projectile a seeker 
    private bool seeker;
    public bool Seeker
    {
        get { return seeker; }
        private set { seeker = value; }
    }


    // Base constructor that will initialize no data
    private WeaponBlock() : base() { }

    // Construct a instance of the object and pass the data for the 'Block' ,'PoweredBlock' and for the 'WeponBlock' class
    private WeaponBlock(int id, string name, int health, int weight, int cost, Vector2Int partDim, Vector2Int textureCoord, int animationFrameCount, int[,] animationFrames, bool[,] colisionSides, bool rotatable, bool mirrorRotation, // Base Variables
        PoweredBlockType storageType, float maxInputPower, float maxStoredPower, float maxOutputPower,
        float shotPower, int baseDamage,float projectileLifeTime,float projectileSpeed, Vector3 projectileSize, Color projectileColor, bool seeker) :                                        
        base(id, name, health, weight, cost, partDim, textureCoord, animationFrameCount, animationFrames, colisionSides, rotatable, mirrorRotation, 
            storageType, maxInputPower, maxStoredPower, maxOutputPower)
    {
        // Initialize class member variables
        ShotPower = shotPower;
        BaseDamage = baseDamage;
        ProjectileLifeTime = projectileLifeTime;
        ProjectileSpeed = projectileSpeed;
        ProjectileSize = projectileSize;
        ProjectileColor = projectileColor;
        Seeker = seeker;
    }

    // Remove x power from the stored power after a shot
    public void DischargeAfterShot()
    {
        if (IsType(PoweredBlockType.Storage))
        {
            // If there is not enough power to shoot, then set the stored power to 0.0f
            if (CurrentStoredPower - shotPower < 0.0f)
            {
                CurrentStoredPower = 0.0f;
            }
            else
            {
                StorePower(-shotPower);
            }
        }
        else
        {
            // If there is not enough power to shoot, then set the stored power to 0.0f
            if (CurrentInputPower - shotPower < 0.0f)
            {
                CurrentInputPower = 0.0f;
            }
            else
            {
                InputPower(-shotPower);
            }
        }
        
    }

    // Check to see if there is enough stored power to shoot
    public bool CanShoot()
    {
        return (!IsType(PoweredBlockType.Storage) && CurrentInputPower >= shotPower) || // If the Weapon dose not store power, but has available power then shoot
            (IsType(PoweredBlockType.Storage) && CurrentStoredPower >= shotPower); // If the weapon dose store power and has available power
    }

    // Create a blank instance of the object
    public static readonly WeaponBlock WEPON_BLOCK_INSTANCE = new WeaponBlock();

    // Define weapon blocks and there data
    public static readonly WeaponBlock CHAIN_GUN = new WeaponBlock(4, "Chain Gun", 100, 30, 80, new Vector2Int(1, 2), new Vector2Int(0, 12), 3, new int[,] { { 0, 0 }, { 1, 1 }, { 1, 2 } }, new bool[4, 2] { { false, false }, { false, false }, { true, false }, { false, false } }, false, false,
        PoweredBlockType.Input, 3.0f, 0.0f, 0.0f,
        3.0f, 10, 4.0f, 2.4f, new Vector3(0.2f, 1.0f, 1.0f), new Color(0.3f, 0.3f, 0.3f),false);

    public static readonly WeaponBlock RAIL_GUN = new WeaponBlock(12, "Rail Gun", 100, 30, 100, new Vector2Int(1, 2), new Vector2Int(3, 12), 6, new int[,] { { 0, 0, 0, 0, 0 }, { 1, 1, 1, 1, 1 }, { 1, 2, 3, 4, 5 } }, new bool[4, 2] { { false, false }, { false, false }, { true, false }, { false, false } }, false, false,
        PoweredBlockType.Input | PoweredBlockType.Storage, 1.0f, 16.0f, 0.0f,
        16.0f, 40, 3.0f, 3.4f, new Vector3(0.2f, 2.0f, 1.4f), new Color(1.0f, 0.2f, 0.2f),false);

    public static readonly WeaponBlock PLASMA_CANNON = new WeaponBlock(13, "Plasma Cannon", 100, 30, 125, new Vector2Int(1, 2), new Vector2Int(9, 12), 5, new int[,] { { 0, 0, 0, 0 }, { 1, 2, 3, 4 }, { 1, 2, 3, 4 } }, new bool[4, 2] { { false, false }, { false, false }, { true, false }, { false, false } }, false, false,
        PoweredBlockType.Input | PoweredBlockType.Storage, 1.0f, 24.0f, 0.0f,
        24.0f, 120, 10.0f, 1.4f, new Vector3(1.4f, 1.4f, 1.4f), new Color(0.5f, 0.0f, 1.0f),false);

    public static readonly WeaponBlock ROCKET_LAUNCHER = new WeaponBlock(21, "Rocket Launcher", 400, 60, 250, new Vector2Int(2, 2), new Vector2Int(0, 10), 7, new int[,] { { 0, 0, 0, 0, 0, 0 }, { 1, 2, 3, 4, 5, 6 }, { 1, 2, 3, 4, 5, 6 } }, new bool[4, 2] { { false, false }, { true, false }, { true, true }, { true, false } }, false, false,
        PoweredBlockType.Input | PoweredBlockType.Storage, 4.0f, 400.0f, 0.0f,
        40.0f, 50, 10.0f, 1.0f, new Vector3(0.6f, 1.4f, 1.4f), new Color(0.5f, 0.0f, 1.0f),true);

    // This is used as just a random variable that we need to access on project start-up to make sure the class is in memory
    public static bool wepon_block_object_compiled = true;
}