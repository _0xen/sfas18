﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngineBlock : PoweredBlock
{
    // The max thrust of the engine
    private float maxThrust = 0.0f;
    public float MaxThrust
    {
        get { return maxThrust; }
        private set { maxThrust = value; }
    }

    // Base constructor that will initialize no data
    private EngineBlock() : base() { }

    // Construct a instance of the object and pass the data for the 'Block' ,'PoweredBlock' and for the 'EngineBlock' class
    private EngineBlock(int id, string name, int health, int weight, int cost, Vector2Int partDim, Vector2Int textureCoord, int animationFrameCount, int[,] animationFrames, bool[,] colisionSides, bool rotatable, bool mirrorRotation, // Base Variables
        PoweredBlockType storageType, float maxInputPower, float maxStoredPower, float maxOutputPower,                                                                           // Powered block variables
        float maxThrust) :                                                                                                                                       // Engine block Variables
        base(id, name, health, weight, cost, partDim, textureCoord, animationFrameCount, animationFrames,colisionSides, rotatable, mirrorRotation, storageType, maxInputPower, maxStoredPower, maxOutputPower)
    {
        // Initialize class member variables
        MaxThrust = maxThrust;
    }

    // Create a blank instance of the object
    public static readonly EngineBlock ENGINE_BLOCK_INSTANCE = new EngineBlock();

    // Define engine blocks and there data
    public static readonly EngineBlock CHEMICAL_THRUSTER_LARGE = new EngineBlock(1, "Chemical Engine L", 100, 40, 80, new Vector2Int(2, 2), new Vector2Int(0, 14), 4, new int[,] { { 0, 0 }, { 1, 1 }, { 2, 3 } }, new bool[4, 2] { { true, true }, { false, true }, { false, false }, { false, true } }, true, true, // Base Variables
        PoweredBlockType.Input, 1.0f, 0.0f, 0.0f,                                                                                                                                                  // Powered block variables
        0.01f);
    public static readonly EngineBlock CHEMICAL_THRUSTER_SMALL = new EngineBlock(14, "Chemical Engine S", 50, 20, 50, new Vector2Int(1, 2), new Vector2Int(8, 14), 4, new int[,] { { 0, 0 }, { 3, 3 }, { 1, 2 } }, new bool[4, 2] { { true, true }, { false, true }, { false, false }, { false, true } }, true, true, // Base Variables
        PoweredBlockType.Input, 0.5f, 0.0f, 0.0f,                                                                                                                                                  // Powered block variables
        0.005f);                                                                                                                                                                      // Engine block Variables

    // This is used as just a random variable that we need to access on project start-up to make sure the class is in memory
    public static bool engine_block_object_compiled = true;
}
