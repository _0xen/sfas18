﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block
{
    // A static count of the total amount of blocks inside the game
    private static int count = 100;

    // Used to store all block instances
    public static Block[] Blocks;

    // Create a dictionary that stores the blocks in relation to there type (Thruster => Small Engine)
    private static Dictionary<System.Type,List<Block>> typeBlocks = new Dictionary<System.Type, List<Block>>();
    public static Dictionary<System.Type, List<Block>> TypeBlocks
    {
        get { return typeBlocks; }
        set { typeBlocks = value; }
    }

    // The blocks unique id
    private int id;
    public int ID
    {
        get { return id; }
        private set { id = value; }
    }
    // The blocks name for UI interfaces
    private string name;
    public string Name
    {
        get { return name; }
        private set { name = value; }
    }
    // The blocks texture coord on the sprite sheet
    private Vector2Int textureCoord;
    public Vector2Int TextureCoord
    {
        get { return textureCoord; }
        private set { textureCoord = value; }
    }
    // The dimensions of the block
    private Vector2Int partDim;
    public Vector2Int PartDim
    {
        get { return partDim; }
        private set { partDim = value; }
    }
    // Defines the max health of the block
    private int health;
    public int Health
    {
        get { return health; }
        private set { health = value; }
    }
    // Set to the current health
    private int currentHealth;
    public int CurrentHealth
    {
        get { return currentHealth; }
        set { currentHealth = value; }
    }
    // The Cost the place the block
    private int cost;
    public int Cost
    {
        get { return cost; }
        private set { cost = value; }
    }
    // The blocks weight, this will affect thrust and drag
    private int weight;
    public int Weight
    {
        get { return weight; }
        private set { weight = value; }
    }
    // The total amount of animation frames for this block
    private int animationFrameCount;
    public int AnimationFrameCount
    {
        get { return animationFrameCount; }
        private set { animationFrameCount = value; }
    }
    // Multidimensional array to store the animation frames of the block
    private int[,] animationFrames;
    public int[,] AnimationFrames
    {
        get { return animationFrames; }
        private set { animationFrames = value; }
    }
    // Multidimensional array to store what sides can be touching other sides 
    private bool[,] colisionSides;
    public bool[,] ColisionSides
    {
        get { return colisionSides; }
        private set { colisionSides = value; }
    }
    // Can the block be rotated
    private bool rotatable;
    public bool Rotatable
    {
        get { return rotatable; }
        private set { rotatable = value; }
    }
    // Can the block be mirror rotated
    private bool mirrorRotation;
    public bool MirrorRotation
    {
        get { return mirrorRotation; }
        private set { mirrorRotation = value; }
    }

    // Define a blank instance of the block
    protected Block(){ }

    //Construct a instance of the block object
    protected Block(int id, string name, int health, int weight, int cost, Vector2Int partDim, Vector2Int textureCoord, int animationFrameCount, int[,] animationFrames, bool[,] colisionSides, bool rotatable, bool mirrorRotation)
    {
        // Store all the pass initializer variables to the members
        ID = id;
        Name = name;
        Cost = cost;
        PartDim = partDim;
        TextureCoord = textureCoord;
        AnimationFrameCount = animationFrameCount;
        Health = health;
        CurrentHealth = health;
        Weight = weight;
        AnimationFrames = animationFrames;
        ColisionSides = colisionSides;
        Rotatable = rotatable;
        MirrorRotation = mirrorRotation;
        if (Blocks == null) Blocks = new Block[count];
        if (Blocks[id] != null)
            throw new System.Exception("Block with ID " + id + " already exists");
        Blocks[id] = this;
        // If no type under x type is found
        if(!typeBlocks.ContainsKey(this.GetType()))
        {
            // Initialize a new list to store blocks of x type into
            typeBlocks.Add(this.GetType(), new List<Block>());
        }
        // Store the current block
        typeBlocks[this.GetType()].Add(this);
    }

    // Get the texture at x animation frame
    public Texture2D GetTexture(int animationFrame,int rotation)
    {
        InitTextures();
        // If the frame is out of range, return null
        if (animationFrame >= animationFrameCount)
        {
            Debug.Log("Texture Frame Out Of Range");
            return null;
        }
        // return the generated texture
        return DataStorage.BlockTextures[id][animationFrame][rotation % 4];
    }

    // Clone a block into a new instance
    public Block Clone()
    {
        return (Block)this.MemberwiseClone();
    }

    // Used to initialize textures for the block
    private void InitTextures()
    {
        // Check to see if the blocks texture already exists
        if (!DataStorage.BlockTextures.ContainsKey(id))
        {
            // Add a new list to store the block texture frames
            DataStorage.BlockTextures.Add(id, new List<Texture2D[]>());
            // Loop through for the block texture animation frames
            for (int i = 0; i < animationFrameCount; i++)
            {
                // Add a new texture array and put it into the block textures array
                DataStorage.BlockTextures[id].Add(new Texture2D[4]);
                // Add the non rotated texture to the texture arrays
                DataStorage.BlockTextures[id][DataStorage.BlockTextures[id].Count - 1][0] = Utilities.GetSpriteSheetTexture(textureCoord + new Vector2Int(i * partDim.x, 0), partDim);
                // Get the non rotated texture as a base for the rotated ones
                Texture2D baseForRotation = DataStorage.BlockTextures[id][DataStorage.BlockTextures[id].Count - 1][0];
                // Loop through for the last three rotation frames
                for (int j = 1; j < 4; j++)
                {
                    // Rotate the textures and add them to the rotation frames
                    DataStorage.BlockTextures[id][DataStorage.BlockTextures[id].Count - 1][j] = Utilities.RotateTexture(baseForRotation, j);
                }
            }
        }
    }

    // A instance of the Block object but without any of the data 
    public static readonly Block BLOCK_INSTANCE = new Block();

    // Define ship blocks and there data
    public static readonly Block LIGHT_ARMOUR_BLOCK = new Block(0, "Light Armour", 100, 10, 4, new Vector2Int(1, 1), new Vector2Int(2, 0), 1, new int[,] { { 0 } }, new bool[0, 0], false, false);
    public static readonly Block LIGHT_ARMOUR_SLOPE = new Block(5, "Armour Slope", 50, 5, 2, new Vector2Int(1, 1), new Vector2Int(4, 0), 1, new int[,] { { 0 } }, new bool[4, 1] { { false }, { false }, { true }, { true } }, true, true);
    public static readonly Block LIGHT_ARMOUR_SLOPE_1_2 = new Block(6, "Armour Slope 1x2", 100, 10, 8, new Vector2Int(1, 2), new Vector2Int(5, 0), 1, new int[,] { { 0 } }, new bool[4, 2] { { false, false }, { false, false }, { true, true }, { true, true } }, true, true);
    public static readonly Block LIGHT_ARMOUR_POINT = new Block(16, "Armour Point", 25, 3, 1, new Vector2Int(1, 1), new Vector2Int(4, 1), 1, new int[,] { { 0 } }, new bool[4, 1] { { false }, { false }, { true }, { false } }, true, true);


    public static readonly Block HEAVY_ARMOUR_BLOCK = new Block(17, "Heavy Armour", 300, 50, 8, new Vector2Int(1, 1), new Vector2Int(0, 1), 1, new int[,] { { 0 } }, new bool[0, 0], false, false);
    public static readonly Block HEAVY_ARMOUR_SLOPE = new Block(18, "Heavy Armour Slope", 150, 25, 4, new Vector2Int(1, 1), new Vector2Int(1, 1), 1, new int[,] { { 0 } }, new bool[4, 1] { { false }, { false }, { true }, { true } }, true, true);
    //public static readonly Block HEAVY_ARMOUR_SLOPE_1_2 = new Block(19, "Heavy Armour Slope 1x2", 300, 50, 8, new Vector2Int(1, 2), new Vector2Int(5, 0), 1, new int[,] { { 0 } }, new bool[4, 2] { { false, false }, { false, false }, { true, true }, { true, true } }, true, true);
    public static readonly Block HEAVY_ARMOUR_POINT = new Block(20, "Heavy Armour Point", 75, 12, 2, new Vector2Int(1, 1), new Vector2Int(2, 1), 1, new int[,] { { 0 } }, new bool[4, 1] { { false }, { false }, { true }, { false } }, true, true);




    public static readonly Block STRAIGHT_STRUT = new Block(15, "Straight Strut", 120, 4, 10, new Vector2Int(1, 2), new Vector2Int(15, 0), 1, new int[,] { { 0 } }, new bool[4, 2] { { true, true }, { false, false }, { true, true }, { false, false } }, true, true);

    // This is used as just a random variable that we need to access on project start-up to make sure the class is in memory
    public static bool block_object_compiled = true;
}

