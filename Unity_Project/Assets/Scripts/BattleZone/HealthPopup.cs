﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPopup : MonoBehaviour
{

    private Vector3 direction;
	// Use this for initialization
	void Start ()
    {
        // Define a random direction for the number to fly to
        direction = Random.insideUnitSphere.normalized * 0.05f;
    }

    // Called to set the health text pop-up
    public void SetHealthChange(int healthChange)
    {
        // Set the value of the text to the value of the object to the health change value. If it
        // Contains a minus sign for negative numbers, remove it
        gameObject.GetComponent<TextMesh>().text = healthChange.ToString().Replace("-","");
        // After x seconds, call the function to destroy the object
        InvokeRepeating("DesconstructionTimeout", 1.0f, 1.0f);
        // If the health is less then 0, set the color to red, or set it to green
        if (healthChange < 0)
            gameObject.GetComponent<TextMesh>().color = Color.red;
        else
            gameObject.GetComponent<TextMesh>().color = Color.green;
    }

    // Called when the object is to be destroyed
    void DesconstructionTimeout()
    {
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update ()
    {
        // Move the object x distance in y direction (to add a slow drifting effect for the text)
        gameObject.transform.position += direction;

    }
}
