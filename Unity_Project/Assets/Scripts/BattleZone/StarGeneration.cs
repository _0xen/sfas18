﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarGeneration : MonoBehaviour
{
    // The center of the star field
    public Vector2 starFieldCenter;
    // What is the dimensions of the start field if it was centered at 'starFieldCenter'
    public float starFieldDim;
    // Define the min and max z distance
    // x: min
    // y: max
    public Vector2 starsMinMaxZ;
    // Define the min and max size of a star
    // x: min
    // y: max
    public Vector2 starsMinMaxSize;
    // Store the max amount of stars we want generated
    public int starCount;

    // Store all the stars in a particle array
    private ParticleSystem.Particle[] stars;
    
    // Use this for initialization
    void Start ()
    {
        GenerateStars();
    }

    // Generates stars based on the parameters passed to the script
    private void GenerateStars()
    {
        // Initialize the array with particles
        stars = new ParticleSystem.Particle[starCount];
        // Loop through for all instances of the particles
        for(int i = 0; i < starCount; i++)
        {
            // Define a vec2 for the x and y position for the particle
            Vector2 starPosition = Random.insideUnitSphere * starFieldDim;
            // Set the position of the star to the x and y position then calculate the z position based on the range provided
            stars[i].position = new Vector3(starPosition.x, Random.Range(starsMinMaxZ.x, starsMinMaxZ.y), starPosition.y);
            // Set the size of the star based on the min and max size
            stars[i].startSize = Random.Range(starsMinMaxSize.x, starsMinMaxSize.y);
            // Set the color of the star to be slightly red and blue shifted
            stars[i].startColor = new Color(Random.Range(0.7f, 1.0f), 1, Random.Range(0.7f, 1.0f), 1);
        }
        // Now that the particles are setup, render them
        gameObject.GetComponent<ParticleSystem>().SetParticles(stars, stars.Length);
    }
}
