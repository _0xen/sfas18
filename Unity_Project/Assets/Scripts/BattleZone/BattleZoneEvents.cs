﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;



public class BattleZoneEvents : MonoBehaviour
{
    // Store a instance of the battle zone
    public static BattleZoneEvents instance;

    // Define the background music of the hanger
    public AudioClip backgroundMusic;
    // Define the background music of the hanger
    public AudioClip[] sfxClips;
    // The scene name that will be used when we require to navigate back to the hanger
    public string hangerSceneName;
    // Textures
    public Texture2D mainTexture;
    // Basic block object for all objects
    public GameObject blockObject;
    // Basic block object for the battle ground bounds
    public GameObject battleZoneBoundary;
    // Explosion Object
    public GameObject explosionObject;
    // Basic projectile object
    public GameObject projectileObject;
    // Health pop-up
    public GameObject healthPopup;
    // Sprite sheet block size
    public int tileSize;
    // Define the speed that the camera will zoom in and out when scrolling the scroll wheel
    public float zoomScrollSpeed;
    // How far can the user zoom into the screen
    public float zoomLowerBounds;
    // How far out can the user zoom the camera
    public float zoomUperBounds;
    // How long the game should last for
    public int roundLength;
    // Define the length of the health bar
    public float healthBarLength;


    // Stores the instance of the players ship
    private int playerShipID;

    // Time represented as tick
    private int tick;
    // Stores the current camera zoom state
    private bool zoomedOut = false;
    // Used to store the timer ui instance
    private Text timeLeftText;

    // Stores the key codes for movement
    private KeyCode[] movmentKeycodes = new KeyCode[4] { KeyCode.W, KeyCode.D, KeyCode.S, KeyCode.A };
    // Stores the 4 player ui instances
    private CanvasGroup[] PlayerStatusUIs = new CanvasGroup[4];

    // Used to store the main music object instance
    private GameObject MainMusic;
    // Used to store the sfx object instance
    private SFXController SFXInstance;

    // Use this for initialization
    void Start ()
    {
        // Define the current instance of the battle zone event, this is so it is accessible from other classes
        instance = this;

        // Get the music game object instance
        MainMusic = GameObject.Find("MainMusic");
        // Switch the audio source
        MainMusic.GetComponent<AudioHandler>().SwitchClip(backgroundMusic);
        // Define the instances of sfx clips
        DataStorage.SFXClips = sfxClips;
        // Get the sfxInstance
        SFXInstance = GameObject.Find("SFXController").GetComponent<SFXController>();
        // Draw the map boundary
        DrawmapBoundary();
        // If we are debugging and we started up on this scene, then return the user to
        // the hanger
        if (DataStorage.Connection == DataStorage.ConnectionType.None)
        {
            // Return to the hanger
            Utilities.ReturnToHangerFromMultiplayer();
            return;
        }
        // Make sure all ship blocks are load into ram
        Utilities.PrepairShipBlocks();
        // Setup class instances of objects and store them in there respective static holders
        DataStorage.BlockPrefab = blockObject;
        DataStorage.ExplosionPrefab = explosionObject;
        //DataStorage.BlockShader = blockShader;
        DataStorage.SpriteSheetSpriteSize = tileSize;
        DataStorage.SpriteSheetSpriteWidth = mainTexture.width / tileSize;
        DataStorage.SpriteSheetSpriteHeight = mainTexture.height / tileSize;
        DataStorage.TexturesInstance = mainTexture;

        // Loop through all player ui components and store them in the array
        for (int i = 0; i < 4; i++)
            PlayerStatusUIs[i] = GameObject.Find("Player" + (i + 1)).GetComponent<CanvasGroup>();

        // Initialize the ships array
        DataStorage.BattleGroundShips = new Dictionary<int, Ship>();
        // Loop through all ship data that was passed to the client during the lobby connection
        foreach (DataStorage.ShipLobbyData shipData in DataStorage.ClientPlayersInstance.Values)
        {
            // Add the current ship into the ships array
            DataStorage.BattleGroundShips.Add(shipData.id, new Ship(shipData.data, shipData.id));
            // Get the location of where the ship should spawn
            Vector3 spawnPoint = GameObject.Find("Spawn" + shipData.id).transform.position;
            // Position the ship at the required location
            DataStorage.BattleGroundShips[shipData.id].GetShipObject().transform.position = new Vector3(spawnPoint.x, spawnPoint.y, 0.0f);
            // Show the current players stats ui
            Utilities.ShowUI(PlayerStatusUIs[shipData.id - 1]);
            // Pass the current players name to the UI name field
            GameObject.Find("Player" + shipData.id + "Name").GetComponent<Text>().text = shipData.name;
            // If the current ship is the users ship
            if (DataStorage.ClientData.id == shipData.id)
            {
                // Set the users ship instance to the current ship
                playerShipID = shipData.id;
                // Position the camera at the spawn point
                Camera.main.transform.position = new Vector3(spawnPoint.x, spawnPoint.y, -30.0f);
                // Attach the camera to the current ship
                Camera.main.transform.parent = DataStorage.BattleGroundShips[playerShipID].GetShipObject().transform;
            }
        }

        // Initialize the time left ui component
        timeLeftText = GameObject.Find("TimeLeft").GetComponent<Text>();

        // Setup all packet listeners for the client
        SetupHandlers();
        // Initialize the tick variable
        tick = 0;
        // Start delayed update that will control ship animations /etc
        InvokeRepeating("UpdateTick", 0, 0.2f);
        // Round timer loop
        InvokeRepeating("RoundTimerLoop", 1.0f, 1.0f);
    }

    // Update the clock on the ui
    void RoundTimerLoop()
    {
        lock (DataStorage.Lock_)
        {
            // Deduct one from the round length
            roundLength--;
            // format the time output that the seconds will always be two digits long
            timeLeftText.text = (roundLength / 60) + ":" + string.Format("{0:00}", (roundLength % 60));
            // If the timer expires, return to the lobby
            if (roundLength <= 0)
            {
                // Loop through each player ship
                foreach (Ship ship in DataStorage.BattleGroundShips.Values)
                {
                    // Pass the kills and deaths to the lobby data
                    DataStorage.ClientPlayersInstance[ship.GetID()].kills = ship.GetKills();
                    DataStorage.ClientPlayersInstance[ship.GetID()].deaths = ship.GetDeaths();
                }
                SceneManager.LoadScene("Lobby");
            }
        }
    }

    // Setup all packet listeners for the client
    void SetupHandlers()
    {
        lock (DataStorage.Lock_)
        {
            // Pass the position callback function pointer
            DataStorage.BattleZoneHandlesInstance.OnPlayerPositionCallbackMethod = OnPlayerPositionCallback;
            DataStorage.BattleZoneHandlesInstance.OnBlockUpdate = OnBlockUpdate;
            DataStorage.BattleZoneHandlesInstance.OnPlayerDeath = OnPlayerDeath;
            DataStorage.BattleZoneHandlesInstance.OnPlayerRespawn = OnPlayerRespawn;
            DataStorage.BattleZoneHandlesInstance.OnProjectileShot = OnProjectileShot;
            // Attach the passed functions to the client handlers
            DataStorage.Client.SetupBattleZoneHandles();
        }
    }

    // Called when a player re-spawns
    private void OnPlayerRespawn(NetworkMessage netMsg)
    {
        lock (DataStorage.Lock_)
        {
            // Read the re-spawns message
            DataStorage.RespawnMessage respawnMessage = netMsg.ReadMessage<DataStorage.RespawnMessage>();
            // If the player is not alive, bring them back
            if (!DataStorage.BattleGroundShips[respawnMessage.id].IsAlive())
            {
                // Reset the ships thrust, momentum, etc
                DataStorage.BattleGroundShips[respawnMessage.id].SetAlive(true);
                DataStorage.BattleGroundShips[respawnMessage.id].UpdateShipAfterChange();
                DataStorage.BattleGroundShips[respawnMessage.id].flags = 0;
                DataStorage.BattleGroundShips[respawnMessage.id].thrust = new Vector3();
                DataStorage.BattleGroundShips[respawnMessage.id].momentum = new Vector3();
                DataStorage.BattleGroundShips[respawnMessage.id].GetShipObject().transform.position = respawnMessage.position;
            }
        }
    }
    
    // Called when a projectile is shot
    private void OnProjectileShot(NetworkMessage netMsg)
    {
        lock (DataStorage.Lock_)
        {
            // Read the projectile message
            DataStorage.ShotMessage shotMessage = netMsg.ReadMessage<DataStorage.ShotMessage>();
            // Spawn the projectile
            FireShot(shotMessage.shipID, shotMessage.blockID, shotMessage.shotID, shotMessage.position, shotMessage.rotation);
        }
    }

    // called when a on player death update packet is received from the server
    private void OnPlayerDeath(NetworkMessage netMsg)
    {
        lock (DataStorage.Lock_)
        {
            // Read the player death message
            DataStorage.PlayerDeathMessage playerDeath = netMsg.ReadMessage<DataStorage.PlayerDeathMessage>();
            // Add a kill to the players kill counter
            // If the killer was the sun it wont have a id so skip it
            if(DataStorage.BattleGroundShips.ContainsKey(playerDeath.killerID)) DataStorage.BattleGroundShips[playerDeath.killerID].AddKill();
            // Add a kill to the players death counter
            DataStorage.BattleGroundShips[playerDeath.deadPlayerID].AddDeath();
            // If the player is alive
            if (DataStorage.BattleGroundShips[playerDeath.deadPlayerID].IsAlive())
            {
                // Reset the players position, movement, etc
                DataStorage.BattleGroundShips[playerDeath.deadPlayerID].SetAlive(false);
                DataStorage.BattleGroundShips[playerDeath.deadPlayerID].flags = 0;
                DataStorage.BattleGroundShips[playerDeath.deadPlayerID].thrust = new Vector3();
                DataStorage.BattleGroundShips[playerDeath.deadPlayerID].momentum = new Vector3();
            }
        }
    }

    // Called when a block update packet is received from the server
    void OnBlockUpdate(NetworkMessage netMsg)
    {
        lock (DataStorage.Lock_)
        {
            // Read the data packet from the server
            DataStorage.BlockUpdate blockUpdate = netMsg.ReadMessage<DataStorage.BlockUpdate>();

            // Get the instance of the ship referenced from the data packet
            Ship ship = DataStorage.BattleGroundShips[blockUpdate.shipId];

            // If the ship is dead, no point updating its health
            if (!ship.IsAlive()) return;

            // If the health has changed, continue
            if (blockUpdate.healthChange != 0)
            {
                // Create a instance of the health change pop up
                GameObject.Instantiate(healthPopup, 
                    ship.GetPart(blockUpdate.blockX, blockUpdate.blockY).blockObject.transform.position, 
                    Quaternion.identity).GetComponent<HealthPopup>().SetHealthChange(blockUpdate.healthChange);
                // If the block has took damage, continue
                if (blockUpdate.healthChange < 0)
                {
                    // Spawn a explosion particle effect
                    Utilities.SpawnExplosion(
                        ship.GetPart(blockUpdate.blockX, blockUpdate.blockY).blockObject.GetComponent<Renderer>().bounds.center
                        + new Vector3(0.0f, 0.0f, -1.0f));
                    // Play the explosion effect
                    SFXInstance.PlaySFX(DataStorage.SFXClips[2], SFXController.SFXSource.Channel2, 10);
                }
            }

            // Update the block in question
            ship.UpdateBlock(blockUpdate.alive, new Vector2Int(blockUpdate.blockX, blockUpdate.blockY), blockUpdate.healthChange);
            // Update the ships general information after the blocks information has been updated
            ship.UpdateShipAfterChange();
        }
    }

    // When a player moves, call this function
    void OnPlayerPositionCallback(NetworkMessage netMsg)
    {
        lock (DataStorage.Lock_)
        {
            // Get the position data from the message
            DataStorage.PlayerPosition position = netMsg.ReadMessage<DataStorage.PlayerPosition>();
            // If we received the clients position, return
            if (position.id == DataStorage.ClientData.id) return;
            // Get the current ships instance
            Ship ship = DataStorage.BattleGroundShips[position.id];
            if (ship != null && ship.GetShipObject() != null) 
            {
                // Set the ships position
                ship.GetShipObject().transform.position = position.position;
                // Set the ships rotation
                ship.GetRotationObject().transform.rotation = position.rotation;
                // Set the ships thrust
                ship.thrust = position.thrust;
                // Set the ships momentum
                ship.momentum = position.momentum;
                // Set the ships flags for such things as animation states
                ship.flags = position.flags;
                // Set the required rotation of the ship
                ship.rotationDirection = position.rotationDirection;
            }
        }
    }

    // Update the users UI interface
    void UpdateUI()
    {
        lock (DataStorage.Lock_)
        {
            foreach (Ship ship in DataStorage.BattleGroundShips.Values)
            {
                // Update the size of the health bar based on the block's health change
                GameObject.Find("Player" + ship.GetID() + "HealthbarHealth").GetComponent<RectTransform>().sizeDelta = new Vector2(healthBarLength * ((float)ship.GetCurrentHealth() / (float)ship.GetHealth()), 20.0f);
                // Update kills and deaths
                GameObject.Find("Player" + ship.GetID() + "Stats").GetComponent<Text>().text = "K:" + ship.GetKills() + " D:" + ship.GetDeaths();
            }
        }
    }

    // Called once every x seconds to update animations
    void UpdateTick()
    {
        lock (DataStorage.Lock_)
        {
            // Increment the tick value
            tick++;
            // Loop through the ships and update
            foreach (Ship ship in DataStorage.BattleGroundShips.Values)
            {
                // Update the current ship
                if (ship.IsAlive())
                {
                    // Calculate the vector between the ships posiiton and the sun and pass it to the ship
                    ship.solarBody = (GameObject.Find("Sun Outer").transform.position - ship.GetShipObject().transform.position).normalized * DataStorage.GravityPull;
                    ship.UpdateTick(tick);
                }
            }
        }

        // If the player is shooting
        if ((DataStorage.BattleGroundShips[playerShipID].flags & DataStorage.ShipFlags.Shooting) == DataStorage.ShipFlags.Shooting)
        {
            // Loop through each part within the ship
            foreach (ShipPart part in DataStorage.BattleGroundShips[playerShipID].GetParts())
            {
                //  If the part is a weapon
                if (part.block is WeaponBlock)
                {
                    // Get the block as a weapon block
                    WeaponBlock weapon = (WeaponBlock)part.block;
                    // If the block is powered and can shoot AND is
                    if (part.IsActive() && weapon.isPowered() && weapon.CanShoot())
                    {
                        // Discharge the weapon of the available power
                        weapon.DischargeAfterShot();
                        // Tell the server that we are shooting
                        DataStorage.Client.SendShot(playerShipID, weapon.ID, part.blockObject.transform.position + part.blockObject.transform.up, part.blockObject.transform.rotation);
                    }
                }
            }
        }
    }

    // Called when we spawn a projectile
    private void FireShot(int shipID, int blockId,int shotID, Vector3 position, Quaternion rotation)
    {
        // Create a instance of the projectile
        GameObject projectile = GameObject.Instantiate(projectileObject, position, rotation);
        // If the block at the block is in fact a weapon, continue
        if (Block.Blocks[blockId] is WeaponBlock)
        {
            // Get the instance of the weapon
            WeaponBlock weapon = (WeaponBlock)Block.Blocks[blockId];
            // Pass the relevant weapon data to the projectile script
            projectile.gameObject.transform.localScale = weapon.ProjectileSize;
            projectile.gameObject.GetComponent<Renderer>().material.color = weapon.ProjectileColor;

            projectile.GetComponent<ProjectileMovment>().speed = weapon.ProjectileSpeed;
            projectile.GetComponent<ProjectileMovment>().timeBeforeDestruction = weapon.ProjectileLifeTime;
            projectile.GetComponent<ProjectileMovment>().weaponblockID = blockId;
            projectile.GetComponent<ProjectileMovment>().projectileShipID = shipID;
            projectile.GetComponent<ProjectileMovment>().seeker = weapon.Seeker;
            // Pass the shot id to the projectile script
            projectile.GetComponent<ProjectileMovment>().shotID = shotID;
            // Add the ship id to the projectile tag
            projectile.transform.tag += shipID.ToString();
            // Play the lazor sound effect
            SFXInstance.PlaySFX(DataStorage.SFXClips[1], SFXController.SFXSource.Channel1);
        }
    }

    // Update is called once per frame
    void Update()
    {
        lock (DataStorage.Lock_)
        {
            // If we are not connected to a server, return (Used for Development)
            if (DataStorage.Connection == DataStorage.ConnectionType.None) return;
            // If the player ship is alive but dose not have power, then it should die
            if (DataStorage.BattleGroundShips[playerShipID].IsAlive() && !DataStorage.BattleGroundShips[playerShipID].hasPowerSystem())
            {
                // Set the ship to not alive
                DataStorage.BattleGroundShips[playerShipID].SetAlive(false);
                // Send the players death to the server
                DataStorage.Client.SendDeath(DataStorage.BattleGroundShips[playerShipID].GetID());
                return;
            }
            // If the ship is not alive, continue
            else if (!DataStorage.BattleGroundShips[playerShipID].IsAlive())
            {
                return;
            }
            // if the mouse is over a ui element, don't let them move or shoot
            if (Utilities.IsMouseOverUI()) return;
            // If the mouse wheel was scrolled, zoom the camera accordingly
            Camera.main.transform.Translate(Vector3.forward * (Input.GetAxis("Mouse ScrollWheel") * zoomScrollSpeed));
            // If the camera zoom has went out of the zoom upper bounds, lock it at the max zoom
            if (Camera.main.transform.position.z < zoomUperBounds)
                Camera.main.transform.position = new Vector3(
                   Camera.main.transform.position.x,
                   Camera.main.transform.position.y,
                   zoomUperBounds);
            // If the camera zoom has went out of the zoom lower bounds, lock it at the min zoom
            if (Camera.main.transform.position.z > zoomLowerBounds)
                Camera.main.transform.position = new Vector3(
                   Camera.main.transform.position.x,
                   Camera.main.transform.position.y,
                   zoomLowerBounds);

            // if the mouse scroll wheel is pressed
            if (Input.GetMouseButtonDown(2))
            {
                // Toggle between the lower and upper zoom values based on 'zoomedOut' value
                Camera.main.transform.position = new Vector3(Camera.main.transform.position.x,
                    Camera.main.transform.position.y,
                    zoomedOut ? zoomLowerBounds : zoomUperBounds);
                // Toggle the position of the camera
                zoomedOut = !zoomedOut;
            }
            // If the left click button was pressed, start the firing animation
            if (Input.GetMouseButtonDown(0))
            {
                DataStorage.BattleGroundShips[playerShipID].BeginShooting();
            }
            // If the left click button was released, stop the firing animation
            if (Input.GetMouseButtonUp(0))
            {
                DataStorage.BattleGroundShips[playerShipID].EndShooting();
            }

            // If the user presses the escape key, open the battle menu
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                // check to see if the ui is open
                if (Utilities.IsUIOpen(GameObject.Find("BattleMenuModel").GetComponent<CanvasGroup>()))
                {
                    // Close the UI
                    Utilities.HideUI(GameObject.Find("BattleMenuModel").GetComponent<CanvasGroup>());
                }
                else
                {
                    // Open the UI
                    Utilities.ShowUI(GameObject.Find("BattleMenuModel").GetComponent<CanvasGroup>());
                }
                Utilities.HideUI(GameObject.Find("SettingsModel").GetComponent<CanvasGroup>());
                // Play the ui click sfx
                SFXInstance.PlaySFX(DataStorage.SFXClips[0], SFXController.SFXSource.Channel1);
            }

            // Loop through each instance of a key code
            for (int i = 0; i < movmentKeycodes.Length; i++)
            {
                // If the current key is pressed, start thrusting in that direction
                if (Input.GetKeyDown(movmentKeycodes[i]))
                {
                    DataStorage.BattleGroundShips[playerShipID].BeingThrust(i);
                }
                // If the current key is released, stop thrusting in that direction
                if (Input.GetKeyUp(movmentKeycodes[i]))
                {
                    DataStorage.BattleGroundShips[playerShipID].EndThrust(i);
                }
            }
        }
    }


    // Called once every x milliseconds
    void FixedUpdate()
    {
        lock (DataStorage.Lock_)
        {
            // If we are not connected to a server, return (Used for Development)
            if (DataStorage.Connection == DataStorage.ConnectionType.None) return;
            // Get the current mouse position
            Vector3 currentMousePosition = Input.mousePosition;
            // Get the current mouse position ray-cast to intersect with the ships plane
            Vector3 targetPosition = Camera.main.ScreenToWorldPoint(new Vector3(currentMousePosition.x, currentMousePosition.y, -Camera.main.transform.position.z));
            // Calculate the ships direction vector based on the mouse position
            Vector3 shipDirectionVector = (targetPosition - DataStorage.BattleGroundShips[playerShipID].GetShipObject().transform.position).normalized;
            // Rotate the ship towards the ship direction vector
            DataStorage.BattleGroundShips[playerShipID].rotationDirection = shipDirectionVector;
            
            // Loop through each ship and update its position
            foreach (Ship ship in DataStorage.BattleGroundShips.Values)
            {
                // make sure the ship is alive before updating it
                if (ship.IsAlive())
                {
                    ship.Update();
                }
                else
                {
                    // Force set the ship to y-100f
                    DataStorage.BattleGroundShips[ship.GetID()].GetShipObject().transform.position = new Vector3(0.0f, -500.0f, 0.0f);
                }
            }
            // Update the ui
            UpdateUI();
            // Send the players ship data to the server
            DataStorage.Client.SendPosiiton(DataStorage.BattleGroundShips[playerShipID]);

            // Checks to see if we are the server if we are then, call server update
            if (DataStorage.Connection == DataStorage.ConnectionType.ServerClient)
                ServerUpdate();
        }
    }

    // Called when a shot has collided with a ship
    public void ShotCollided(int shotID, int shipID, int weaponblockID,GameObject collidedBlock)
    {
        Vector2Int shipBlockPosition = new Vector2Int(-1, -1);
        // Loop through the ship parts to get the referenced block
        foreach (ShipPart part in DataStorage.BattleGroundShips[shipID].GetParts())
        {
            // if we find a instance of the block, set the position and break
            if (part.blockObject == collidedBlock)
            {
                shipBlockPosition = part.GetPosition();
                break;
            }
        }
        // Send the collision to the server
        DataStorage.Client.SendShotCollision(shotID, shipID, weaponblockID, shipBlockPosition);
    }

    public void ShipPartCollidedWithSolarObeject(int shipIndex, GameObject shipblock)
    {
        lock (DataStorage.Lock_)
        {
            // Create holders for the ship blocks positions
            Vector2Int shipBlockPosition = new Vector2Int(-1, -1);
            
            // Loop through the ship parts to get the referenced block
            foreach (ShipPart part in DataStorage.BattleGroundShips[shipIndex].GetParts())
            {
                // if we find a instance of the block, set the position and break
                if (part.blockObject == shipblock)
                {
                    shipBlockPosition = part.GetPosition();
                    break;
                }
            }
            // If we could not find a block, return 
            if (shipBlockPosition.x < 0 || shipBlockPosition.y < 0)
                return;
            DataStorage.Client.SendBlockDamage(shipIndex, shipBlockPosition);
        }
    }

    // Called when two ships have collided together
    public void ShipPartCollided(int shipIndex1, int shipIndex2, GameObject shipblock1, GameObject shipblock2)
    {
        lock (DataStorage.Lock_)
        {
            // Create holders for the ship blocks positions
            Vector2Int ship1BlockPosition = new Vector2Int(-1, -1);
            Vector2Int ship2BlockPosition = new Vector2Int(-1, -1);

            // Loop through the ship parts to get the referenced block
            foreach (ShipPart part in DataStorage.BattleGroundShips[shipIndex1].GetParts())
            {
                // if we find a instance of the block, set the position and break
                if (part.blockObject == shipblock1)
                {
                    ship1BlockPosition = part.GetPosition();
                    break;
                }
            }

            // Loop through the ship parts to get the referenced block
            foreach (ShipPart part in DataStorage.BattleGroundShips[shipIndex2].GetParts())
            {
                // if we find a instance of the block, set the position and break
                if (part.blockObject == shipblock2)
                {
                    ship2BlockPosition = part.GetPosition();
                    break;
                }
            }
            // If we could not find a block, return 
            if (ship1BlockPosition.x < 0 || ship1BlockPosition.y < 0 ||
                ship2BlockPosition.x < 0 || ship2BlockPosition.y < 0)
                return;

            // Calculate the speed that the two ships hit each other using there momentum
            float speed = (DataStorage.BattleGroundShips[shipIndex2].momentum - DataStorage.BattleGroundShips[shipIndex1].momentum).magnitude;
            // Calculate the new momentum's for the two ships positions
            Vector3 shipNewMomentum = DataStorage.BattleGroundShips[shipIndex1].GetShipObject().transform.position - DataStorage.BattleGroundShips[shipIndex2].GetShipObject().transform.position;
            // Normalize the momentum
            shipNewMomentum.Normalize();
            // If the ship is traveling very slow, give it a momentum boost
            if (speed / 2 < 0.04f)
                shipNewMomentum *= 0.04f;
            else
                shipNewMomentum *= speed / 2; // Calculate the momentum based on the speed / 2

            // Apply the momentums to the ships momentums
            DataStorage.BattleGroundShips[shipIndex1].momentum = shipNewMomentum;
            DataStorage.BattleGroundShips[shipIndex2].momentum = -shipNewMomentum * 1.25f;
            // Send the ships collision to the server
            DataStorage.Client.SendShipCollision(shipIndex1, shipIndex2, ship1BlockPosition, ship2BlockPosition, speed);
        }
    }

    // Called once per update if we are running on the server
    private void ServerUpdate()
    {
        // Call the update function within the server
        DataStorage.Server.Update();
    }

    // Called when the game starts up to draw the boundary for the whole map
    private void DrawmapBoundary()
    {
        // Create a instance of the block object
        GameObject TopBoundary = GameObject.Instantiate(battleZoneBoundary, new Vector3(0.0f, -10.0f, 0.0f), new Quaternion());
        // Set the position of the boundary to the edge of the map
        // Use the boundary thickness to make it so the user cant see space outside the bounds
        TopBoundary.transform.position = new Vector3(0.0f, DataStorage.MapBounderySize.y + (DataStorage.MapBounderyThickness / 2), -1.0f);
        // Set the blocks scale
        TopBoundary.transform.localScale = new Vector3(DataStorage.MapBounderySize.x * 2, DataStorage.MapBounderyThickness, 0.001f);
        TopBoundary.GetComponent<Renderer>().material.color = Utilities.BorderColor;

        // Create a instance of the block object
        GameObject BottomBoundary = GameObject.Instantiate(battleZoneBoundary, new Vector3(0.0f, -10.0f, 0.0f), new Quaternion());
        // Set the position of the boundary to the edge of the map
        // Use the boundary thickness to make it so the user cant see space outside the bounds
        BottomBoundary.transform.position = new Vector3(0.0f, -DataStorage.MapBounderySize.y - (DataStorage.MapBounderyThickness / 2), -1.0f);
        // Set the blocks scale
        BottomBoundary.transform.localScale = new Vector3(DataStorage.MapBounderySize.x * 2, DataStorage.MapBounderyThickness, 0.001f);

        // Create a instance of the block object
        GameObject LeftBoundary = GameObject.Instantiate(battleZoneBoundary, new Vector3(0.0f, -10.0f, 0.0f), new Quaternion());
        // Set the position of the boundary to the edge of the map
        // Use the boundary thickness to make it so the user cant see space outside the bounds
        LeftBoundary.transform.position = new Vector3(DataStorage.MapBounderySize.x + (DataStorage.MapBounderyThickness / 2), 0.0f, -1.0f);
        // Set the blocks scale
        LeftBoundary.transform.localScale = new Vector3(DataStorage.MapBounderyThickness, (DataStorage.MapBounderySize.y * 2) + (DataStorage.MapBounderyThickness * 2), 0.001f);

        // Create a instance of the block object
        GameObject RightBoundary = GameObject.Instantiate(battleZoneBoundary, new Vector3(0.0f, -10.0f, 0.0f), new Quaternion());
        // Set the position of the boundary to the edge of the map
        // Use the boundary thickness to make it so the user cant see space outside the bounds
        RightBoundary.transform.position = new Vector3(-DataStorage.MapBounderySize.x - (DataStorage.MapBounderyThickness / 2), 0.0f, -1.0f);
        // Set the blocks scale
        RightBoundary.transform.localScale = new Vector3(DataStorage.MapBounderyThickness, (DataStorage.MapBounderySize.y * 2) + (DataStorage.MapBounderyThickness * 2), 0.001f);
    }
}
