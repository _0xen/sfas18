﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ProjectileMovment : MonoBehaviour
{
    // Set the speed of the current projectile
    public float speed;
    // Set the lifespan of the projectile
    public float timeBeforeDestruction = 3.0f;
    // This stores the weapon block id, this is so we know how much damage to deal
    public int weaponblockID;
    // The id that is assigned to this shot
    public int shotID;
    // Get the ship id
    public int projectileShipID;
    // Will it follow ships
    public bool seeker = true;

    private Ship ClosestShip = null;

    // Use this for initialization
    void Start()
    {
        // After x time, destroy the projectile
        Destroy(gameObject, timeBeforeDestruction);
        // Rstart a slow timer to follow ships
        InvokeRepeating("Retarget", 0.0f, 0.5f);
    }

    void Retarget()
    {

        float distance = 0.0f;
        ClosestShip = null;


        foreach (Ship ship in DataStorage.BattleGroundShips.Values)
        {
            if(ship.IsAlive())
            {
                if (ship.GetID() == projectileShipID) continue;
                float distanceBetween = Vector3.Distance(gameObject.transform.position, ship.GetShipObject().transform.position);
                if (ClosestShip == null || distance > distanceBetween)
                {
                    distance = distanceBetween;
                    ClosestShip = ship;
                }
            }
        }


    }

    // Update is called once per frame
    void Update()
    {


        if (seeker)
        {
            // Move the projectile along its y axis based on the speed
            if (ClosestShip != null)
            {
                Vector3 directionVector = (ClosestShip.GetShipObject().transform.position - gameObject.transform.position).normalized;
                float distance = Mathf.Abs((ClosestShip.GetShipObject().transform.position - gameObject.transform.position).magnitude);

                float angle = Vector3.Angle(gameObject.transform.up, directionVector);

                if (Mathf.Abs(angle) < 45.0f && distance < 20.0f)
                {
                    gameObject.transform.localPosition += directionVector * speed;
                }
                else
                {
                    gameObject.transform.localPosition += (gameObject.transform.up * speed);
                }
            }
            else
            {
                gameObject.transform.localPosition += (gameObject.transform.up * speed);
            }
        }
        else
        {
            // Move the projectile along its y axis based on the speed
            gameObject.transform.localPosition += (gameObject.transform.up * speed);
        }


        // should it follow ships
        if (seeker)
        {
            /*//targetPosition
            Debug.Log("test");
            // Calculate the angle between the 
            float angle = Vector3.Angle(gameObject.transform.up, targetPosition);
            float rotationSpeed = 2.0f;
            Quaternion newRotation;
            if (Vector3.Angle(gameObject.transform.up, targetPosition) < Vector3.Angle(-gameObject.transform.up, targetPosition))
                newRotation = Quaternion.Euler(0, 0, gameObject.transform.rotation.eulerAngles.z - angle);
            else
                newRotation = Quaternion.Euler(0, 0, gameObject.transform.rotation.eulerAngles.z + angle);
            gameObject.transform.rotation = Quaternion.RotateTowards(gameObject.transform.rotation, newRotation, rotationSpeed);**/


        }
    }

    // Called when a object collides with this one
    void OnCollisionEnter2D(Collision2D coll)
    {
        // If either of the objects are not active, return
        if (!gameObject.activeInHierarchy) return;
        if (!coll.collider.gameObject.activeInHierarchy) return;
        // If the object we collide with contains "Ship" in its tag, continue
        if (coll.collider.gameObject.tag.Contains("Ship"))
        {
            // Get the ship id of the ship we collided with
            int shipID = Int32.Parse(coll.collider.gameObject.tag.Replace("Ship", ""));
            // If the ship id is different to the projectile ship id, continue
            if(shipID!= projectileShipID)
            {
                // Destroy the object as it collided with the ship
                Destroy(gameObject);
                // Return the collision of the projectile and the ship to the battle zone class
                BattleZoneEvents.instance.ShotCollided(shotID, shipID, weaponblockID, coll.collider.gameObject);
            }
        }
    }
}