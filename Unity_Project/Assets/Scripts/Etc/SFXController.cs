﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXController : MonoBehaviour
{
    // Store a instance of the audio source
    public GameObject audioSourcePrefabInstance;

    public enum SFXSource {
        Channel1=0,
        Channel2=1,
        Channel3=2
    };

    private Dictionary<SFXSource, List<GameObject>> sources;

    // Store a instance of the DataStoreage block
    public static SFXController Instance;
    // Call when the script starts up
    void Awake()
    {
        // If there is already a instance of 'Instance' then destroy this instance
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        // Set the value of instance to be this
        Instance = this;
        // Define that when we switch scenes that we do not want the object to be destroyed
        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start ()
    {
        // Create a instance of the source dictionary
        sources = new Dictionary<SFXSource, List<GameObject>>();
        // Loop through the sfx types and add them to the dictionary
        foreach (SFXSource sfx in Enum.GetValues(typeof(SFXSource)))
        {
            sources.Add(sfx, new List<GameObject>());
        }
    }

    // Called when we need to play a sfx file
    public void PlaySFX(AudioClip clip, SFXSource sfx, int limit = 0)
    {
        // If the limit = 0 or we have not met the limit yet, continue
        if (limit == 0 || limit > 0 && sources[sfx].Count < limit)
        {
            // Create a instance of the audio prefab
            GameObject obj = GameObject.Instantiate(audioSourcePrefabInstance, new Vector3(0.0f, 0.0f, 0.0f), Quaternion.identity);

            obj.GetComponent<SFXHandler>().SwitchClip(clip);
            sources[sfx].Add(obj);
        }
    }


	void Update ()
    {
        // Loop through the sfx types
        foreach (SFXSource sfx in Enum.GetValues(typeof(SFXSource)))
        {
            // Loop through the sources backwards
            for(int i = sources[sfx].Count - 1; i >= 0; i--)
            {
                if (sources[sfx][i] == null)
                {
                    // Remove the instance of it
                    sources[sfx].Remove(sources[sfx][i]);
                }else
                // If the object contains a clip and it has stopped playing
                if (sources[sfx][i].GetComponent<AudioSource>().clip != null && !sources[sfx][i].GetComponent<AudioSource>().isPlaying)
                {
                    Destroy(sources[sfx][i]);
                }
            }
        }
    }
}
