﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXHandler : MonoBehaviour
{
    // Store a instance of the audio source
    private AudioSource source;

    // Use this for initialization
    void Start()
    {
        // Set the source of the audio
        source = gameObject.GetComponent<AudioSource>();
        // Set the audio source volume
        source.volume = PlayerPrefs.GetFloat("SFXVolume", 0.5f);
    }

    // Switch the audio source
    public void SwitchClip(AudioClip clip)
    {
        // If the source happens not to exist, return
        if (source == null)
        {
            Start();
        }
        // If the clip dose not exist, return
        if (clip == null)
        {
            source.Stop();
            return;
        }
        // If the source is different, switch it
        if (source.clip != clip)
        {
            // Set the audio source
            source.clip = clip;
            // Start playing the music
            source.Play();
        }
    }
}
