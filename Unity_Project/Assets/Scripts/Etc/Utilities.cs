﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System.IO;
using System.Text.RegularExpressions;

public static class Utilities
{
    // Define the maximum ship cost
    public static int MaxShipCost = 1500;

    // Define the different block shader colors
    public static Vector4 BlockColorRed = new Vector4(1.0f, 0.8f, 0.8f, 1.0f);
    public static Vector4 BlockColorGreen = new Vector4(0.6f, 1.0f, 0.8f, 1.0f);
    public static Vector4 BlockColor = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);
    public static Vector4 BorderColor = new Vector4(0.0f, 0.0f, 1.0f, 0.4f);

    // Define a instance of the projectile object
    public static GameObject projectileInstance;

    // This is unfortunately a nessesery setup as I need to verify that all inherited block types are loaded into memory before we continue
    public static void PrepairShipBlocks()
    {
        // Nessesery to have the bool value passed onto something, but this is just a formality
        if(!Block.block_object_compiled ||
            !PoweredBlock.powered_block_object_compiled ||
            !EngineBlock.engine_block_object_compiled ||
            !WeaponBlock.wepon_block_object_compiled)
        {
            Debug.LogError("Block Objects not compiled!");
        }
    }

    // Called when we want to spawn a explosion
    public static void SpawnExplosion(Vector3 pos)
    {
        // Create a instance of the explosion efect
        GameObject obj = GameObject.Instantiate(DataStorage.ExplosionPrefab, pos, Quaternion.identity);
        // Play the effect
        obj.GetComponent<ParticleSystem>().Play();
    }

    // Get a texture from the sprite sheet at the requested position and at x size
    public static Texture2D GetSpriteSheetTexture(Vector2Int pos, Vector2Int textureDim)
    {
        // Create a new texture of x width and height based on the texture dimensions
        Texture2D tex = new Texture2D(DataStorage.SpriteSheetSpriteSize * textureDim.x, DataStorage.SpriteSheetSpriteSize * textureDim.y);
        // Move the relevant pixels to the new texture
        tex.SetPixels(DataStorage.TexturesInstance.GetPixels(pos.x * DataStorage.SpriteSheetSpriteSize, pos.y * DataStorage.SpriteSheetSpriteSize, DataStorage.SpriteSheetSpriteSize * textureDim.x, DataStorage.SpriteSheetSpriteSize * textureDim.y));
        // Set the pixels to the texture sheet
        tex.Apply();
        return tex;
    }

    // Save the ship under the defined path
    public static void SaveShip(List<ShipGrid> ship_grid, string shipPath)
    {
        // Create the directory for the ship
        (new FileInfo(shipPath)).Directory.Create();
        // Create a new text writer instance
        TextWriter tw = new StreamWriter(shipPath, false);
        // Loop through all the grids
        for(int i = 0; i < ship_grid.Count; i++)
        {
            // Add a grid instance to the file
            tw.WriteLine("G "+i);
            // Loop through all the ship parts
            foreach(ShipPart part in ship_grid[i].GetParts())
            {
                // Outputs a line in the file in the following format
                // P 1 2 3 4 0
                // Add the block id
                tw.Write("P " + part.block.ID + " ");
                // Add the parts position
                tw.Write(part.position.x + " ");
                tw.Write(part.position.y + " ");
                tw.Write(part.position.z + " ");
                // Add the blocks rotation to the file
                tw.Write(part.rotation);
                tw.WriteLine();
            }
        }
        tw.Close();
    }

    // Load the ship data file
    public static string LoadShipData(string path)
    {
        // If no file exists, create it
        if (!File.Exists(path)) File.Create(path);
        // Load the file
        StreamReader file = new StreamReader(@path);
        // Read the whole file
        string data = file.ReadToEnd();
        file.Close();
        return data;
    }

    // Used to check to see if a ship is within the defined map size
    public static Vector3 IsShipInBoundery(Vector3 pos, Vector3 momentum)
    {
        // Create a instance of the current position
        Vector3 position = pos;
        // If the position is outside the maps x upper bounds
        if (pos.x > DataStorage.MapBounderySize.x)
        {
            // Adjust the x pos to fit back inside the bounds
            position = new Vector3(DataStorage.MapBounderySize.x, position.y, position.z);
            // Define we hit the bounds
            momentum = new Vector3(0.0f, momentum.y, 0.0f);
        }
        else if (pos.x < -DataStorage.MapBounderySize.x) // If the position is outside the maps x lower bounds
        {
            // Adjust the x pos to fit back inside the bounds
            position = new Vector3(-DataStorage.MapBounderySize.x, position.y, position.z);
            // Define we hit the bounds
            momentum = new Vector3(0.0f, momentum.y, 0.0f);
            Debug.Log("Yes");
        }
        else
        // If the position is outside the maps y upper bounds
        if (pos.y > DataStorage.MapBounderySize.y)
        {
            // Adjust the y pos to fit back inside the bounds
            position = new Vector3(position.x, DataStorage.MapBounderySize.y, position.z);
            // Define we hit the bounds
            momentum = new Vector3(momentum.x, 0.0f, 0.0f);
        }
        else if (pos.y < -DataStorage.MapBounderySize.y) // If the position is outside the maps y lower bounds
        {
            // Adjust the y pos to fit back inside the bounds
            position = new Vector3(position.x, -DataStorage.MapBounderySize.y, position.z);
            // Define we hit the bounds
            momentum = new Vector3(momentum.x, 0.0f, 0.0f);
        }
        return position;
    }

    // Load the ship and return it as a grid
    public static ShipGrid LoadShipBattleGround(GameObject parent, ShipGrid shipGrid, string shipData, int shipID)
    {
        // Split the ship data file at the line ends
        string[] lines = shipData.Split('\n');
        // Loop through each line
        foreach(string line in lines)
        {
            // The line at the whitespace
            string[] lineParts = line.Split(new char[] { ' ' });
            // Make sure the line parts have data
            if (lineParts.Length > 0)
            {
                // Preform a switch case on the line start
                switch (lineParts[0])
                {
                    case "P":
                        {
                            // If we have 5 or more line parts, continue
                            if (lineParts.Length >= 5)
                            {
                                // Define variable placeholders for the ship part
                                int tryBlockId;
                                int tryRotation;
                                float tryX;
                                float tryY;
                                float tryZ;
                                // Make sure each line part is the correct data type and not malformed
                                if (!int.TryParse(lineParts[1], out tryBlockId) ||
                                    !float.TryParse(lineParts[2], out tryX) ||
                                    !float.TryParse(lineParts[3], out tryY) ||
                                    !float.TryParse(lineParts[4], out tryZ) ||
                                    !int.TryParse(lineParts[5], out tryRotation))
                                {
                                    Debug.Log("Malformed ship part");
                                }
                                else
                                {
                                    // Initialize a block that is based on block id 'x'
                                    Block block = Block.Blocks[tryBlockId];
                                    // Create a new ship part based on the position data
                                    ShipPart part = new ShipPart(new Vector3(tryX, tryY, tryZ), block, parent, tryRotation);
                                    // Assign the ship tag that is relevant to the block. This is used in collision detection to make sure we are not colliding with ourselves
                                    part.blockObject.tag = "Ship" + shipID;
                                    // Add the part to the ships grid
                                    shipGrid.AddPart(part);
                                }
                            }
                        }
                        break;
                }
            }
        }
        return shipGrid;
    }

    // Calculate the center of the grid
    public static Vector3 GetGridCenter(ShipGrid shipGrid)
    {
        // Initialize a min and max for the x and y dimensions
        float minX = 1000;
        float minY = 1000;
        float maxX = 0;
        float maxY = 0;
        // Loop through all the parts
        foreach(ShipPart part in shipGrid.GetParts())
        {
            // If the part we are currently looping through is not enabled (Destroyed) skip it
            if (!part.IsActive())
                continue;
            // If the min x is larger the current x, modify it
            if (minX > part.GetPosition().x)
                minX = part.GetPosition().x;
            // If the min y is larger the current y, modify it
            if (minY > part.GetPosition().y)
                minY = part.GetPosition().y;
            // If the max x is smaller the current x, modify it
            if (maxX < part.GetPosition().x + part.block.PartDim.x)
                maxX = part.GetPosition().x + part.block.PartDim.x;
            // If the max y is smaller the current y, modify it
            if (maxY < part.GetPosition().y + part.block.PartDim.y)
                maxY = part.GetPosition().y + part.block.PartDim.y;
        }


        // Calculate the ships width and height based on the calculated values
        float shipWidth = (maxX - minX);
        float shipHeight = (maxY - minY);
        // Return the center of the ship grid
        return new Vector3(-minX - (shipWidth / 2), -minY - (shipHeight / 2), 0.0f);
    }

    // Load the ship for the hanger and store it in a list of ship grids
    public static List<ShipGrid> LoadShipHanger(GameObject parent, string shipPath)
    {
        // Create a list of ship grids that will be the return object
        List<ShipGrid> shipGrid = new List<ShipGrid>();
        // Create the directory that the ship grid will be in
        (new FileInfo(shipPath)).Directory.Create();
        // If the file dose not exist, return the empty grids
        if (!File.Exists(shipPath)) return shipGrid;
        // Create the file reader
        StreamReader file = new StreamReader(shipPath);
        // Define a new string to store the current line
        string line;
        // Define a int to store the current grid
        int currentGrid = -1;
        // Loop through all line
        while ((line = file.ReadLine()) != null)
        {
            // Split the current line into its parts
            string[] parts = line.Split(new char[] {' '});
            // Make sure the current line has some parts
            if (parts.Length > 0)
            {
                // Preform a switch case on all the current line
                switch (parts[0])
                {
                    // If the current line is a grid
                    case "G":
                        {
                            // If the current line has 2 or more parts
                            if (parts.Length >= 2)
                            {
                                // Define variable placeholders for the grid part
                                int tryParseResponce;
                                // Make sure each line part is the correct data type and not malformed
                                if (!int.TryParse(parts[1], out tryParseResponce))
                                {
                                    Debug.Log("Malformed ship grid");
                                    continue;
                                }
                                else
                                {
                                    // Set the current grid to the grid id
                                    currentGrid = tryParseResponce;
                                    // Add a new grid to the list
                                    shipGrid.Add(new ShipGrid());
                                }
                            }
                        }
                        break;
                    case "P":
                        {
                            if (parts.Length >= 5)
                            {
                                // Define variable placeholders for the ship part
                                int tryBlockId;
                                int tryRotation;
                                float tryX;
                                float tryY;
                                float tryZ;
                                // Make sure each line part is the correct data type and not malformed
                                if (!int.TryParse(parts[1], out tryBlockId) ||
                                    !float.TryParse(parts[2], out tryX) ||
                                    !float.TryParse(parts[3], out tryY) ||
                                    !float.TryParse(parts[4], out tryZ) ||
                                    !int.TryParse(parts[5], out tryRotation))
                                {
                                    Debug.Log("Malformed ship part");
                                }
                                else
                                {
                                    // Create a new ship part based on the position data
                                    ShipPart part = new ShipPart(new Vector3(tryX, tryY, tryZ), Block.Blocks[tryBlockId], parent, tryRotation);
                                    // Add the part to the ships grid
                                    shipGrid[currentGrid].AddPart(part);
                                }
                            }
                        }
                        break;
                }
            }
        }
        file.Close();
        return shipGrid;
    }

    // Updates the animation for a specific grid's block type (EG. Setting a thruster to be active)
    public static void UpdateAnimation(System.Type type, ShipGrid grid, int animationFrameGroup)
    {
        // Loop through all the ship parts
        foreach (ShipPart part in grid.GetParts())
        {
            // If the current part is of x type and the block has the required animation frame
            if (part.block.GetType() == type && part.block.AnimationFrames.Length > animationFrameGroup)
            {
                // Set the parts animation
                part.SetAnimationGroup(part.block.AnimationFrames[animationFrameGroup , 0]);
            }
        }
    }

    // Create a instance of a object
    public static GameObject CreateObject(GameObject baseObject, Vector3 position = new Vector3(), GameObject parent = null, Texture2D texture = null, Shader shader = null)
    {
        // Create a instance of the game object
        GameObject obj = GameObject.Instantiate(baseObject, position,Quaternion.identity);
        // If the object has a parent then attach it to its parent
        if (parent != null)
        {
            // Attach the object to its parent
            obj.transform.parent = parent.transform;
            // Update the parts name so it becomes unique
            obj.name += " (" + parent.transform.childCount + ")";
        }
        // If the part has a renderer component
        if(obj.GetComponent<Renderer>() != null)
        {
            // If a texture is defined, apply it
            if (texture != null) obj.GetComponent<Renderer>().material.mainTexture = texture;
            // If a shader is defined, apply it
            if (shader != null) obj.GetComponent<Renderer>().material.shader = shader;
        }
        return obj;
    }

    // Rotate a texture by the defined rotation, only pass in non rotated textures
    public static Texture2D RotateTexture(Texture2D original, int rotation)
    {
        // if the rotation equals the same, return the original
        if (rotation % 4 == 0) return original;
        // Set the rotation to the models of itself
        rotation = rotation % 4;
        // Create a new texture that width and hight will change based on the current rotation
        Texture2D rotatedTexture = new Texture2D(rotation % 2 == 0 ? original.width : original.height, rotation % 2 == 0 ? original.height : original.width);
        // Loop through the width of the original texture
        for (int i = 0; i < original.width; i++)
        {
            // Loop through the height of the original texture
            for (int j = 0; j < original.height; j++)
            {
                switch (rotation)
                {
                    case 1:
                        // Apply the pixel of the original texture to the new one
                        // Here we flip the width coord
                        rotatedTexture.SetPixel(j, original.width - i - 1, original.GetPixel(i, j));
                        break;
                    case 2:
                        // Apply the pixel of the original texture to the new one
                        // Here we flip the width and height coord
                        rotatedTexture.SetPixel(original.width - i - 1, original.height - j - 1, original.GetPixel(i, j));
                        break;
                    case 3:
                        // Apply the pixel of the original texture to the new one
                        // Here we flip the height coord
                        rotatedTexture.SetPixel(original.height - j - 1, i, original.GetPixel(i, j));
                        break;
                }
            }
        }
        // Apply the changes to the texture
        rotatedTexture.Apply();
        return rotatedTexture;
    }

    // Set the value of a variable inside a game objects shader
    public static void SetShaderVar(GameObject obj,string var,Vector4 vec)
    {
        // If the game object has a renderer, modify then shader variable
        if (obj.GetComponent<Renderer>() != null) obj.GetComponent<Renderer>().material.SetVector(var, vec);
    }

    // Check to see if the passed position is inside the boundary
    public static bool InBoundery(Vector2Int position, Vector2Int boundery,ShipPart part)
    {
        Vector2Int p = part.GetPosition();
        // Define two variables for is a block width/height
        int width = 0;
        int height = 0;
        // If a block is rotate on a even axis, its width and height will align to the x and y of the partDim
        if (part.rotation % 2 == 0)
        {
            width = part.block.PartDim.x;
            height = part.block.PartDim.y;
        }
        else // Else the width and height will be flipped as the block will be on its side
        {
            width = part.block.PartDim.y;
            height = part.block.PartDim.x;
        }
        // If we are outside the lower and upper bounds, return false
        if (p.x < 0 || 
            p.y < 0 || 
            p.x + width > boundery.x || 
            p.y + height > boundery.y) return false;

        return true;
    }

    // Check to see if a point is colliding with a part
    public static bool PointColliding(Vector2Int point, ShipPart part)
    {
        Vector2Int p = part.GetPosition();
        // Define two variables for is a block width/height
        int width = 0;
        int height = 0;
        // If a block is rotate on a even axis, its width and height will align to the x and y of the partDim
        if (part.rotation % 2 == 0)
        {
            width = part.block.PartDim.x;
            height = part.block.PartDim.y;
        }
        else // Else the width and height will be flipped as the block will be on its side
        {
            width = part.block.PartDim.y;
            height = part.block.PartDim.x;
        }
        // If the block is located in the range of the bock return true
        if (p.x <= point.x &&
            p.x + width- 1 >= point.x && 
            p.y <= point.y &&
            p.y + height - 1 >= point.y)
        {
            return true;
        }
        return false;
    }

    // Checks if a part is colliding with a ship
    public static bool CollidingWithShipPart(List<ShipGrid> grids, ShipPart part)
    {
        // Loop through all grids
        for (int i = 0; i < grids.Count; i++)
        {
            // Loop through all parts
            foreach(ShipPart p in grids[i].GetParts())
            {
                // If the current part is colliding with the provided part then a collision was found
                if (PartsColiding(p, part)) return true;
            }
        }
        return false;
    }

    // Check to see if two parts are colliding
    public static bool PartsColiding(ShipPart part1, ShipPart part2)
    {
        // Get the two positions of the parts
        Vector2Int p1 = part1.GetPosition();
        Vector2Int p2 = part2.GetPosition();
        // Create variables to store the two parts withs and heights
        int width1 = 0;
        int height1 = 0;
        int width2 = 0;
        int height2 = 0;
        // If a block is rotate on a even axis, its width and height will align to the x and y of the partDim
        if (part1.rotation % 2 == 0)
        {
            width1 = part1.block.PartDim.x;
            height1 = part1.block.PartDim.y;
        }
        else // Else the width and height will be flipped as the block will be on its side
        {
            width1 = part1.block.PartDim.y;
            height1 = part1.block.PartDim.x;
        }
        // If a block is rotate on a even axis, its width and height will align to the x and y of the partDim
        if (part2.rotation % 2 == 0)
        {
            width2 = part2.block.PartDim.x;
            height2 = part2.block.PartDim.y;
        }
        else // Else the width and height will be flipped as the block will be on its side
        {
            width2 = part2.block.PartDim.y;
            height2 = part2.block.PartDim.x;
        }
        // Calculate to see if one axis is not inside another, if so return false
        if (p1.x >= p2.x + width2 || p1.x + width1 <= p2.x
            || p1.y >= p2.y + height2 || p1.y + height1 <= p2.y)
            return false;

        return true;
    }

    // Checks to see if two parts are touching
    public static bool PartTouching(ShipPart part1, ShipPart part2)
    {
        // Get each parts position
        Vector2Int p1 = part1.GetPosition();
        Vector2Int p2 = part2.GetPosition();
        
        // Create variables to store the two parts withs and heights
        int width1 = 0;
        int height1 = 0;
        int width2 = 0;
        int height2 = 0;
        // If a block is rotate on a even axis, its width and height will align to the x and y of the partDim
        if (part1.rotation % 2 == 0)
        {
            width1 = part1.block.PartDim.x;
            height1 = part1.block.PartDim.y;
        }
        else // Else the width and height will be flipped as the block will be on its side
        {
            width1 = part1.block.PartDim.y;
            height1 = part1.block.PartDim.x;
        }
        // If a block is rotate on a even axis, its width and height will align to the x and y of the partDim
        if (part2.rotation % 2 == 0)
        {
            width2 = part2.block.PartDim.x;
            height2 = part2.block.PartDim.y;
        }
        else // Else the width and height will be flipped as the block will be on its side
        {
            width2 = part2.block.PartDim.y;
            height2 = part2.block.PartDim.x;
        }
        // Check to see if the two parts are touching on the X axis
        if (p1.x == p2.x + width2 || p2.x == p1.x + width1)
        {
            // Check to see if the two parts Y axis + Height is in range of each other
            if (p1.y < p2.y + height2 && p2.y < p1.y + height1)
            {
                // Define the offsets for each ship parts colisionSides arrays
                int offset1 = 0;
                int offset2 = 0;
                // If p1 is larger then p2 then increase offset1 by p1-p2 or vice versa
                if (p1.y > p2.y)
                    offset1 = p1.y - p2.y;
                else
                    offset2 = p2.y - p1.y;
                // Calculate the first index of the colisionSides multidimensional array.
                int collisionSideIndex = 1 + (p1.x < p2.x ? 0 : 2);
                // Loop through for the length of the longest component
                for (int i = 0; i < height1 || i < height2; i++)
                {
                    // Get the index's for each parts colisionSides
                    int index1 = i - offset1;
                    int index2 = i - offset2;
                    // if both index's are in the range of there size and are greater then 0, continue
                    if (index1 >= 0 && index2 >= 0 && index1 < height1 && index2 < height2)
                    {
                        // Create two variables to store what indexed array needs accessed to specify what part is touching
                        int part1CollisionIndex = 0;
                        int part2CollisionIndex = 0;

                        // Checks to see if we are mirroring, then calculate index 
                        if (part1.rotation < 4)
                            part1CollisionIndex = (4 + collisionSideIndex - part1.rotation) % 4;
                        else
                            part1CollisionIndex = (collisionSideIndex + (4 - (part1.rotation % 4))) % 4;


                        // If we are in rotation range 0-3 and the rotation is in the range of 2-3 then execute the if statement or
                        // If we are in the range 5+ and the inverse modules is larger then 1 then continue.
                        // We skip 4 as we know 4 % 4 == 0 and that the collision detection should already be configured for that orientation
                        if (part1.rotation < 4 && part1.rotation % 4 > 1 ||
                            part1.rotation > 4 && 4 - (part1.rotation % 4) > 1)
                            index1 = part1.block.ColisionSides.GetLength(1) - index1 - 1;


                        // Checks to see if we are mirroring, then calculate index 
                        if (part2.rotation < 4)
                            part2CollisionIndex = (4 + collisionSideIndex + 2 - part2.rotation) % 4;
                        else
                            part2CollisionIndex = (collisionSideIndex + 2 + (4 - (part1.rotation % 4))) % 4;


                        // If we are in rotation range 0-3 and the rotation is in the range of 2-3 then execute the if statement or
                        // If we are in the range 5+ and the inverse modules is larger then 1 then continue.
                        // We skip 4 as we know 4 % 4 == 0 and that the collision detection should already be configured for that orientation
                        if (part2.rotation < 4 && part2.rotation % 4 > 1 ||
                            part2.rotation > 4 && 4 - (part2.rotation % 4) > 1)
                            index2 = part2.block.ColisionSides.GetLength(1) - index2 - 1;


                        part1CollisionIndex = (8 + collisionSideIndex - part1.rotation) % 4;
                        if (part1.rotation >= 4) part1CollisionIndex = (part1CollisionIndex + 2) % 4;
                        part2CollisionIndex = (8 + collisionSideIndex + 2 - part2.rotation) % 4;
                        if (part2.rotation >= 4) part2CollisionIndex = (part2CollisionIndex + 2) % 4;

                        // If both colisionSides come out true, then return true
                        if ((part1.block.ColisionSides.Length < 4 || part1.block.ColisionSides[part1CollisionIndex, index1]) &&
                            (part2.block.ColisionSides.Length < 4 || part2.block.ColisionSides[part2CollisionIndex, index2])) return true;
                    }
                }
            }
        }
        // Check to see if the two parts are touching on the Y axis
        if (p1.y == p2.y + height2 || p2.y == p1.y + height1)
        {
            // Check to see if the two parts X axis + Height is in range of each other
            if (p1.x < p2.x + width2 && p2.x < p1.x + width1)
            {
                // Define the offsets for each ship parts colisionSides arrays
                int offset1 = 0;
                int offset2 = 0;
                // If p1 is larger then p2 then increase offset1 by p1-p2 or vice versa
                if (p1.x > p2.x)
                    offset1 = p1.x - p2.x;
                else
                    offset2 = p2.x - p1.x;
                // Calculate the first index of the colisionSides multidimensional array.
                int collisionSideIndex = 0 + (p1.y < p2.y ? 0 : 2);
                // Loop through for the length of the longest component
                for (int i = 0; i < width1 || i < width2; i++)
                {
                    // Get the index's for each parts colisionSides
                    int index1 = i - offset1;
                    int index2 = i - offset2;
                    // if both index's are in the range of there size and are greater then 0, continue
                    if (index1 >= 0 && index2 >= 0 && index1 < width1 && index2 < width2)
                    {
                        // Create two variables to store what indexed array needs accessed to specify what part is touching
                        int part1CollisionIndex = 0;
                        int part2CollisionIndex = 0;

                        part1CollisionIndex = (8 + collisionSideIndex - part1.rotation) % 4;

                        // If we are in rotation range 0-3 and the rotation is in the range of 2-3 then execute the if statement or
                        // If we are in the range 5+ and the inverse modules is larger then 1 then continue.
                        // We skip 4 as we know 4 % 4 == 0 and that the collision detection should already be configured for that orientation
                        if (part1.rotation < 4 && part1.rotation % 4 > 1 ||
                            part1.rotation > 4 && 4 - (part1.rotation % 4) > 1)
                            index1 = part1.block.ColisionSides.GetLength(1) - index1 - 1;
                        

                        part2CollisionIndex = (8 + collisionSideIndex + 2 - part2.rotation) % 4;

                        // If we are in rotation range 0-3 and the rotation is in the range of 2-3 then execute the if statement or
                        // If we are in the range 5+ and the inverse modules is larger then 1 then continue.
                        // We skip 4 as we know 4 % 4 == 0 and that the collision detection should already be configured for that orientation
                        if (part2.rotation < 4 && part2.rotation % 4 > 1 ||
                            part2.rotation > 4 && 4 - (part2.rotation % 4) > 1)
                            index2 = part2.block.ColisionSides.GetLength(1) - index2 - 1;
                            

                        // If both colisionSides come out true, then return true
                        if ((part1.block.ColisionSides.Length < 4 || part1.block.ColisionSides[part1CollisionIndex, index1]) &&
                            (part2.block.ColisionSides.Length < 4 || part2.block.ColisionSides[part2CollisionIndex, index2])) return true;
                    }
                }
            }
        }
        return false;
    }

    // Check to see if a part is touching a grid
    public static bool GridTouchingPart(ShipGrid grid, ShipPart part)
    {
        // Loop through all the parts in the grid
        for(int i = 0; i < grid.PartCount(); i++)
        {
            // If the grid part is not being rendered skip it as it is disabled
            if (!grid.GetParts()[i].IsActive())
                continue;
            // If a part is touching the passed part, return true
            if (PartTouching(grid.GetParts()[i], part)) return true;
        }
        return false;
    }

    // Clamp the data sent to the function by the min, max bounds
    public static float Clamp(float f, float min, float max)
    {
        if (f > max) return max;
        if (f < min) return min;
        return f;
    }

    // Remove all empty grids from a list of grids
    public static List<ShipGrid> RemoveEmptyGrids(List<ShipGrid> grids)
    {
        // Loop backwards through all the grids
        for(int i = grids.Count - 1; i >= 0; i--)
        {
            // if the grid comes up null or empty, then remove it from the array
            if (grids[i] == null || grids[i].PartCount() == 0) grids.Remove(grids[i]);
        }
        return grids;
    }
    
    // Add a part to a ship
    public static void AddPartToShip(List<ShipGrid> grid, ShipPart part)
    {
        // Define a grid to add the part to it
        ShipGrid gridToAddTo = null;
        // if there is no grids
        if (grid.Count == 0)
        {
            // Add a new grid
            grid.Add(new ShipGrid());
            gridToAddTo = grid[0];
        }
        else
        {
            // Loop through all the grids
            for (int i = 0; i < grid.Count; i++)
            {
                // if the part is touching the grid
                if (Utilities.GridTouchingPart(grid[i], part))
                {
                    // If there is currently no grid that we are wanting to add to
                    if (gridToAddTo == null)
                    {
                        // Define what grid we are wanting to add to
                        gridToAddTo = grid[i];
                    }
                    else
                    {
                        // Merge both grids into the grid with the lower index
                        gridToAddTo = Utilities.MergeGrids(gridToAddTo, grid[i]);
                        grid[i] = null;
                    }
                }
            }
            // If we could not find a new grid to add to
            if (gridToAddTo == null)
            {
                // Add a new grid
                grid.Add(new ShipGrid());
                gridToAddTo = grid[grid.Count - 1];
            }
            else // If grids were found they have a chance of now being empty if they merged, so delete them
            {
                Utilities.RemoveEmptyGrids(grid);
            }
        }
        // Add the part the current grid
        gridToAddTo.AddPart(part);
    }

    // Merge two grids together and add it all to the first grid
    public static ShipGrid MergeGrids(ShipGrid grid1, ShipGrid grid2)
    {
        // Loop through all parts in grid 2
        for(int i = 0; i < grid2.PartCount(); i++)
        {
            // Add all grid 2 parts into grid 1
            grid1.AddPart(grid2.GetParts()[i]);
        }
        return grid1;
    }

    // Set the color of all blocks in a grid
    public static void SetGridColor(ShipGrid grid, Vector4 color)
    {
        // Loop through all ship parts
        foreach(ShipPart part in grid.GetParts())
        {
            // Set the part to the required color
            part.SetShaderColor(color);
        }
    }

    // Update the entire ships color for the hanger
    public static void UpdateShipGridColors(List<ShipGrid> grids)
    {
        // If there are some grids in the grid list
        if (grids.Count > 0)
            SetGridColor(grids[0], BlockColor); // Set the first grid to the default color
        // Loop through all the other grids
        for (int i = 1; i < grids.Count; i++)
        {
            // Set the grid color to red
            SetGridColor(grids[i], BlockColorRed);
        }
    }

    // Recalculate the ships stats data, this will recalculate its cost, power usage, etc
    public static DataStorage.ShipStatData RecalculateShipsStatsData(List<ShipGrid> grids)
    {
        DataStorage.ShipStatData data = new DataStorage.ShipStatData();
        // Loop through the grids within the list and add up the data values
        foreach (ShipGrid grid in grids)
        {
            data.cost += grid.GetShipCost();
            data.generatedPower += grid.GetGeneratedPower();
            data.storedPower += grid.GetStoredPower();
            data.inputPower += grid.GetDrawnPower();
            data.weight += grid.GetWeight();
            data.health += grid.GetHealth();
        }
        return data;
    }

    // Recalculate the cost of the entire ship
    public static int RecalculateCost(List<ShipGrid> grids)
    {
        int cost = 0;
        // Loop through all the grids
        for (int i = 0; i < grids.Count; i++)
        {
            // Loop through all the parts
            foreach(ShipPart part in grids[i].GetParts())
            {
                // Add to the total cost
                cost += part.block.Cost;
            }
        }
        return cost;
    }

    // Recalculate the power usage of the ship
    public static float RecalculateGeneratedPowerUsage(List<ShipGrid> grids)
    {
        float power = 0;
        // Loop through all the parts
        foreach (ShipGrid grid in grids)
        {
            // Add to the total power usage
            power += grid.GetGeneratedPower();
        }
        return power;
    }

    // Recalculate the stored power in the ship
    public static float RecalculateStoredPowerUsage(List<ShipGrid> grids)
    {
        float power = 0;
        // Loop through all the parts
        foreach (ShipGrid grid in grids)
        {
            // Add to the total stored power
            power += grid.GetStoredPower();
        }
        return power;
    }

    // Recalculate the drawn power in the ship
    public static float RecalculateDrawnPowerUsage(List<ShipGrid> grids)
    {
        float power = 0;
        // Loop through all the parts
        foreach (ShipGrid grid in grids)
        {
            // Add to the total drawn power
            power += grid.GetDrawnPower();
        }
        return power;
    }

    // Check to see if the mouse is over a UI element
    public static bool IsMouseOverUI()
    {
        return EventSystem.current.IsPointerOverGameObject();
    }

    // Apply a scalar to a vector
    public static Vector3 Scalar(float scale, Vector3 vec)
    {
        return vec * scale;
    }

    // Calculate the sum of three vectors added together
    public static Vector3 Sum(Vector3 v1, Vector3 v2, Vector3 v3)
    {
        return v1 + v2 + v3;
    }

    // Hides a UI element that has a Canvas Group
    public static void HideUI(CanvasGroup cg)
    {
        cg.alpha = 0.0f;
        cg.blocksRaycasts = false;
        cg.interactable = false;
    }

    // called to see if the ui object is open
    public static bool IsUIOpen(CanvasGroup cg)
    {
        return cg.alpha > float.Epsilon;
    }

    // Hides a UI element that has a Canvas Group
    public static void ShowUI(CanvasGroup cg)
    {
        cg.alpha = 1.0f;
        cg.blocksRaycasts = true;
        cg.interactable = true;
    }

    // Open the confirmation model if the scene has the object
    public static void OpenConfirmationModel(string message)
    {
        ShowUI(GameObject.Find("ConfirmationModel").GetComponent<CanvasGroup>());
        HideUI(GameObject.Find("SettingsModel").GetComponent<CanvasGroup>());
        HideUI(GameObject.Find("HangerMenuModel").GetComponent<CanvasGroup>());
        GameObject.Find("ConfirmationModelMainText").GetComponent<Text>().text = message;
    }

    // Close the confirmation model if the scene has the object
    public static void CloseConfirmationModel()
    {
        HideUI(GameObject.Find("ConfirmationModel").GetComponent<CanvasGroup>());
    }

    // Get the scale of a object so that no side is max 1.0f
    public static Vector3 GetItemPortrateScale(ShipPart part)
    {
        // if both sides are the same size return the default size
        if(part.block.PartDim.x == part.block.PartDim.y)
            return new Vector3(1.0f, 1.0f, 1.0f);
        // Get the width of the part
        float width = part.rotation % 2 == 0 ? part.block.PartDim.x : part.block.PartDim.y;
        // Get the height of the part
        float height = part.rotation % 2 == 0 ? part.block.PartDim.y : part.block.PartDim.x;
        // Return the scale
        if (width > height)
            return new Vector3(1.0f, height / width, 1.0f);
        else
            return new Vector3(width / height, 1.0f, 1.0f);
    }

    // Returns a list of all ships stored
    public static List<string> GetSavedShipNames(string path)
    {
        // If the directory dose not exist then create one
        (new FileInfo(path)).Directory.Create();
        // Create a list for all file names
        List<string> filenames = new List<string>();
        // Get all files that are stored in the directory
        foreach (string file in Directory.GetFiles(path, "*"))
        {
            // Add the filename to the list
            filenames.Add(Path.GetFileName(file));
        }
        // Return all filenames
        return filenames;
    }

    // Used to remove all illegal characters from a filename
    public static string FilterIllegalFilenameChars(string filename)
    {
        Regex illegalFilter = new Regex(@"[\\/:*?""<>|]");
        return illegalFilter.Replace(filename, "");
    }

    // Set the scene to the hanger scene, also if we are connected to multi player, disconnect
    public static void ReturnToHangerFromMultiplayer()
    {
        // check the current connection type
        switch (DataStorage.Connection)
        {
            case DataStorage.ConnectionType.ServerClient:

                if (DataStorage.Client != null)
                { 
                    // If we are the client and server, disconnect from the server then close it down
                    DataStorage.Client.Close();
                    DataStorage.Client = null;
                }
                if (DataStorage.Server != null)
                {
                    DataStorage.Server.Close();
                    DataStorage.Server = null;
                }
                Debug.Log("Shut down Client/Server");
                break;
            case DataStorage.ConnectionType.Client:
                if (DataStorage.Client != null)
                {
                    // If we are the client, disconnect from the server
                    DataStorage.Client.Close();
                    DataStorage.Client = null;
                    Debug.Log("Shut down Client");
                }
                break;
        }
        // Open the hanger scene
        SceneManager.LoadScene("Hanger");
    }
}
