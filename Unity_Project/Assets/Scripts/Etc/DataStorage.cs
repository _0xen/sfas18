﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DataStorage : MonoBehaviour
{
    // Store a instance of the DataStoreage block
    public static DataStorage Instance;
    // Call when the script starts up
    void Awake() {
        // If there is already a instance of 'Instance' then destroy this instance
        if(Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        // Set the value of instance to be this
        Instance = this;
        // Define that when we switch scenes that we do not want the object to be destroyed
        DontDestroyOnLoad(gameObject);
    }
    // Defines the connection types
    public enum ConnectionType { None, ServerClient, Client }
    // Define the different flags states of a ship
    public enum ShipFlags
    {
        Thrust_Up = 1,
        Thrust_Right = 2,
        Thrust_Down = 4,
        Thrust_Left = 8,
        Shooting = 16
    }

    // Define a array of all the different thruster movement flags
    public static DataStorage.ShipFlags[] thrusterFlags = new DataStorage.ShipFlags[4] {
            DataStorage.ShipFlags.Thrust_Up,
            DataStorage.ShipFlags.Thrust_Right,
            DataStorage.ShipFlags.Thrust_Down,
            DataStorage.ShipFlags.Thrust_Left
        };

    // Stored the data on how much the ship costs, its generated power, etc
    public class ShipStatData
    {
        public int cost;
        public int health;
        public int weight;
        public float generatedPower;
        public float inputPower;
        public float storedPower;
    }
    
    // Stored server specific data about the clients ships
    public class ShipServerData
    {
        public bool alive = true;
        public float deathTime = 0.0f;
        public float damageTime = 0.0f;
        public int lastDamage = -1;
    } 

    // Define the different channel messages
    public class CustomMsgType
    {
        public static short AssignPlayerIdentity = MsgType.Highest + 1;
        public static short ShipUpdate = MsgType.Highest + 2;
        public static short StartGame = MsgType.Highest + 3;
        public static short UpdatePosition = MsgType.Highest + 4;
        public static short ShipCollision = MsgType.Highest + 5;
        public static short BlockUpdate = MsgType.Highest + 6;
        public static short PlayerDeath = MsgType.Highest + 7;
        public static short PlayerRespawn = MsgType.Highest + 8;
        public static short ProjectileShot = MsgType.Highest + 9;
        public static short ProjectileShotCollision = MsgType.Highest + 10;
        public static short ShipSolarCollision = MsgType.Highest + 11;
    };

    // A structure to store the relevant data for the server about a collision such as who collided and when
    public struct CollisionInstance
    {
        public ShipCollisionMessage messageInstance;
        public float recordTime; 
    }

    // Data message passed between the server and the client saying who killed the player
    public class PlayerDeathMessage : MessageBase
    {
        public int deadPlayerID;
        public int killerID;
    }

    // When a block updates, this data packet is dispersed to all other clients
    public class BlockUpdate : MessageBase
    {
        public int shipId;
        public int blockX;
        public int blockY;
        public int health;
        public int healthChange;
        public bool alive;
    }

    // Used to relay to the server what blocks have hit each other
    public class ShipCollisionMessage : MessageBase
    {
        public int ship1Id;
        public int ship2Id;
        // We have to store the x and y in individual variables as unity dose not like the use of vector2int here due to serialization issues
        public int block1X;
        public int block1Y;
        public int block2X;
        public int block2Y;
        public float speed;
    }
    // Used to relay to the server what blocks have hit each other
    public class ShipSolarCollisionMessage : MessageBase
    {
        public int shipId;
        // We have to store the x and y in individual variables as unity dose not like the use of vector2int here due to serialization issues
        public int blockX;
        public int blockY;
    }

    // Used to relay shots fired by the ship to the server
    public class ShotMessage : MessageBase
    {
        public int shotID;
        public int shipID;
        public int blockID;
        public Vector3 position;
        public Quaternion rotation;
        public float time;
    }

    // Used to relay shots fired by the ship to the server
    public class ShotCollisionMessage : MessageBase
    {
        public int shotID;
        public int shipID;
        public int blockID;
        public int collidedBlockX;
        public int collidedBlockY;
    }

    // Use to send a re spawn message to the client
    public class RespawnMessage: MessageBase
    {
        public int id;
        public Vector3 position;
    }

    // Create a data packet that defines information needed in the lobby
    public class ShipLobbyData : MessageBase
    {
        public int id; // Define the players unique id
        public string name; // Defines the ship name that will be the users name when inside the game
        public string data; // Defines the players ship data
        public int kills;
        public int deaths;
        public bool ready; // Defines if the user has marked down that they are ready
        public bool connected; // Is the user connected
    }

    // Create a data packet that defines information about the players position
    public class PlayerPosition : MessageBase
    {
        public int id; // Define the players unique id
        public Vector3 position; // Define the players position
        public Quaternion rotation; // Define the rotation of the ship
        public Vector3 momentum; // Define the current momentum of the ship
        public Vector3 thrust; // Define the current thrust of the ship
        public DataStorage.ShipFlags flags; // Defines the ship flags for things like animations
        public Vector3 rotationDirection; // This is the requested rotation of the ship that is needed
    }

    // Define the handles for the battle zone
    public class BattleZoneHandles
    {
        // Called when the player moves
        public NetworkMessageDelegate OnPlayerPositionCallbackMethod;
        public NetworkMessageDelegate OnBlockUpdate;
        public NetworkMessageDelegate OnPlayerDeath;
        public NetworkMessageDelegate OnPlayerRespawn;
        public NetworkMessageDelegate OnProjectileShot;
    }

    // Define the handles for the lobby
    public class LobbyHandles
    {
        // Called when the player connects
        public NetworkMessageDelegate OnConnect;
        // Called when the player disconnect
        public NetworkMessageDelegate OnClientDisconnected;
        // Called when the player needs to start the game
        public NetworkMessageDelegate OnGameStart;
        // Called when the player needs to refresh ship data on the UI
        public NetworkMessageDelegate OnClientShipUpdate;
    }

    // Create a lock object for when we might have sync issued with networking
    private static readonly object lock_ = new object();
    public static object Lock_
    {
        get { return lock_; }
    }

    // Stored all instances of textures for all blocks in there various rotations
    private static Dictionary<int, List<Texture2D[]>> blockTextures = new Dictionary<int, List<Texture2D[]>>();
    public static Dictionary<int, List<Texture2D[]>> BlockTextures
    {
        get { return blockTextures; }
        set { blockTextures = value; }
    }

    // Stored the instances of the sfx's
    private static AudioClip[] sfxClips;
    public static AudioClip[] SFXClips
    {
        get { return sfxClips; }
        set { sfxClips = value; }
    }

    // Define the gravity's pull
    private static float gravityPull = 0.0015f;
    public static float GravityPull
    {
        get { return gravityPull; }
        set { gravityPull = value; }
    }

    // Define the minimum screen resolution allowed
    private static Vector2Int mimimumScreenRes = new Vector2Int(800,600);
    public static Vector2Int MimimumScreenRes
    {
        get { return mimimumScreenRes; }
        set { mimimumScreenRes = value; }
    }

    // Stores the instances of all player ships
    private static Dictionary<int, Ship> battleGroundShips;
    public static Dictionary<int, Ship> BattleGroundShips
    {
        get { return battleGroundShips; }
        set { battleGroundShips = value; }
    }

    // Stored how long a collision should be stored in the servers collision records
    private static float collisionStorageLifetime = 1.0f;
    // getters and setters for the collision lifetime
    public static float CollisionStorageLifetime
    {
        get { return collisionStorageLifetime; }
        set { collisionStorageLifetime = value; }
    }

    // Stored how long a shot should be stored in the servers collision records
    private static float shotStorageLifetime = 10.0f;
    // getters and setters for the shot lifetime
    public static float ShotStorageLifetime
    {
        get { return shotStorageLifetime; }
        set { shotStorageLifetime = value; }
    }

    // Stored how long a ship should wait before res-pawning blocks back
    private static float timeBeforeBlockRespawn = 8.0f;
    // getters and setters for the block respawn time
    public static float TimeBeforeBlockRespawn
    {
        get { return timeBeforeBlockRespawn; }
        set { timeBeforeBlockRespawn = value; }
    }

    // Stored how long a ship should wait between block respawns
    private static float blockRespawnDelay = 0.3f;
    // getters and setters for the block respawn delay
    public static float BlockRespawnDelay
    {
        get { return blockRespawnDelay; }
        set { blockRespawnDelay = value; }
    }

    private static float maxShipSpeed = 0.8f;
    // getters and setters for the max ship speed
    public static float MaxShipSpeed
    {
        get { return maxShipSpeed; }
        set { maxShipSpeed = value; }
    }


    // Stored how long a ship should wait before re spawning
    private static float shipRespawnTime = 4.0f;
    // getters and setters for the ship respawn time
    public static float ShipRespawnTime
    {
        get { return shipRespawnTime; }
        set { shipRespawnTime = value; }
    }

    // Create a instance of the handles
    private static BattleZoneHandles battleZoneHandles = new BattleZoneHandles();
    // Define the getters and setters for the handles
    public static BattleZoneHandles BattleZoneHandlesInstance
    {
        get { return battleZoneHandles; }
        set { battleZoneHandles = value; }
    }

    // Create a instance of the lobby handles
    private static LobbyHandles lobbyZoneHandles = new LobbyHandles();
    // Define the getters and setters for the handles
    public static LobbyHandles LobbyZoneHandles
    {
        get { return lobbyZoneHandles; }
        set { lobbyZoneHandles = value; }
    }

    // Define the base rotation speed of ships
    private static float baseRotationSpeed = 16.0f;
    // Define the getters and setters for the handles
    public static float BaseRotationSpeed
    {
        get { return baseRotationSpeed; }
        set { baseRotationSpeed = value; }
    }

    // Define the base rotation speed of ships
    private static float rotationSpeedDescale = 0.02f;
    // Define the getters and setters for the handles
    public static float RotationSpeedDescale
    {
        get { return rotationSpeedDescale; }
        set { rotationSpeedDescale = value; }
    }

    // Define a game object to store a explosion prefab
    private static GameObject explosionPrefab;
    // Define the getters and setters for the explosion prefab
    public static GameObject ExplosionPrefab
    {
        get { return explosionPrefab; }
        set { explosionPrefab = value; }
    }

    // Define a game object to store a block prefab
    private static GameObject blockPrefabInstance;
    // Define the getters and setters for the block prefab
    public static GameObject BlockPrefab
    {
        get { return blockPrefabInstance; }
        set { blockPrefabInstance = value; }
    }

    // Define a variable to store the max speed a ship can be colliding before it takes damage
    private static float maxSpeedBeforeDamage = 0.3f;
    // Define getters and setters for the maxSpeedBeforeDamage variable
    public static float MaxSpeedBeforeDamage
    {
        get { return maxSpeedBeforeDamage; }
        set { maxSpeedBeforeDamage = value; }
    }


    // Define how much damage should be scaled up by based on speed
    private static float collisionDamageScalar = 2.5f;
    // Define getters and setters for the collision damage scalar
    public static float CollisionDamageScalar
    {
        get { return collisionDamageScalar; }
        set { collisionDamageScalar = value; }
    }

    // Define a instance of the block shader
    private static Shader blockShaderInstance;
    // Define the getters and setters for the block shader
    public static Shader BlockShader
    {
        get { return blockShaderInstance; }
        set { blockShaderInstance = value; }
    }

    // Define the current sprite sheet width
    private static int spriteSheetSpriteWidth = 0;
    // Define the getters and setters for the sprite sheet width
    public static int SpriteSheetSpriteWidth
    {
        get { return spriteSheetSpriteWidth; }
        set { spriteSheetSpriteWidth = value; }
    }

    // Define the current sprite sheet height
    private static int spriteSheetSpriteHeight = 0;
    // Define the getters and setters for the sprite sheet height
    public static int SpriteSheetSpriteHeight
    {
        get { return spriteSheetSpriteHeight; }
        set { spriteSheetSpriteHeight = value; }
    }

    // Define the size of the sprite sheet, this will be a x * x dimension (EG: 64 x 64)
    private static int spriteSheetSpriteSize = 64;
    // Define the getters and setters for the sprite sheet size
    public static int SpriteSheetSpriteSize
    {
        get { return spriteSheetSpriteSize; }
        set { spriteSheetSpriteSize = value; }
    }

    // Define the instance of the textures
    private static Texture2D texturesInstance;
    // Define the getters and setters for the texture instance
    public static Texture2D TexturesInstance
    {
        get { return texturesInstance; }
        set { texturesInstance = value; }
    }
    
    // Define a instance of a connection config
    private static ConnectionConfig connectionConfig = new ConnectionConfig();
    // Define the getter for the connection config
    public static ConnectionConfig ConnectionConfig
    {
        get { return connectionConfig; }
    }
    
    // Define the TCP channel
    private static int _TCPChannel = connectionConfig.AddChannel(QosType.ReliableSequenced);
    // Define the getter for the tcp channel
    public static int TCPChannel
    {
        get { return _TCPChannel; }
    }
    
    // Define the UDP channel
    private static int _UDPChannel = connectionConfig.AddChannel(QosType.UnreliableSequenced);
    // Define the getter for the udp channel
    public static int UDPChannel
    {
        get { return _UDPChannel; }
    }

    // Define the current connection type that has been established
    private static ConnectionType connection = ConnectionType.None;
    // Define the getters and setters for the connection type
    public static ConnectionType Connection
    {
        get { return connection; }
        set { connection = value; }
    }

    // Define the name of the current ship
    private static string shipName = "DefaultShip";
    // Define the getter and setters for the ship name
    public static string ShipName
    {
        get { return shipName; }
        set { shipName = value; }
    }

    // Define the port that users will be connected to
    private static int port = 7777;
    // Define the getter for the connection port
    public static int Port
    {
        get { return port; }
    }

    // Define the ids for the current connected players, used to link a network connection to a id
    private static Dictionary<NetworkConnection, int> serverConnectedPlayers = new Dictionary<NetworkConnection, int>();
    // Define the getter and setters for the connected players on the server
    public static Dictionary<NetworkConnection, int> ServerConnectedPlayers
    {
        get { return serverConnectedPlayers; }
        set { serverConnectedPlayers = value; }
    }
    
    // Used to store all player instances on the server side
    private static Dictionary<int, DataStorage.ShipLobbyData> serverPlayersInstance;
    // Define the getters and setters for the server side player data
    public static Dictionary<int, DataStorage.ShipLobbyData> ServerPlayersInstance
    {
        get { return serverPlayersInstance; }
        set { serverPlayersInstance = value; }
    }
    
    // Used to store all player instances on the client side
    private static Dictionary<int, DataStorage.ShipLobbyData> clientPlayersInstance;
    // Define the getters and setters for the client side player data
    public static Dictionary<int, DataStorage.ShipLobbyData> ClientPlayersInstance
    {
        get { return clientPlayersInstance; }
        set { clientPlayersInstance = value; }
    }

    // Define the map boundary size
    private static Vector2Int mapBounderySize = new Vector2Int(300, 300);
    // Define getters and setters for the map boundary
    public static Vector2Int MapBounderySize
    {
        get { return mapBounderySize; }
        set { mapBounderySize = value; }
    }

    // Define the map boundary thickness
    private static float mapBounderyThickness = 300.0f;
    // Define getters and setters for the map boundary thickness
    public static float MapBounderyThickness
    {
        get { return mapBounderyThickness; }
        set { mapBounderyThickness = value; }
    }

    // Local client instance of itself
    private static DataStorage.ShipLobbyData clientData;
    // Define a getter and setter for the local client data
    public static DataStorage.ShipLobbyData ClientData
    {
        get { return clientData; }
        set { clientData = value; }
    }

    // Define a instance of the client
    private static Client client;
    // Define getters and setters for the client
    public static Client Client
    {
        get { return client; }
        set { client = value; }
    }

    // Define a instance of the server
    private static Server server;
    // Define getters and setters for the server
    public static Server Server
    {
        get { return server; }
        set { server = value; }
    }

    // Define the ship folder path
    private static string shipFolderPath = "Ships/";
    // Define a getter for the ship folder path
    public static string ShipFolderPath
    {
        get { return shipFolderPath; }
    }
}
