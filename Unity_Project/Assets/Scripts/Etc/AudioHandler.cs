﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHandler : MonoBehaviour 
{
    // Store a instance of the audio source
    private AudioSource source;

    // Store a instance of the DataStoreage block
    public static AudioHandler Instance;
    // Call when the script starts up
    void Awake()
    {
        // If there is already a instance of 'Instance' then destroy this instance
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        // Set the value of instance to be this
        Instance = this;
        // Define that when we switch scenes that we do not want the object to be destroyed
        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start()
    {
        // Set the source of the audio
        source = gameObject.GetComponent<AudioSource>();
        // Set the audio source volume
        source.volume = PlayerPrefs.GetFloat("MusicVolume", 0.5f);
    }

    // Switch the audio source
    public void SwitchClip(AudioClip clip)
    {
        // If the source happens not to exist, return
        if (source == null)
        {
            Start();
        }
        // If the clip dose not exist, return
        if (clip == null)
        {
            source.Stop();
            return;
        }
        // If the source is different, switch it
        if (source.clip != clip)
        {
            // Set the audio source
            source.clip = clip;
            // Start playing the music
            source.Play();
        }
    }
}
